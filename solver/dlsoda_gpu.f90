module dlsoda_gpu

use krome_cuda_common

contains
! *DECK DLSODA
      ATTRIBUTES(DEVICE) SUBROUTINE DLSODA (F, NEQ, Y, T, TOUT, ITOL, RTOL, ATOL, ITASK, &
                  ISTATE, IOPT, RWORK, LRW, IWORK, LIW, JAC, JT, CB) 
      !!!EXTERNAL F, JAC
      INTEGER NEQ, ITOL, ITASK, ISTATE, IOPT, LRW, IWORK, LIW, JT
      DOUBLE PRECISION Y, T, TOUT, RTOL, ATOL, RWORK
      DIMENSION NEQ(*), Y(*), RTOL(*), ATOL(*), RWORK(LRW), IWORK(LIW)   
      TYPE(commonBlock) CB
      interface
        attributes(device) subroutine F (NEQ, TT, NIN, DN)
          implicit none
          integer NEQ(*)
          double precision TT, NIN(*), DN(*)
        end subroutine F
        attributes(device) subroutine JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
          implicit none
          integer NEQ(*), NROWPD, ML, MU
          double precision T, Y(*), PD(NROWPD,*)
        end subroutine JAC
      end interface
! C-----------------------------------------------------------------------
! C This is the 12 November 2003 version of
! C DLSODA: Livermore Solver for Ordinary Differential Equations, with
! C         Automatic method switching for stiff and nonstiff problems.
! C
! C This version is in double precision.
! C
! C DLSODA solves the initial value problem for stiff or nonstiff
! C systems of first order ODEs,
! C     dy/dt = f(t,y) ,  or, in component form,
! C     dy(i)/dt = f(i) = f(i,t,y(1),y(2),...,y(NEQ)) (i = 1,...,NEQ).
! C
! C This a variant version of the DLSODE package.
! C It switches automatically between stiff and nonstiff methods.
! C This means that the user does not have to determine whether the
! C problem is stiff or not, and the solver will automatically choose the
! C appropriate method.  It always starts with the nonstiff method.
! C
! C Authors:       Alan C. Hindmarsh
! C                Center for Applied Scientific Computing, CB%CM_L-561
! C                Lawrence Livermore National Laboratory
! C                Livermore, CA 94551
! C and
! C                Linda R. Petzold
! C                Univ. of California at Santa Barbara
! C                Dept. of Computer Science
! C                Santa Barbara, CA 93106
! C
! C References:
! C 1.  Alan C. Hindmarsh,  ODEPACK, A Systematized Collection of ODE
! C     Solvers, in Scientific Computing, R. S. Stepleman et al. (Eds.),
! C     North-Holland, Amsterdam, 1983, pp. 55-64.
! C 2.  Linda R. Petzold, Automatic Selection of Methods for Solving
! C     Stiff and Nonstiff Systems of Ordinary Differential Equations,
! C     Siam J. Sci. Stat. Comput. 4 (1983), pp. 136-148.
! C-----------------------------------------------------------------------
! C Summary of Usage.
! C
! C Communication between the user and the DLSODA package, for normal
! C situations, is summarized here.  This summary describes only a subset
! C of the full set of options available.  See the full description for
! C details, including alternative treatment of the Jacobian matrix,
! C optional inputs and outputs, nonstandard options, and
! C instructions for special situations.  See also the example
! C problem (with program and output) following this summary.
! C
! C A. First provide a subroutine of the form:
! C               SUBROUTINE F (NEQ, T, Y, YDOT)
! C               DOUBLE PRECISION T, Y(*), YDOT(*)
! C which supplies the vector function f by loading YDOT(i) with f(i).
! C
! C B. Write a main program which calls Subroutine DLSODA once for
! C each point at which answers are desired.  This should also provide
! C for possible use of logical unit 6 for output of error messages
! C by DLSODA.  On the first call to DLSODA, supply arguments as follows:
! C F      = name of subroutine for right-hand side vector f.
! C          This name must be declared External in calling program.
! C NEQ    = number of first order ODEs.
! C Y      = array of initial values, of length NEQ.
! C T      = the initial value of the independent variable.
! C TOUT   = first point where output is desired (.ne. T).
! C ITOL   = 1 or 2 according as ATOL (below) is a scalar or array.
! C RTOL   = relative tolerance parameter (scalar).
! C ATOL   = absolute tolerance parameter (scalar or array).
! C          the estimated local error in y(i) will be controlled so as
! C          to be less than
! C             EWT(i) = RTOL*ABS(Y(i)) + ATOL     if ITOL = 1, or
! C             EWT(i) = RTOL*ABS(Y(i)) + ATOL(i)  if ITOL = 2.
! C          Thus the local error test passes if, in each component,
! C          either the absolute error is less than ATOL (or ATOL(i)),
! C          or the relative error is less than RTOL.
! C          Use RTOL = 0.0 for pure absolute error control, and
! C          use ATOL = 0.0 (or ATOL(i) = 0.0) for pure relative error
! C          control.  Caution: actual (global) errors may exceed these
! C          local tolerances, so choose them conservatively.
! C ITASK  = 1 for normal computation of output values of y at t = TOUT.
! C ISTATE = integer flag (input and output).  Set ISTATE = 1.
! C IOPT   = 0 to indicate no optional inputs used.
! C RWORK  = real work array of length at least:
! C             22 + NEQ * MAX(16, NEQ + 9).
! C          See also Paragraph E below.
! C LRW    = declared length of RWORK (in user's dimension).
! C IWORK  = integer work array of length at least  20 + NEQ.
! C LIW    = declared length of IWORK (in user's dimension).
! C JAC    = name of subroutine for Jacobian matrix.
! C          Use a dummy name.  See also Paragraph E below.
! C JT     = Jacobian type indicator.  Set JT = 2.
! C          See also Paragraph E below.
! C Note that the main program must declare arrays Y, RWORK, IWORK,
! C and possibly ATOL.
! C
! C C. The output from the first call (or any call) is:
! C      Y = array of computed values of y(t) vector.
! C      T = corresponding value of independent variable (normally TOUT).
! C ISTATE = 2  if DLSODA was successful, negative otherwise.
! C          -1 means excess work done on this call (perhaps wrong JT).
! C          -2 means excess accuracy requested (tolerances too small).
! C          -3 means illegal input detected (see printed message).
! C          -4 means repeated error test failures (check all inputs).
! C          -5 means repeated convergence failures (perhaps bad Jacobian
! C             supplied or wrong choice of JT or tolerances).
! C          -6 means error weight became zero during problem. (Solution
! C             component i vanished, and ATOL or ATOL(i) = 0.)
! C          -7 means work space insufficient to finish (see messages).
! C
! C D. To continue the integration after a successful return, simply
! C reset TOUT and call DLSODA again.  No other parameters need be reset.
! C
! C E. Note: If and when DLSODA regards the problem as stiff, and
! C switches methods accordingly, it must make use of the NEQ by NEQ
! C Jacobian matrix, J = df/dy.  For the sake of simplicity, the
! C inputs to DLSODA recommended in Paragraph B above cause DLSODA to
! C treat J as a full matrix, and to approximate it internally by
! C difference quotients.  Alternatively, J can be treated as a band
! C matrix (with great potential reduction in the size of the RWORK
! C array).  Also, in either the full or banded case, the user can supply
! C J in closed form, with a routine whose name is passed as the JAC
! C argument.  These alternatives are described in the paragraphs on
! C RWORK, JAC, and JT in the full description of the call sequence below.
! C
! C-----------------------------------------------------------------------
! C Example Problem.
! C
! C The following is a simple example problem, with the coding
! C needed for its solution by DLSODA.  The problem is from chemical
! C kinetics, and consists of the following three rate equations:
! C     dy1/dt = -.04*y1 + 1.e4*y2*y3
! C     dy2/dt = .04*y1 - 1.e4*y2*y3 - 3.e7*y2**2
! C     dy3/dt = 3.e7*y2**2
! C on the interval from t = 0.0 to t = 4.e10, with initial conditions
! C y1 = 1.0, y2 = y3 = 0.  The problem is stiff.
! C
! C The following coding solves this problem with DLSODA,
! C printing results at t = .4, 4., ..., 4.e10.  It uses
! C ITOL = 2 and ATOL much smaller for y2 than y1 or y3 because
! C y2 has much smaller values.
! C At the end of the run, statistical quantities of interest are
! C printed (see optional outputs in the full description below).
! C
! C     EXTERNAL FEX
! C     DOUBLE PRECISION ATOL, RTOL, RWORK, T, TOUT, Y
! C     DIMENSION Y(3), ATOL(3), RWORK(70), IWORK(23)
! C     NEQ = 3
! C     Y(1) = 1.
! C     Y(2) = 0.
! C     Y(3) = 0.
! C     T = 0.
! C     TOUT = .4
! C     ITOL = 2
! C     RTOL = 1.D-4
! C     ATOL(1) = 1.D-6
! C     ATOL(2) = 1.D-10
! C     ATOL(3) = 1.D-6
! C     ITASK = 1
! C     ISTATE = 1
! C     IOPT = 0
! C     LRW = 70
! C     LIW = 23
! C     JT = 2
! C     DO 40 IOUT = 1,12
! C       CALL DLSODA(FEX,NEQ,Y,T,TOUT,ITOL,RTOL,ATOL,ITASK,ISTATE,
! C    1     IOPT,RWORK,LRW,IWORK,LIW,JDUM,JT)
! C       WRITE(6,20)T,Y(1),Y(2),Y(3)
! C 20    FORMAT(' At t =',D12.4,'   Y =',3D14.6)
! C       IF (ISTATE .LT. 0) GO TO 80
! C 40    TOUT = TOUT*10.
! C     WRITE(6,60)IWORK(11),IWORK(12),IWORK(13),IWORK(19),RWORK(15)
! C 60  FORMAT(/' No. steps =',I4,'  No. f-s =',I4,'  No. J-s =',I4/
! C    1   ' Method last used =',I2,'   Last switch was at t =',D12.4)
! C     STOP
! C 80  WRITE(6,90)ISTATE
! C 90  FORMAT(///' Error halt.. ISTATE =',I3)
! C     STOP
! C     END
! C
! C     SUBROUTINE FEX (NEQ, T, Y, YDOT)
! C     DOUBLE PRECISION T, Y, YDOT
! C     DIMENSION Y(3), YDOT(3)
! C     YDOT(1) = -.04*Y(1) + 1.D4*Y(2)*Y(3)
! C     YDOT(3) = 3.D7*Y(2)*Y(2)
! C     YDOT(2) = -YDOT(1) - YDOT(3)
! C     RETURN
! C     END
! C
! C The output of this program (on a CDC-7600 in single precision)
! C is as follows:
! C
! C   At t =  4.0000e-01   y =  9.851712e-01  3.386380e-05  1.479493e-02
! C   At t =  4.0000e+00   Y =  9.055333e-01  2.240655e-05  9.444430e-02
! C   At t =  4.0000e+01   Y =  7.158403e-01  9.186334e-06  2.841505e-01
! C   At t =  4.0000e+02   Y =  4.505250e-01  3.222964e-06  5.494717e-01
! C   At t =  4.0000e+03   Y =  1.831975e-01  8.941774e-07  8.168016e-01
! C   At t =  4.0000e+04   Y =  3.898730e-02  1.621940e-07  9.610125e-01
! C   At t =  4.0000e+05   Y =  4.936363e-03  1.984221e-08  9.950636e-01
! C   At t =  4.0000e+06   Y =  5.161831e-04  2.065786e-09  9.994838e-01
! C   At t =  4.0000e+07   Y =  5.179817e-05  2.072032e-10  9.999482e-01
! C   At t =  4.0000e+08   Y =  5.283401e-06  2.113371e-11  9.999947e-01
! C   At t =  4.0000e+09   Y =  4.659031e-07  1.863613e-12  9.999995e-01
! C   At t =  4.0000e+10   Y =  1.404280e-08  5.617126e-14  1.000000e+00
! C
! C   No. steps = 361  No. f-s = 693  No. J-s =  64
! C   Method last used = 2   Last switch was at t =  6.0092e-03
! C-----------------------------------------------------------------------
! C Full description of user interface to DLSODA.
! C
! C The user interface to DLSODA consists of the following parts.
! C
! C 1.   The call sequence to Subroutine DLSODA, which is a driver
! C      routine for the solver.  This includes descriptions of both
! C      the call sequence arguments and of user-supplied routines.
! C      following these descriptions is a description of
! C      optional inputs available through the call sequence, and then
! C      a description of optional outputs (in the work arrays).
! C
! C 2.   Descriptions of other routines in the DLSODA package that may be
! C      (optionally) called by the user.  These provide the ability to
! C      alter error message handling, save and restore the internal
! C      Common, and obtain specified derivatives of the solution y(t).
! C
! C 3.   Descriptions of Common blocks to be declared in overlay
! C      or similar environments, or to be saved when doing an interrupt
! C      of the problem and continued solution later.
! C
! C 4.   Description of a subroutine in the DLSODA package,
! C      which the user may replace with his/her own version, if desired.
! C      this relates to the measurement of errors.
! C
! C-----------------------------------------------------------------------
! C Part 1.  Call Sequence.
! C
! C The call sequence parameters used for input only are
! C     F, NEQ, TOUT, ITOL, RTOL, ATOL, ITASK, IOPT, LRW, LIW, JAC, JT,
! C and those used for both input and output are
! C     Y, T, ISTATE.
! C The work arrays RWORK and IWORK are also used for conditional and
! C optional inputs and optional outputs.  (The term output here refers
! C to the return from Subroutine DLSODA to the user's calling program.)
! C
! C The legality of input parameters will be thoroughly checked on the
! C initial call for the problem, but not checked thereafter unless a
! C change in input parameters is flagged by ISTATE = 3 on input.
! C
! C The descriptions of the call arguments are as follows.
! C
! C F      = the name of the user-supplied subroutine defining the
! C          ODE system.  The system must be put in the first-order
! C          form dy/dt = f(t,y), where f is a vector-valued function
! C          of the scalar t and the vector y.  Subroutine F is to
! C          compute the function f.  It is to have the form
! C               SUBROUTINE F (NEQ, T, Y, YDOT)
! C               DOUBLE PRECISION T, Y(*), YDOT(*)
! C          where NEQ, T, and Y are input, and the array YDOT = f(t,y)
! C          is output.  Y and YDOT are arrays of length NEQ.
! C          Subroutine F should not alter Y(1),...,Y(NEQ).
! C          F must be declared External in the calling program.
! C
! C          Subroutine F may access user-defined quantities in
! C          NEQ(2),... and/or in Y(NEQ(1)+1),... if NEQ is an array
! C          (dimensioned in F) and/or Y has length exceeding NEQ(1).
! C          See the descriptions of NEQ and Y below.
! C
! C          If quantities computed in the F routine are needed
! C          externally to DLSODA, an extra call to F should be made
! C          for this purpose, for consistent and accurate results.
! C          If only the derivative dy/dt is needed, use DINTDY instead.
! C
! C NEQ    = the size of the ODE system (number of first order
! C          ordinary differential equations).  Used only for input.
! C          NEQ may be decreased, but not increased, during the problem.
! C          If NEQ is decreased (with ISTATE = 3 on input), the
! C          remaining components of Y should be left undisturbed, if
! C          these are to be accessed in F and/or JAC.
! C
! C          Normally, NEQ is a scalar, and it is generally referred to
! C          as a scalar in this user interface description.  However,
! C          NEQ may be an array, with NEQ(1) set to the system size.
! C          (The DLSODA package accesses only NEQ(1).)  In either case,
! C          this parameter is passed as the NEQ argument in all calls
! C          to F and JAC.  Hence, if it is an array, locations
! C          NEQ(2),... may be used to store other integer data and pass
! C          it to F and/or JAC.  Subroutines F and/or JAC must include
! C          NEQ in a Dimension statement in that case.
! C
! C Y      = a real array for the vector of dependent variables, of
! C          length NEQ or more.  Used for both input and output on the
! C          first call (ISTATE = 1), and only for output on other calls.
! C          On the first call, Y must contain the vector of initial
! C          values.  On output, Y contains the computed solution vector,
! C          evaluated at T.  If desired, the Y array may be used
! C          for other purposes between calls to the solver.
! C
! C          This array is passed as the Y argument in all calls to
! C          F and JAC.  Hence its length may exceed NEQ, and locations
! C          Y(NEQ+1),... may be used to store other real data and
! C          pass it to F and/or JAC.  (The DLSODA package accesses only
! C          Y(1),...,Y(NEQ).)
! C
! C T      = the independent variable.  On input, T is used only on the
! C          first call, as the initial point of the integration.
! C          on output, after each call, T is the value at which a
! C          computed solution Y is evaluated (usually the same as TOUT).
! C          on an error return, T is the farthest point reached.
! C
! C TOUT   = the next value of t at which a computed solution is desired.
! C          Used only for input.
! C
! C          When starting the problem (ISTATE = 1), TOUT may be equal
! C          to T for one call, then should .ne. T for the next call.
! C          For the initial t, an input value of TOUT .ne. T is used
! C          in order to determine the direction of the integration
! C          (i.e. the algebraic sign of the step sizes) and the rough
! C          scale of the problem.  Integration in either direction
! C          (forward or backward in t) is permitted.
! C
! C          If ITASK = 2 or 5 (one-step modes), TOUT is ignored after
! C          the first call (i.e. the first call with TOUT .ne. T).
! C          Otherwise, TOUT is required on every call.
! C
! C          If ITASK = 1, 3, or 4, the values of TOUT need not be
! C          monotone, but a value of TOUT which backs up is limited
! C          to the current internal T interval, whose endpoints are
! C          TCUR - CB%CM_HU and TCUR (see optional outputs, below, for
! C          TCUR and CB%CM_HU).
! C
! C ITOL   = an indicator for the type of error control.  See
! C          description below under ATOL.  Used only for input.
! C
! C RTOL   = a relative error tolerance parameter, either a scalar or
! C          an array of length NEQ.  See description below under ATOL.
! C          Input only.
! C
! C ATOL   = an absolute error tolerance parameter, either a scalar or
! C          an array of length NEQ.  Input only.
! C
! C             The input parameters ITOL, RTOL, and ATOL determine
! C          the error control performed by the solver.  The solver will
! C          control the vector E = (E(i)) of estimated local errors
! C          in y, according to an inequality of the form
! C                      max-norm of ( E(i)/EWT(i) )   .le.   1,
! C          where EWT = (EWT(i)) is a vector of positive error weights.
! C          The values of RTOL and ATOL should all be non-negative.
! C          The following table gives the types (scalar/array) of
! C          RTOL and ATOL, and the corresponding form of EWT(i).
! C
! C             ITOL    RTOL       ATOL          EWT(i)
! C              1     scalar     scalar     RTOL*ABS(Y(i)) + ATOL
! C              2     scalar     array      RTOL*ABS(Y(i)) + ATOL(i)
! C              3     array      scalar     RTOL(i)*ABS(Y(i)) + ATOL
! C              4     array      array      RTOL(i)*ABS(Y(i)) + ATOL(i)
! C
! C          When either of these parameters is a scalar, it need not
! C          be dimensioned in the user's calling program.
! C
! C          If none of the above choices (with ITOL, RTOL, and ATOL
! C          fixed throughout the problem) is suitable, more general
! C          error controls can be obtained by substituting a
! C          user-supplied routine for the setting of EWT.
! C          See Part 4 below.
! C
! C          If global errors are to be estimated by making a repeated
! C          run on the same problem with smaller tolerances, then all
! C          components of RTOL and ATOL (i.e. of EWT) should be scaled
! C          down uniformly.
! C
! C ITASK  = an index specifying the task to be performed.
! C          Input only.  ITASK has the following values and meanings.
! C          1  means normal computation of output values of y(t) at
! C             t = TOUT (by overshooting and interpolating).
! C          2  means take one step only and return.
! C          3  means stop at the first internal mesh point at or
! C             beyond t = TOUT and return.
! C          4  means normal computation of output values of y(t) at
! C             t = TOUT but without overshooting t = TCRIT.
! C             TCRIT must be input as RWORK(1).  TCRIT may be equal to
! C             or beyond TOUT, but not behind it in the direction of
! C             integration.  This option is useful if the problem
! C             has a singularity at or beyond t = TCRIT.
! C          5  means take one step, without passing TCRIT, and return.
! C             TCRIT must be input as RWORK(1).
! C
! C          Note:  If ITASK = 4 or 5 and the solver reaches TCRIT
! C          (within roundoff), it will return T = TCRIT (exactly) to
! C          indicate this (unless ITASK = 4 and TOUT comes before TCRIT,
! C          in which case answers at t = TOUT are returned first).
! C
! C ISTATE = an index used for input and output to specify the
! C          the state of the calculation.
! C
! C          On input, the values of ISTATE are as follows.
! C          1  means this is the first call for the problem
! C             (initializations will be done).  See note below.
! C          2  means this is not the first call, and the calculation
! C             is to continue normally, with no change in any input
! C             parameters except possibly TOUT and ITASK.
! C             (If ITOL, RTOL, and/or ATOL are changed between calls
! C             with ISTATE = 2, the new values will be used but not
! C             tested for legality.)
! C          3  means this is not the first call, and the
! C             calculation is to continue normally, but with
! C             a change in input parameters other than
! C             TOUT and ITASK.  Changes are allowed in
! C             NEQ, ITOL, RTOL, ATOL, IOPT, LRW, LIW, JT, ML, MU,
! C             and any optional inputs except H0, CB%CM_MXORDN, and CB%CM_MXORDS.
! C             (See IWORK description for ML and MU.)
! C          Note:  A preliminary call with TOUT = T is not counted
! C          as a first call here, as no initialization or checking of
! C          input is done.  (Such a call is sometimes useful for the
! C          purpose of outputting the initial conditions.)
! C          Thus the first call for which TOUT .ne. T requires
! C          ISTATE = 1 on input.
! C
! C          On output, ISTATE has the following values and meanings.
! C           1  means nothing was done; TOUT = T and ISTATE = 1 on input.
! C           2  means the integration was performed successfully.
! C          -1  means an excessive amount of work (more than CB%CM_MXSTEP
! C              steps) was done on this call, before completing the
! C              requested task, but the integration was otherwise
! C              successful as far as T.  (CB%CM_MXSTEP is an optional input
! C              and is normally 500.)  To continue, the user may
! C              simply reset ISTATE to a value .gt. 1 and call again
! C              (the excess work step counter will be reset to 0).
! C              In addition, the user may increase CB%CM_MXSTEP to avoid
! C              this error return (see below on optional inputs).
! C          -2  means too much accuracy was requested for the precision
! C              of the machine being used.  This was detected before
! C              completing the requested task, but the integration
! C              was successful as far as T.  To continue, the tolerance
! C              parameters must be reset, and ISTATE must be set
! C              to 3.  The optional output TOLSF may be used for this
! C              purpose.  (Note: If this condition is detected before
! C              taking any steps, then an illegal input return
! C              (ISTATE = -3) occurs instead.)
! C          -3  means illegal input was detected, before taking any
! C              integration steps.  See written message for details.
! C              Note:  If the solver detects an infinite loop of calls
! C              to the solver with illegal input, it will cause
! C              the run to stop.
! C          -4  means there were repeated error test failures on
! C              one attempted step, before completing the requested
! C              task, but the integration was successful as far as T.
! C              The problem may have a singularity, or the input
! C              may be inappropriate.
! C          -5  means there were repeated convergence test failures on
! C              one attempted step, before completing the requested
! C              task, but the integration was successful as far as T.
! C              This may be caused by an inaccurate Jacobian matrix,
! C              if one is being used.
! C          -6  means EWT(i) became zero for some i during the
! C              integration.  Pure relative error control (ATOL(i)=0.0)
! C              was requested on a variable which has now vanished.
! C              The integration was successful as far as T.
! C          -7  means the length of RWORK and/or IWORK was too small to
! C              proceed, but the integration was successful as far as T.
! C              This happens when DLSODA chooses to switch methods
! C              but LRW and/or LIW is too small for the new method.
! C
! C          Note:  Since the normal output value of ISTATE is 2,
! C          it does not need to be reset for normal continuation.
! C          Also, since a negative input value of ISTATE will be
! C          regarded as illegal, a negative output value requires the
! C          user to change it, and possibly other inputs, before
! C          calling the solver again.
! C
! C IOPT   = an integer flag to specify whether or not any optional
! C          inputs are being used on this call.  Input only.
! C          The optional inputs are listed separately below.
! C          IOPT = 0 means no optional inputs are being used.
! C                   default values will be used in all cases.
! C          IOPT = 1 means one or more optional inputs are being used.
! C
! C RWORK  = a real array (double precision) for work space, and (in the
! C          first 20 words) for conditional and optional inputs and
! C          optional outputs.
! C          As DLSODA switches automatically between stiff and nonstiff
! C          methods, the required length of RWORK can change during the
! C          problem.  Thus the RWORK array passed to DLSODA can either
! C          have a static (fixed) length large enough for both methods,
! C          or have a dynamic (changing) length altered by the calling
! C          program in response to output from DLSODA.
! C
! C                       --- Fixed Length Case ---
! C          If the RWORK length is to be fixed, it should be at least
! C               MAX (LRN, LRS),
! C          where LRN and LRS are the RWORK lengths required when the
! C          current method is nonstiff or stiff, respectively.
! C
! C          The separate RWORK length requirements LRN and LRS are
! C          as follows:
! C          IF NEQ is constant and the maximum method orders have
! C          their default values, then
! C             LRN = 20 + 16*NEQ,
! C             LRS = 22 + 9*NEQ + NEQ**2           if JT = 1 or 2,
! C             LRS = 22 + 10*NEQ + (2*ML+MU)*NEQ   if JT = 4 or 5.
! C          Under any other conditions, LRN and LRS are given by:
! C             LRN = 20 + CB%CM_NYH*(CB%CM_MXORDN+1) + 3*NEQ,
! C             LRS = 20 + CB%CM_NYH*(CB%CM_MXORDS+1) + 3*NEQ + LMAT,
! C          where
! C             CB%CM_NYH    = the initial value of NEQ,
! C             CB%CM_MXORDN = 12, unless a smaller value is given as an
! C                      optional input,
! C             CB%CM_MXORDS = 5, unless a smaller value is given as an
! C                      optional input,
! C             LMAT   = length of matrix work space:
! C             LMAT   = NEQ**2 + 2              if JT = 1 or 2,
! C             LMAT   = (2*ML + MU + 1)*NEQ + 2 if JT = 4 or 5.
! C
! C                       --- Dynamic Length Case ---
! C          If the length of RWORK is to be dynamic, then it should
! C          be at least LRN or LRS, as defined above, depending on the
! C          current method.  Initially, it must be at least LRN (since
! C          DLSODA starts with the nonstiff method).  On any return
! C          from DLSODA, the optional output MCUR indicates the current
! C          method.  If MCUR differs from the value it had on the
! C          previous return, or if there has only been one call to
! C          DLSODA and MCUR is now 2, then DLSODA has switched
! C          methods during the last call, and the length of RWORK
! C          should be reset (to LRN if MCUR = 1, or to LRS if
! C          MCUR = 2).  (An increase in the RWORK length is required
! C          if DLSODA returned ISTATE = -7, but not otherwise.)
! C          After resetting the length, call DLSODA with ISTATE = 3
! C          to signal that change.
! C
! C LRW    = the length of the array RWORK, as declared by the user.
! C          (This will be checked by the solver.)
! C
! C IWORK  = an integer array for work space.
! C          As DLSODA switches automatically between stiff and nonstiff
! C          methods, the required length of IWORK can change during
! C          problem, between
! C             LIS = 20 + NEQ   and   LIN = 20,
! C          respectively.  Thus the IWORK array passed to DLSODA can
! C          either have a fixed length of at least 20 + NEQ, or have a
! C          dynamic length of at least LIN or LIS, depending on the
! C          current method.  The comments on dynamic length under
! C          RWORK above apply here.  Initially, this length need
! C          only be at least LIN = 20.
! C
! C          The first few words of IWORK are used for conditional and
! C          optional inputs and optional outputs.
! C
! C          The following 2 words in IWORK are conditional inputs:
! C            IWORK(1) = ML     these are the lower and upper
! C            IWORK(2) = MU     half-bandwidths, respectively, of the
! C                       banded Jacobian, excluding the main diagonal.
! C                       The band is defined by the matrix locations
! C                       (i,j) with i-ML .le. j .le. i+MU.  ML and MU
! C                       must satisfy  0 .le.  ML,MU  .le. NEQ-1.
! C                       These are required if JT is 4 or 5, and
! C                       ignored otherwise.  ML and MU may in fact be
! C                       the band parameters for a matrix to which
! C                       df/dy is only approximately equal.
! C
! C LIW    = the length of the array IWORK, as declared by the user.
! C          (This will be checked by the solver.)
! C
! C Note: The base addresses of the work arrays must not be
! C altered between calls to DLSODA for the same problem.
! C The contents of the work arrays must not be altered
! C between calls, except possibly for the conditional and
! C optional inputs, and except for the last 3*NEQ words of RWORK.
! C The latter space is used for internal scratch space, and so is
! C available for use by the user outside DLSODA between calls, if
! C desired (but not for use by F or JAC).
! C
! C JAC    = the name of the user-supplied routine to compute the
! C          Jacobian matrix, df/dy, if JT = 1 or 4.  The JAC routine
! C          is optional, but if the problem is expected to be stiff much
! C          of the time, you are encouraged to supply JAC, for the sake
! C          of efficiency.  (Alternatively, set JT = 2 or 5 to have
! C          DLSODA compute df/dy internally by difference quotients.)
! C          If and when DLSODA uses df/dy, it treats this NEQ by NEQ
! C          matrix either as full (JT = 1 or 2), or as banded (JT =
! C          4 or 5) with half-bandwidths ML and MU (discussed under
! C          IWORK above).  In either case, if JT = 1 or 4, the JAC
! C          routine must compute df/dy as a function of the scalar t
! C          and the vector y.  It is to have the form
! C               SUBROUTINE JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
! C               DOUBLE PRECISION T, Y(*), PD(NROWPD,*)
! C          where NEQ, T, Y, ML, MU, and NROWPD are input and the array
! C          PD is to be loaded with partial derivatives (elements of
! C          the Jacobian matrix) on output.  PD must be given a first
! C          dimension of NROWPD.  T and Y have the same meaning as in
! C          Subroutine F.
! C               In the full matrix case (JT = 1), ML and MU are
! C          ignored, and the Jacobian is to be loaded into PD in
! C          columnwise manner, with df(i)/dy(j) loaded into PD(i,j).
! C               In the band matrix case (JT = 4), the elements
! C          within the band are to be loaded into PD in columnwise
! C          manner, with diagonal lines of df/dy loaded into the rows
! C          of PD.  Thus df(i)/dy(j) is to be loaded into PD(i-j+MU+1,j).
! C          ML and MU are the half-bandwidth parameters (see IWORK).
! C          The locations in PD in the two triangular areas which
! C          correspond to nonexistent matrix elements can be ignored
! C          or loaded arbitrarily, as they are overwritten by DLSODA.
! C               JAC need not provide df/dy exactly.  A crude
! C          approximation (possibly with a smaller bandwidth) will do.
! C               In either case, PD is preset to zero by the solver,
! C          so that only the nonzero elements need be loaded by JAC.
! C          Each call to JAC is preceded by a call to F with the same
! C          arguments NEQ, T, and Y.  Thus to gain some efficiency,
! C          intermediate quantities shared by both calculations may be
! C          saved in a user Common block by F and not recomputed by JAC,
! C          if desired.  Also, JAC may alter the Y array, if desired.
! C          JAC must be declared External in the calling program.
! C               Subroutine JAC may access user-defined quantities in
! C          NEQ(2),... and/or in Y(NEQ(1)+1),... if NEQ is an array
! C          (dimensioned in JAC) and/or Y has length exceeding NEQ(1).
! C          See the descriptions of NEQ and Y above.
! C
! C JT     = Jacobian type indicator.  Used only for input.
! C          JT specifies how the Jacobian matrix df/dy will be
! C          treated, if and when DLSODA requires this matrix.
! C          JT has the following values and meanings:
! C           1 means a user-supplied full (NEQ by NEQ) Jacobian.
! C           2 means an internally generated (difference quotient) full
! C             Jacobian (using NEQ extra calls to F per df/dy value).
! C           4 means a user-supplied banded Jacobian.
! C           5 means an internally generated banded Jacobian (using
! C             ML+MU+1 extra calls to F per df/dy evaluation).
! C          If JT = 1 or 4, the user must supply a Subroutine JAC
! C          (the name is arbitrary) as described above under JAC.
! C          If JT = 2 or 5, a dummy argument can be used.
! C-----------------------------------------------------------------------
! C Optional Inputs.
! C
! C The following is a list of the optional inputs provided for in the
! C call sequence.  (See also Part 2.)  For each such input variable,
! C this table lists its name as used in this documentation, its
! C location in the call sequence, its meaning, and the default value.
! C The use of any of these inputs requires IOPT = 1, and in that
! C case all of these inputs are examined.  A value of zero for any
! C of these optional inputs will cause the default value to be used.
! C Thus to use a subset of the optional inputs, simply preload
! C locations 5 to 10 in RWORK and IWORK to 0.0 and 0 respectively, and
! C then set those of interest to nonzero values.
! C
! C Name    Location      Meaning and Default Value
! C
! C H0      RWORK(5)  the step size to be attempted on the first step.
! C                   The default value is determined by the solver.
! C
! C HMAX    RWORK(6)  the maximum absolute step size allowed.
! C                   The default value is infinite.
! C
! C CB%CM_HMIN    RWORK(7)  the minimum absolute step size allowed.
! C                   The default value is 0.  (This lower bound is not
! C                   enforced on the final step before reaching TCRIT
! C                   when ITASK = 4 or 5.)
! C
! C CB%CM_IXPR    IWORK(5)  flag to generate extra printing at method switches.
! C                   CB%CM_IXPR = 0 means no extra printing (the default).
! C                   CB%CM_IXPR = 1 means print data on each switch.
! C                   T, CB%CM_H, and CB%CM_NST will be printed on the same logical
! C                   unit as used for error messages.
! C
! C CB%CM_MXSTEP  IWORK(6)  maximum number of (internally defined) steps
! C                   allowed during one call to the solver.
! C                   The default value is 500.
! C
! C CB%CM_MXHNIL  IWORK(7)  maximum number of messages printed (per problem)
! C                   warning that T + CB%CM_H = T on a step (CB%CM_H = step size).
! C                   This must be positive to result in a non-default
! C                   value.  The default value is 10.
! C
! C CB%CM_MXORDN  IWORK(8)  the maximum order to be allowed for the nonstiff
! C                   (Adams) method.  the default value is 12.
! C                   if CB%CM_MXORDN exceeds the default value, it will
! C                   be reduced to the default value.
! C                   CB%CM_MXORDN is held constant during the problem.
! C
! C CB%CM_MXORDS  IWORK(9)  the maximum order to be allowed for the stiff
! C                   (BDF) method.  The default value is 5.
! C                   If CB%CM_MXORDS exceeds the default value, it will
! C                   be reduced to the default value.
! C                   CB%CM_MXORDS is held constant during the problem.
! C-----------------------------------------------------------------------
! C Optional Outputs.
! C
! C As optional additional output from DLSODA, the variables listed
! C below are quantities related to the performance of DLSODA
! C which are available to the user.  These are communicated by way of
! C the work arrays, but also have internal mnemonic names as shown.
! C except where stated otherwise, all of these outputs are defined
! C on any successful return from DLSODA, and on any return with
! C ISTATE = -1, -2, -4, -5, or -6.  On an illegal input return
! C (ISTATE = -3), they will be unchanged from their existing values
! C (if any), except possibly for TOLSF, LENRW, and LENIW.
! C On any error return, outputs relevant to the error will be defined,
! C as noted below.
! C
! C Name    Location      Meaning
! C
! C CB%CM_HU      RWORK(11) the step size in t last used (successfully).
! C
! C HCUR    RWORK(12) the step size to be attempted on the next step.
! C
! C TCUR    RWORK(13) the current value of the independent variable
! C                   which the solver has actually reached, i.e. the
! C                   current internal mesh point in t.  On output, TCUR
! C                   will always be at least as far as the argument
! C                   T, but may be farther (if interpolation was done).
! C
! C TOLSF   RWORK(14) a tolerance scale factor, greater than 1.0,
! C                   computed when a request for too much accuracy was
! C                   detected (ISTATE = -3 if detected at the start of
! C                   the problem, ISTATE = -2 otherwise).  If ITOL is
! C                   left unaltered but RTOL and ATOL are uniformly
! C                   scaled up by a factor of TOLSF for the next call,
! C                   then the solver is deemed likely to succeed.
! C                   (The user may also ignore TOLSF and alter the
! C                   tolerance parameters in any other way appropriate.)
! C
! C CB%CM_TSW     RWORK(15) the value of t at the time of the last method
! C                   switch, if any.
! C
! C CB%CM_NST     IWORK(11) the number of steps taken for the problem so far.
! C
! C CB%CM_NFE     IWORK(12) the number of f evaluations for the problem so far.
! C
! C CB%CM_NJE     IWORK(13) the number of Jacobian evaluations (and of matrix
! C                   LU decompositions) for the problem so far.
! C
! C CB%CM_NQU     IWORK(14) the method order last used (successfully).
! C
! C NQCUR   IWORK(15) the order to be attempted on the next step.
! C
! C IMXER   IWORK(16) the index of the component of largest magnitude in
! C                   the weighted local error vector ( E(i)/EWT(i) ),
! C                   on an error return with ISTATE = -4 or -5.
! C
! C LENRW   IWORK(17) the length of RWORK actually required, assuming
! C                   that the length of RWORK is to be fixed for the
! C                   rest of the problem, and that switching may occur.
! C                   This is defined on normal returns and on an illegal
! C                   input return for insufficient storage.
! C
! C LENIW   IWORK(18) the length of IWORK actually required, assuming
! C                   that the length of IWORK is to be fixed for the
! C                   rest of the problem, and that switching may occur.
! C                   This is defined on normal returns and on an illegal
! C                   input return for insufficient storage.
! C
! C CB%CM_MUSED   IWORK(19) the method indicator for the last successful step:
! C                   1 means Adams (nonstiff), 2 means BDF (stiff).
! C
! C MCUR    IWORK(20) the current method indicator:
! C                   1 means Adams (nonstiff), 2 means BDF (stiff).
! C                   This is the method to be attempted
! C                   on the next step.  Thus it differs from CB%CM_MUSED
! C                   only if a method switch has just been made.
! C
! C The following two arrays are segments of the RWORK array which
! C may also be of interest to the user as optional outputs.
! C For each array, the table below gives its internal name,
! C its base address in RWORK, and its description.
! C
! C Name    Base Address      Description
! C
! C YH      21             the Nordsieck history array, of size CB%CM_NYH by
! C                        (NQCUR + 1), where CB%CM_NYH is the initial value
! C                        of NEQ.  For j = 0,1,...,NQCUR, column j+1
! C                        of YH contains HCUR**j/factorial(j) times
! C                        the j-th derivative of the interpolating
! C                        polynomial currently representing the solution,
! C                        evaluated at T = TCUR.
! C
! C ACOR     CB%CM_LACOR         array of size NEQ used for the accumulated
! C         (from Common   corrections on each step, scaled on output
! C           as noted)    to represent the estimated local error in y
! C                        on the last step.  This is the vector E in
! C                        the description of the error control.  It is
! C                        defined only on a successful return from
! C                        DLSODA.  The base address CB%CM_LACOR is obtained by
! C                        including in the user's program the
! C                        following 2 lines:
! C                           COMMON /DLS001/ CB%CM_RLS(218), CB%CM_ILS(37)
! C                           CB%CM_LACOR = CB%CM_ILS(22)
! C
! C-----------------------------------------------------------------------
! C Part 2.  Other Routines Callable.
! C
! C The following are optional calls which the user may make to
! C gain additional capabilities in conjunction with DLSODA.
! C (The routines XSETUN and XSETF are designed to conform to the
! C SLATEC error handling package.)
! C
! C     Form of Call                  Function
! C   CALL XSETUN(LUN)          set the logical unit number, LUN, for
! C                             output of messages from DLSODA, if
! C                             the default is not desired.
! C                             The default value of LUN is 6.
! C
! C   CALL XSETF(MFLAG)         set a flag to control the printing of
! C                             messages by DLSODA.
! C                             MFLAG = 0 means do not print. (Danger:
! C                             This risks losing valuable information.)
! C                             MFLAG = 1 means print (the default).
! C
! C                             Either of the above calls may be made at
! C                             any time and will take effect immediately.
! C
! C   CALL DSRCMA(RSAV,ISAV,JOB) saves and restores the contents of
! C                             the internal Common blocks used by
! C                             DLSODA (see Part 3 below).
! C                             RSAV must be a real array of length 240
! C                             or more, and ISAV must be an integer
! C                             array of length 46 or more.
! C                             JOB=1 means save Common into RSAV/ISAV.
! C                             JOB=2 means restore Common from RSAV/ISAV.
! C                                DSRCMA is useful if one is
! C                             interrupting a run and restarting
! C                             later, or alternating between two or
! C                             more problems solved with DLSODA.
! C
! C   CALL DINTDY(,,,,,)        provide derivatives of y, of various
! C        (see below)          orders, at a specified point t, if
! C                             desired.  It may be called only after
! C                             a successful return from DLSODA.
! C
! C The detailed instructions for using DINTDY are as follows.
! C The form of the call is:
! C
! C   CALL DINTDY (T, K, RWORK(21), CB%CM_NYH, DKY, IFLAG)
! C
! C The input parameters are:
! C
! C T         = value of independent variable where answers are desired
! C             (normally the same as the T last returned by DLSODA).
! C             For valid results, T must lie between TCUR - CB%CM_HU and TCUR.
! C             (See optional outputs for TCUR and CB%CM_HU.)
! C K         = integer order of the derivative desired.  K must satisfy
! C             0 .le. K .le. NQCUR, where NQCUR is the current order
! C             (see optional outputs).  The capability corresponding
! C             to K = 0, i.e. computing y(T), is already provided
! C             by DLSODA directly.  Since NQCUR .ge. 1, the first
! C             derivative dy/dt is always available with DINTDY.
! C RWORK(21) = the base address of the history array YH.
! C CB%CM_NYH       = column length of YH, equal to the initial value of NEQ.
! C
! C The output parameters are:
! C
! C DKY       = a real array of length NEQ containing the computed value
! C             of the K-th derivative of y(t).
! C IFLAG     = integer flag, returned as 0 if K and T were legal,
! C             -1 if K was illegal, and -2 if T was illegal.
! C             On an error return, a message is also written.
! C-----------------------------------------------------------------------
! C Part 3.  Common Blocks.
! C
! C If DLSODA is to be used in an overlay situation, the user
! C must declare, in the primary overlay, the variables in:
! C   (1) the call sequence to DLSODA, and
! C   (2) the two internal Common blocks
! C         /DLS001/  of length  255  (218 double precision words
! C                      followed by 37 integer words),
! C         /DLSA01/  of length  31    (22 double precision words
! C                      followed by  9 integer words).
! C
! C If DLSODA is used on a system in which the contents of internal
! C Common blocks are not preserved between calls, the user should
! C declare the above Common blocks in the calling program to insure
! C that their contents are preserved.
! C
! C If the solution of a given problem by DLSODA is to be interrupted
! C and then later continued, such as when restarting an interrupted run
! C or alternating between two or more problems, the user should save,
! C following the return from the last DLSODA call prior to the
! C interruption, the contents of the call sequence variables and the
! C internal Common blocks, and later restore these values before the
! C next DLSODA call for that problem.  To save and restore the Common
! C blocks, use Subroutine DSRCMA (see Part 2 above).
! C
! C-----------------------------------------------------------------------
! C Part 4.  Optionally Replaceable Solver Routines.
! C
! C Below is a description of a routine in the DLSODA package which
! C relates to the measurement of errors, and can be
! C replaced by a user-supplied version, if desired.  However, since such
! C a replacement may have a major impact on performance, it should be
! C done only when absolutely necessary, and only with great caution.
! C (Note: The means by which the package version of a routine is
! C superseded by the user's version may be system-dependent.)
! C
! C (a) DEWSET.
! C The following subroutine is called just before each internal
! C integration step, and sets the array of error weights, EWT, as
! C described under ITOL/RTOL/ATOL above:
! C     Subroutine DEWSET (NEQ, ITOL, RTOL, ATOL, YCUR, EWT)
! C where NEQ, ITOL, RTOL, and ATOL are as in the DLSODA call sequence,
! C YCUR contains the current dependent variable vector, and
! C EWT is the array of weights set by DEWSET.
! C
! C If the user supplies this subroutine, it must return in EWT(i)
! C (i = 1,...,NEQ) a positive quantity suitable for comparing errors
! C in y(i) to.  The EWT array returned by DEWSET is passed to the
! C DMNORM routine, and also used by DLSODA in the computation
! C of the optional output IMXER, and the increments for difference
! C quotient Jacobians.
! C
! C In the user-supplied version of DEWSET, it may be desirable to use
! C the current values of derivatives of y.  Derivatives up to order CB%CM_NQ
! C are available from the history array YH, described above under
! C optional outputs.  In DEWSET, YH is identical to the YCUR array,
! C extended to CB%CM_NQ + 1 columns with a column length of CB%CM_NYH and scale
! C factors of CB%CM_H**j/factorial(j).  On the first call for the problem,
! C given by CB%CM_NST = 0, CB%CM_NQ is 1 and CB%CM_H is temporarily set to 1.0.
! C CB%CM_NYH is the initial value of NEQ.  The quantities CB%CM_NQ, CB%CM_H, and CB%CM_NST
! C can be obtained by including in DEWSET the statements:
! C     DOUBLE PRECISION CB%CM_RLS
! C     COMMON /DLS001/ CB%CM_RLS(218),CB%CM_ILS(37)
! C     CB%CM_NQ = CB%CM_ILS(33)
! C     CB%CM_NST = CB%CM_ILS(34)
! C     CB%CM_H = CB%CM_RLS(212)
! C Thus, for example, the current value of dy/dt can be obtained as
! C YCUR(CB%CM_NYH+i)/CB%CM_H  (i=1,...,NEQ)  (and the division by CB%CM_H is
! C unnecessary when CB%CM_NST = 0).
! C-----------------------------------------------------------------------
! C
! C***REVISION HISTORY  (YYYYMMDD)
! C 19811102  DATE WRITTEN
! C 19820126  Fixed bug in tests of work space lengths;
! C           minor corrections in main prologue and comments.
! C 19870330  Major update: corrected comments throughout;
! C           removed TRET from Common; rewrote EWSET with 4 loops;
! C           fixed t test in INTDY; added Cray directives in STODA;
! C           in STODA, fixed DELP init. and logic around PJAC call;
! C           combined routines to save/restore Common;
! C           passed LEVEL = 0 in error message calls (except run abort).
! C 19970225  Fixed lines setting CB%CM_JSTART = -2 in Subroutine LSODA.
! C 20010425  Major update: convert source lines to upper case;
! C           added *DECK lines; changed from 1 to * in dummy dimensions;
! C           changed names R1MACH/D1MACH to RUMACH/DUMACH;
! C           renamed routines for uniqueness across single/double prec.;
! C           converted intrinsic names to generic form;
! C           removed ILLIN and NTREP (data loaded) from Common;
! C           removed all 'own' variables from Common;
! C           changed error messages to quoted strings;
! C           replaced XERRWV/XERRWD with 1993 revised version;
! C           converted prologues, comments, error messages to mixed case;
! C           numerous corrections to prologues and internal comments.
! C 20010507  Converted single precision source to double precision.
! C 20010613  Revised excess accuracy test (to match rest of ODEPACK).
! C 20010808  Fixed bug in DPRJA (matrix in DBNORM call).
! C 20020502  Corrected declarations in descriptions of user routines.
! C 20031105  Restored 'own' variables to Common blocks, to enable
! C           interrupt/restart feature.
! C 20031112  Added SAVE statements for data-loaded constants.
! C
! C-----------------------------------------------------------------------
! C Other routines in the DLSODA package.
! C
! C In addition to Subroutine DLSODA, the DLSODA package includes the
! C following subroutines and function routines:
! C  DINTDY   computes an interpolated value of the y vector at t = TOUT.
! C  DSTODA   is the core integrator, which does one step of the
! C           integration and the associated error control.
! C  DCFODE   sets all method coefficients and test constants.
! C  DPRJA    computes and preprocesses the Jacobian matrix J = df/dy
! C           and the Newton iteration matrix P = I - h*l0*J.
! C  DSOLSY   manages solution of linear system in chord iteration.
! C  DEWSET   sets the error weight vector EWT before each step.
! C  DMNORM   computes the weighted max-norm of a vector.
! C  DFNORM   computes the norm of a full matrix consistent with the
! C           weighted max-norm on vectors.
! C  DBNORM   computes the norm of a band matrix consistent with the
! C           weighted max-norm on vectors.
! C  DSRCMA   is a user-callable routine to save and restore
! C           the contents of the internal Common blocks.
! C  DGEFA and DGESL   are routines from LINPACK for solving full
! C           systems of linear algebraic equations.
! C  DGBFA and DGBSL   are routines from LINPACK for solving banded
! C           linear systems.
! C  DUMACH   computes the unit roundoff in a machine-independent manner.
! C  XERRWD, XSETUN, XSETF, IXSAV, and IUMACH  handle the printing of all
! C           error messages and warnings.  XERRWD is machine-dependent.
! C Note:  DMNORM, DFNORM, DBNORM, DUMACH, IXSAV, and IUMACH are
! C function routines.  All the others are subroutines.
! C
! C-----------------------------------------------------------------------
      EXTERNAL DPRJA, DSOLSY
      DOUBLE PRECISION DUMACH, DMNORM
    !   INTEGER CB%CM_INIT, CB%CM_MXSTEP, CB%CM_MXHNIL, CB%CM_NHNIL, CB%CM_NSLAST, CB%CM_NYH, IOWNS, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   INTEGER CB%CM_INSUFR, CB%CM_INSUFI, CB%CM_IXPR, IOWNS2, CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
      INTEGER I, I1, I2, IFLAG, IMXER, KGO, LF0, &
         LENIW, LENRW, LENWM, ML, MORD, MU, MXHNL0, MXSTP0
      INTEGER LEN1, LEN1C, LEN1N, LEN1S, LEN2, LENIWC, LENRWC
    !   DOUBLE PRECISION ROWNS, &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND
    !   DOUBLE PRECISION CB%CM_TSW, ROWNS2, CB%CM_PDNORM
      DOUBLE PRECISION ATOLI, AYI, BIG, EWTI, H0, HMAX, HMX, RH, RTOLI, &
         TCRIT, TDIST, TNEXT, TOL, TOLSF, TP, SIZE, SUM, W0
      DIMENSION MORD(2)
      LOGICAL IHIT
      CHARACTER*60 MSG
      !!!SAVE MORD, MXSTP0, MXHNL0
! C-----------------------------------------------------------------------
! C The following two internal Common blocks contain
! C (a) variables which are local to any subroutine but whose values must
! C     be preserved between calls to the routine ("own" variables), and
! C (b) variables which are communicated between subroutines.
! C The block DLS001 is declared in subroutines DLSODA, DINTDY, DSTODA,
! C DPRJA, and DSOLSY.
! C The block DLSA01 is declared in subroutines DLSODA, DSTODA, and DPRJA.
! C Groups of variables are replaced by dummy arrays in the Common
! C declarations in routines where those variables are not used.
! C-----------------------------------------------------------------------
    !   COMMON /DLS001/ ROWNS(209), &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND, &
    !      CB%CM_INIT, CB%CM_MXSTEP, CB%CM_MXHNIL, CB%CM_NHNIL, CB%CM_NSLAST, CB%CM_NYH, IOWNS(6), &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
! C
    !   COMMON /DLSA01/ CB%CM_TSW, ROWNS2(20), CB%CM_PDNORM, &
    !      CB%CM_INSUFR, CB%CM_INSUFI, CB%CM_IXPR, IOWNS2(2), CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
! C
      !DATA MORD(1),MORD(2)/12,5/, MXSTP0/500/, MXHNL0/10/
      !!$OMP THREADPRIVATE(/DLS001/,/DLSA01/)
      !!$OMP THREADPRIVATE(MORD,MXSTP0,MXHNL0)
      MORD(1) = 12
      MORD(2) = 5
      MXSTP0  = 500
      MXHNL0  = 10
! C-----------------------------------------------------------------------
! C Block A.
! C This code block is executed on every call.
! C It tests ISTATE and ITASK for legality and branches appropriately.
! C If ISTATE .gt. 1 but the flag CB%CM_INIT shows that initialization has
! C not yet been done, an error return occurs.
! C If ISTATE = 1 and TOUT = T, return immediately.
! C-----------------------------------------------------------------------
      IF (ISTATE .LT. 1 .OR. ISTATE .GT. 3) GO TO 601
      IF (ITASK .LT. 1 .OR. ITASK .GT. 5) GO TO 602
      IF (ISTATE .EQ. 1) GO TO 10
      IF (CB%CM_INIT .EQ. 0) GO TO 603
      IF (ISTATE .EQ. 2) GO TO 200
      GO TO 20
 10   CB%CM_INIT = 0
      IF (TOUT .EQ. T) RETURN
! C-----------------------------------------------------------------------
! C Block B.
! C The next code block is executed for the initial call (ISTATE = 1),
! C or for a continuation call with parameter changes (ISTATE = 3).
! C It contains checking of all inputs and various initializations.
! C
! C First check legality of the non-optional inputs NEQ, ITOL, IOPT,
! C JT, ML, and MU.
! C-----------------------------------------------------------------------
 20   IF (NEQ(1) .LE. 0) GO TO 604
      IF (ISTATE .EQ. 1) GO TO 25
      IF (NEQ(1) .GT. CB%CM_N) GO TO 605
 25   CB%CM_N = NEQ(1)
      IF (ITOL .LT. 1 .OR. ITOL .GT. 4) GO TO 606
      IF (IOPT .LT. 0 .OR. IOPT .GT. 1) GO TO 607
      IF (JT .EQ. 3 .OR. JT .LT. 1 .OR. JT .GT. 5) GO TO 608
      CB%CM_JTYP = JT
      IF (JT .LE. 2) GO TO 30
      ML = IWORK(1)
      MU = IWORK(2)
      IF (ML .LT. 0 .OR. ML .GE. CB%CM_N) GO TO 609
      IF (MU .LT. 0 .OR. MU .GE. CB%CM_N) GO TO 610
 30   CONTINUE
! C Next process and check the optional inputs. --------------------------
      IF (IOPT .EQ. 1) GO TO 40
      CB%CM_IXPR = 0
      CB%CM_MXSTEP = MXSTP0
      CB%CM_MXHNIL = MXHNL0
      CB%CM_HMXI = 0.0D0
      CB%CM_HMIN = 0.0D0
      IF (ISTATE .NE. 1) GO TO 60
      H0 = 0.0D0
      CB%CM_MXORDN = MORD(1)
      CB%CM_MXORDS = MORD(2)
      GO TO 60
 40   CB%CM_IXPR = IWORK(5)
      IF (CB%CM_IXPR .LT. 0 .OR. CB%CM_IXPR .GT. 1) GO TO 611
      CB%CM_MXSTEP = IWORK(6)
      IF (CB%CM_MXSTEP .LT. 0) GO TO 612
      IF (CB%CM_MXSTEP .EQ. 0) CB%CM_MXSTEP = MXSTP0
      CB%CM_MXHNIL = IWORK(7)
      IF (CB%CM_MXHNIL .LT. 0) GO TO 613
      IF (CB%CM_MXHNIL .EQ. 0) CB%CM_MXHNIL = MXHNL0
      IF (ISTATE .NE. 1) GO TO 50
      H0 = RWORK(5)
      CB%CM_MXORDN = IWORK(8)
      IF (CB%CM_MXORDN .LT. 0) GO TO 628
      IF (CB%CM_MXORDN .EQ. 0) CB%CM_MXORDN = 100
      CB%CM_MXORDN = MIN(CB%CM_MXORDN,MORD(1))
      CB%CM_MXORDS = IWORK(9)
      IF (CB%CM_MXORDS .LT. 0) GO TO 629
      IF (CB%CM_MXORDS .EQ. 0) CB%CM_MXORDS = 100
      CB%CM_MXORDS = MIN(CB%CM_MXORDS,MORD(2))
      IF ((TOUT - T)*H0 .LT. 0.0D0) GO TO 614
 50   HMAX = RWORK(6)
      IF (HMAX .LT. 0.0D0) GO TO 615
      CB%CM_HMXI = 0.0D0
      IF (HMAX .GT. 0.0D0) CB%CM_HMXI = 1.0D0/HMAX
      CB%CM_HMIN = RWORK(7)
      IF (CB%CM_HMIN .LT. 0.0D0) GO TO 616
! C-----------------------------------------------------------------------
! C Set work array pointers and check lengths LRW and LIW.
! C If ISTATE = 1, CB%CM_METH is initialized to 1 here to facilitate the
! C checking of work space lengths.
! C Pointers to segments of RWORK and IWORK are named by prefixing CB%CM_L to
! C the name of the segment.  E.g., the segment YH starts at RWORK(CB%CM_LYH).
! C Segments of RWORK (in order) are denoted  YH, WM, EWT, SAVF, ACOR.
! C If the lengths provided are insufficient for the current method,
! C an error return occurs.  This is treated as illegal input on the
! C first call, but as a problem interruption with ISTATE = -7 on a
! C continuation call.  If the lengths are sufficient for the current
! C method but not for both methods, a warning message is sent.
! C-----------------------------------------------------------------------
 60   IF (ISTATE .EQ. 1) CB%CM_METH = 1
      IF (ISTATE .EQ. 1) CB%CM_NYH = CB%CM_N
      CB%CM_LYH = 21
      LEN1N = 20 + (CB%CM_MXORDN + 1)*CB%CM_NYH
      LEN1S = 20 + (CB%CM_MXORDS + 1)*CB%CM_NYH
      CB%CM_LWM = LEN1S + 1
      IF (JT .LE. 2) LENWM = CB%CM_N*CB%CM_N + 2
      IF (JT .GE. 4) LENWM = (2*ML + MU + 1)*CB%CM_N + 2
      LEN1S = LEN1S + LENWM
      LEN1C = LEN1N
      IF (CB%CM_METH .EQ. 2) LEN1C = LEN1S
      LEN1 = MAX(LEN1N,LEN1S)
      LEN2 = 3*CB%CM_N
      LENRW = LEN1 + LEN2
      LENRWC = LEN1C + LEN2
      IWORK(17) = LENRW
      CB%CM_LIWM = 1
      LENIW = 20 + CB%CM_N
      LENIWC = 20
      IF (CB%CM_METH .EQ. 2) LENIWC = LENIW
      IWORK(18) = LENIW
      IF (ISTATE .EQ. 1 .AND. LRW .LT. LENRWC) GO TO 617
      IF (ISTATE .EQ. 1 .AND. LIW .LT. LENIWC) GO TO 618
      IF (ISTATE .EQ. 3 .AND. LRW .LT. LENRWC) GO TO 550
      IF (ISTATE .EQ. 3 .AND. LIW .LT. LENIWC) GO TO 555
      CB%CM_LEWT = LEN1 + 1
      CB%CM_INSUFR = 0
      IF (LRW .GE. LENRW) GO TO 65
      CB%CM_INSUFR = 2
      CB%CM_LEWT = LEN1C + 1
      MSG="DLSODA-  Warning.. RWORK length is sufficient for now, but  "
    !   CALL XERRWD (MSG, 60, 103, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      may not be later.  Integration will proceed anyway.   '
    !   CALL XERRWD (MSG, 60, 103, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      Length needed is LENRW = I1, while LRW = I2.'
    !   CALL XERRWD (MSG, 50, 103, 0, 2, LENRW, LRW, 0, 0.0D0, 0.0D0)
 65   CB%CM_LSAVF = CB%CM_LEWT + CB%CM_N
      CB%CM_LACOR = CB%CM_LSAVF + CB%CM_N
      CB%CM_INSUFI = 0
      IF (LIW .GE. LENIW) GO TO 70
      CB%CM_INSUFI = 2
      MSG='DLSODA-  Warning.. IWORK length is sufficient for now, but  '
    !   CALL XERRWD (MSG, 60, 104, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      may not be later.  Integration will proceed anyway.   '
    !   CALL XERRWD (MSG, 60, 104, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      Length needed is LENIW = I1, while LIW = I2.'
    !   CALL XERRWD (MSG, 50, 104, 0, 2, LENIW, LIW, 0, 0.0D0, 0.0D0)
 70   CONTINUE
! C Check RTOL and ATOL for legality. ------------------------------------
      RTOLI = RTOL(1)
      ATOLI = ATOL(1)
      DO 75 I = 1,CB%CM_N
        IF (ITOL .GE. 3) RTOLI = RTOL(I)
        IF (ITOL .EQ. 2 .OR. ITOL .EQ. 4) ATOLI = ATOL(I)
        IF (RTOLI .LT. 0.0D0) GO TO 619
        IF (ATOLI .LT. 0.0D0) GO TO 620
 75     CONTINUE
      IF (ISTATE .EQ. 1) GO TO 100
! C If ISTATE = 3, set flag to signal parameter changes to DSTODA. -------
      CB%CM_JSTART = -1
      IF (CB%CM_N .EQ. CB%CM_NYH) GO TO 200
! C NEQ was reduced.  Zero part of YH to avoid undefined references. -----
      I1 = CB%CM_LYH + CB%CM_L*CB%CM_NYH
      I2 = CB%CM_LYH + (CB%CM_MAXORD + 1)*CB%CM_NYH - 1
      IF (I1 .GT. I2) GO TO 200
      DO 95 I = I1,I2
 95     RWORK(I) = 0.0D0
      GO TO 200
! C-----------------------------------------------------------------------
! C Block C.
! C The next block is for the initial call only (ISTATE = 1).
! C It contains all remaining initializations, the initial call to F,
! C and the calculation of the initial step size.
! C The error weights in EWT are inverted after being loaded.
! C-----------------------------------------------------------------------
 100  CB%CM_UROUND = DUMACH(CB)
      CB%CM_TN = T
      CB%CM_TSW = T
      CB%CM_MAXORD = CB%CM_MXORDN
      IF (ITASK .NE. 4 .AND. ITASK .NE. 5) GO TO 110
      TCRIT = RWORK(1)
      IF ((TCRIT - TOUT)*(TOUT - T) .LT. 0.0D0) GO TO 625
      IF (H0 .NE. 0.0D0 .AND. (T + H0 - TCRIT)*H0 .GT. 0.0D0) &
         H0 = TCRIT - T
 110  CB%CM_JSTART = 0
      CB%CM_NHNIL = 0
      CB%CM_NST = 0
      CB%CM_NJE = 0
      CB%CM_NSLAST = 0
      CB%CM_HU = 0.0D0
      CB%CM_NQU = 0
      CB%CM_MUSED = 0
      CB%CM_MITER = 0
      CB%CM_CCMAX = 0.3D0
      CB%CM_MAXCOR = 3
      CB%CM_MSBP = 20
      CB%CM_MXNCF = 10
! C Initial call to F.  (LF0 points to YH(*,2).) -------------------------
      LF0 = CB%CM_LYH + CB%CM_NYH
      CALL F (NEQ, T, Y, RWORK(LF0))
      CB%CM_NFE = 1
! C Load the initial value vector in YH. ---------------------------------
      DO 115 I = 1,CB%CM_N
 115    RWORK(I+CB%CM_LYH-1) = Y(I)
! C Load and invert the EWT array.  (CB%CM_H is temporarily set to 1.0.) -------
      CB%CM_NQ = 1
      CB%CM_H = 1.0D0
      CALL DEWSET (CB%CM_N, ITOL, RTOL, ATOL, RWORK(CB%CM_LYH), RWORK(CB%CM_LEWT), CB)
      DO 120 I = 1,CB%CM_N
        IF (RWORK(I+CB%CM_LEWT-1) .LE. 0.0D0) GO TO 621
 120    RWORK(I+CB%CM_LEWT-1) = 1.0D0/RWORK(I+CB%CM_LEWT-1)
! C-----------------------------------------------------------------------
! C The coding below computes the step size, H0, to be attempted on the
! C first step, unless the user has supplied a value for this.
! C First check that TOUT - T differs significantly from zero.
! C A scalar tolerance quantity TOL is computed, as MAX(RTOL(i))
! C if this is positive, or MAX(ATOL(i)/ABS(Y(i))) otherwise, adjusted
! C so as to be between 100*CB%CM_UROUND and 1.0E-3.
! C Then the computed value H0 is given by:
! C
! C   H0**(-2)  =  1./(TOL * w0**2)  +  TOL * (norm(F))**2
! C
! C where   w0     = MAX ( ABS(T), ABS(TOUT) ),
! C         F      = the initial value of the vector f(t,y), and
! C         norm() = the weighted vector norm used throughout, given by
! C                  the DMNORM function routine, and weighted by the
! C                  tolerances initially loaded into the EWT array.
! C The sign of H0 is inferred from the initial values of TOUT and T.
! C ABS(H0) is made .le. ABS(TOUT-T) in any case.
! C-----------------------------------------------------------------------
      IF (H0 .NE. 0.0D0) GO TO 180
      TDIST = ABS(TOUT - T)
      W0 = MAX(ABS(T),ABS(TOUT))
      IF (TDIST .LT. 2.0D0*CB%CM_UROUND*W0) GO TO 622
      TOL = RTOL(1)
      IF (ITOL .LE. 2) GO TO 140
      DO 130 I = 1,CB%CM_N
 130    TOL = MAX(TOL,RTOL(I))
 140  IF (TOL .GT. 0.0D0) GO TO 160
      ATOLI = ATOL(1)
      DO 150 I = 1,CB%CM_N
        IF (ITOL .EQ. 2 .OR. ITOL .EQ. 4) ATOLI = ATOL(I)
        AYI = ABS(Y(I))
        IF (AYI .NE. 0.0D0) TOL = MAX(TOL,ATOLI/AYI)
 150    CONTINUE
 160  TOL = MAX(TOL,100.0D0*CB%CM_UROUND)
      TOL = MIN(TOL,0.001D0)
      SUM = DMNORM (CB%CM_N, RWORK(LF0), RWORK(CB%CM_LEWT), CB)
      SUM = 1.0D0/(TOL*W0*W0) + TOL*SUM**2
      H0 = 1.0D0/SQRT(SUM)
      H0 = MIN(H0,TDIST)
      H0 = SIGN(H0,TOUT-T)
! C Adjust H0 if necessary to meet HMAX bound. ---------------------------
 180  RH = ABS(H0)*CB%CM_HMXI
      IF (RH .GT. 1.0D0) H0 = H0/RH
! C Load CB%CM_H with H0 and scale YH(*,2) by H0. ------------------------------
      CB%CM_H = H0
      DO 190 I = 1,CB%CM_N
 190    RWORK(I+LF0-1) = H0*RWORK(I+LF0-1)
      GO TO 270
! C-----------------------------------------------------------------------
! C Block D.
! C The next code block is for continuation calls only (ISTATE = 2 or 3)
! C and is to check stop conditions before taking a step.
! C-----------------------------------------------------------------------
 200  CB%CM_NSLAST = CB%CM_NST
      GO TO (210, 250, 220, 230, 240), ITASK
 210  IF ((CB%CM_TN - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 250
      CALL DINTDY (TOUT, 0, RWORK(CB%CM_LYH), CB%CM_NYH, Y, IFLAG, CB)
      IF (IFLAG .NE. 0) GO TO 627
      T = TOUT
      GO TO 420
 220  TP = CB%CM_TN - CB%CM_HU*(1.0D0 + 100.0D0*CB%CM_UROUND)
      IF ((TP - TOUT)*CB%CM_H .GT. 0.0D0) GO TO 623
      IF ((CB%CM_TN - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 250
      T = CB%CM_TN
      GO TO 400
 230  TCRIT = RWORK(1)
      IF ((CB%CM_TN - TCRIT)*CB%CM_H .GT. 0.0D0) GO TO 624
      IF ((TCRIT - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 625
      IF ((CB%CM_TN - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 245
      CALL DINTDY (TOUT, 0, RWORK(CB%CM_LYH), CB%CM_NYH, Y, IFLAG, CB)
      IF (IFLAG .NE. 0) GO TO 627
      T = TOUT
      GO TO 420
 240  TCRIT = RWORK(1)
      IF ((CB%CM_TN - TCRIT)*CB%CM_H .GT. 0.0D0) GO TO 624
 245  HMX = ABS(CB%CM_TN) + ABS(CB%CM_H)
      IHIT = ABS(CB%CM_TN - TCRIT) .LE. 100.0D0*CB%CM_UROUND*HMX
      IF (IHIT) T = TCRIT
      IF (IHIT) GO TO 400
      TNEXT = CB%CM_TN + CB%CM_H*(1.0D0 + 4.0D0*CB%CM_UROUND)
      IF ((TNEXT - TCRIT)*CB%CM_H .LE. 0.0D0) GO TO 250
      CB%CM_H = (TCRIT - CB%CM_TN)*(1.0D0 - 4.0D0*CB%CM_UROUND)
      IF (ISTATE .EQ. 2 .AND. CB%CM_JSTART .GE. 0) CB%CM_JSTART = -2
! C-----------------------------------------------------------------------
! C Block E.
! C The next block is normally executed for all calls and contains
! C the call to the one-step core integrator DSTODA.
! C
! C This is a looping point for the integration steps.
! C
! C First check for too many steps being taken, update EWT (if not at
! C start of problem), check for too much accuracy being requested, and
! C check for CB%CM_H below the roundoff level in T.
! C-----------------------------------------------------------------------
 250  CONTINUE
      IF (CB%CM_METH .EQ. CB%CM_MUSED) GO TO 255
      IF (CB%CM_INSUFR .EQ. 1) GO TO 550
      IF (CB%CM_INSUFI .EQ. 1) GO TO 555
 255  IF ((CB%CM_NST-CB%CM_NSLAST) .GE. CB%CM_MXSTEP) GO TO 500
      CALL DEWSET (CB%CM_N, ITOL, RTOL, ATOL, RWORK(CB%CM_LYH), RWORK(CB%CM_LEWT), CB)
      DO 260 I = 1,CB%CM_N
        IF (RWORK(I+CB%CM_LEWT-1) .LE. 0.0D0) GO TO 510
 260    RWORK(I+CB%CM_LEWT-1) = 1.0D0/RWORK(I+CB%CM_LEWT-1)
 270  TOLSF = CB%CM_UROUND*DMNORM (CB%CM_N, RWORK(CB%CM_LYH), RWORK(CB%CM_LEWT), CB)
      IF (TOLSF .LE. 1.0D0) GO TO 280
      TOLSF = TOLSF*2.0D0
      IF (CB%CM_NST .EQ. 0) GO TO 626
      GO TO 520
 280  IF ((CB%CM_TN + CB%CM_H) .NE. CB%CM_TN) GO TO 290
      CB%CM_NHNIL = CB%CM_NHNIL + 1
      IF (CB%CM_NHNIL .GT. CB%CM_MXHNIL) GO TO 290
      MSG = 'DLSODA-  Warning..Internal T (=R1) and CB%CM_H (=R2) are'
    !   CALL XERRWD (MSG, 50, 101, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      such that in the machine, T + CB%CM_H = T on the next step  '
    !   CALL XERRWD (MSG, 60, 101, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '     (CB%CM_H = step size). Solver will continue anyway.'
    !   CALL XERRWD (MSG, 50, 101, 0, 0, 0, 0, 2, CB%CM_TN, CB%CM_H)
      IF (CB%CM_NHNIL .LT. CB%CM_MXHNIL) GO TO 290
      MSG = 'DLSODA-  Above warning has been issued I1 times.  '
    !   CALL XERRWD (MSG, 50, 102, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '     It will not be issued again for this problem.'
    !   CALL XERRWD (MSG, 50, 102, 0, 1, CB%CM_MXHNIL, 0, 0, 0.0D0, 0.0D0)
 290  CONTINUE
! C-----------------------------------------------------------------------
! C   CALL DSTODA(NEQ,Y,YH,CB%CM_NYH,YH,EWT,SAVF,ACOR,WM,IWM,F,JAC,DPRJA,DSOLSY)
! C-----------------------------------------------------------------------
      CALL DSTODA (NEQ, Y, RWORK(CB%CM_LYH), CB%CM_NYH, RWORK(CB%CM_LYH), RWORK(CB%CM_LEWT), &
         RWORK(CB%CM_LSAVF), RWORK(CB%CM_LACOR), RWORK(CB%CM_LWM), IWORK(CB%CM_LIWM), &
         F, JAC, DPRJA, DSOLSY, CB)
      KGO = 1 - CB%CM_KFLAG
      GO TO (300, 530, 540), KGO
! C-----------------------------------------------------------------------
! C Block F.
! C The following block handles the case of a successful return from the
! C core integrator (CB%CM_KFLAG = 0).
! C If a method switch was just made, record CB%CM_TSW, reset CB%CM_MAXORD,
! C set CB%CM_JSTART to -1 to signal DSTODA to complete the switch,
! C and do extra printing of data if CB%CM_IXPR = 1.
! C Then, in any case, check for stop conditions.
! C-----------------------------------------------------------------------
 300  CB%CM_INIT = 1
      IF (CB%CM_METH .EQ. CB%CM_MUSED) GO TO 310
      CB%CM_TSW = CB%CM_TN
      CB%CM_MAXORD = CB%CM_MXORDN
      IF (CB%CM_METH .EQ. 2) CB%CM_MAXORD = CB%CM_MXORDS
      IF (CB%CM_METH .EQ. 2) RWORK(CB%CM_LWM) = SQRT(CB%CM_UROUND)
      CB%CM_INSUFR = MIN(CB%CM_INSUFR,1)
      CB%CM_INSUFI = MIN(CB%CM_INSUFI,1)
      CB%CM_JSTART = -1
      IF (CB%CM_IXPR .EQ. 0) GO TO 310
      IF (CB%CM_METH .EQ. 2) THEN
      MSG='DLSODA- A switch to the BDF (stiff) method has occurred     '
    !   CALL XERRWD (MSG, 60, 105, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      ENDIF
      IF (CB%CM_METH .EQ. 1) THEN
      MSG='DLSODA- A switch to the Adams (nonstiff) method has occurred'
    !   CALL XERRWD (MSG, 60, 106, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      ENDIF
      MSG='     at T = R1,  tentative step size CB%CM_H = R2,  step CB%CM_NST = I1 '
    !   CALL XERRWD (MSG, 60, 107, 0, 1, CB%CM_NST, 0, 2, CB%CM_TN, CB%CM_H)
 310  GO TO (320, 400, 330, 340, 350), ITASK
! C ITASK = 1.  If TOUT has been reached, interpolate. -------------------
 320  IF ((CB%CM_TN - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 250
      CALL DINTDY (TOUT, 0, RWORK(CB%CM_LYH), CB%CM_NYH, Y, IFLAG, CB)
      T = TOUT
      GO TO 420
! C ITASK = 3.  Jump to exit if TOUT was reached. ------------------------
 330  IF ((CB%CM_TN - TOUT)*CB%CM_H .GE. 0.0D0) GO TO 400
      GO TO 250
! C ITASK = 4.  See if TOUT or TCRIT was reached.  Adjust CB%CM_H if necessary.
 340  IF ((CB%CM_TN - TOUT)*CB%CM_H .LT. 0.0D0) GO TO 345
      CALL DINTDY (TOUT, 0, RWORK(CB%CM_LYH), CB%CM_NYH, Y, IFLAG, CB)
      T = TOUT
      GO TO 420
 345  HMX = ABS(CB%CM_TN) + ABS(CB%CM_H)
      IHIT = ABS(CB%CM_TN - TCRIT) .LE. 100.0D0*CB%CM_UROUND*HMX
      IF (IHIT) GO TO 400
      TNEXT = CB%CM_TN + CB%CM_H*(1.0D0 + 4.0D0*CB%CM_UROUND)
      IF ((TNEXT - TCRIT)*CB%CM_H .LE. 0.0D0) GO TO 250
      CB%CM_H = (TCRIT - CB%CM_TN)*(1.0D0 - 4.0D0*CB%CM_UROUND)
      IF (CB%CM_JSTART .GE. 0) CB%CM_JSTART = -2
      GO TO 250
! C ITASK = 5.  See if TCRIT was reached and jump to exit. ---------------
 350  HMX = ABS(CB%CM_TN) + ABS(CB%CM_H)
      IHIT = ABS(CB%CM_TN - TCRIT) .LE. 100.0D0*CB%CM_UROUND*HMX
! C-----------------------------------------------------------------------
! C Block G.
! C The following block handles all successful returns from DLSODA.
! C If ITASK .ne. 1, Y is loaded from YH and T is set accordingly.
! C ISTATE is set to 2, and the optional outputs are loaded into the
! C work arrays before returning.
! C-----------------------------------------------------------------------
 400  DO 410 I = 1,CB%CM_N
 410    Y(I) = RWORK(I+CB%CM_LYH-1)
      T = CB%CM_TN
      IF (ITASK .NE. 4 .AND. ITASK .NE. 5) GO TO 420
      IF (IHIT) T = TCRIT
 420  ISTATE = 2
      RWORK(11) = CB%CM_HU
      RWORK(12) = CB%CM_H
      RWORK(13) = CB%CM_TN
      RWORK(15) = CB%CM_TSW
      IWORK(11) = CB%CM_NST
      IWORK(12) = CB%CM_NFE
      IWORK(13) = CB%CM_NJE
      IWORK(14) = CB%CM_NQU
      IWORK(15) = CB%CM_NQ
      IWORK(19) = CB%CM_MUSED
      IWORK(20) = CB%CM_METH
      RETURN
! C-----------------------------------------------------------------------
! C Block H.
! C The following block handles all unsuccessful returns other than
! C those for illegal input.  First the error message routine is called.
! C If there was an error test or convergence test failure, IMXER is set.
! C Then Y is loaded from YH and T is set to CB%CM_TN.
! C The optional outputs are loaded into the work arrays before returning.
! C-----------------------------------------------------------------------
! C The maximum number of steps was taken before reaching TOUT. ----------
 500  MSG = 'DLSODA-  At current T (=R1), CB%CM_MXSTEP (=I1) steps   '
    !   CALL XERRWD (MSG, 50, 201, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      taken on this call before reaching TOUT     '
    !   CALL XERRWD (MSG, 50, 201, 0, 1, CB%CM_MXSTEP, 0, 1, CB%CM_TN, 0.0D0)
      ISTATE = -1
      GO TO 580
! C EWT(i) .le. 0.0 for some i (not at start of problem). ----------------
 510  EWTI = RWORK(CB%CM_LEWT+I-1)
      MSG = 'DLSODA-  At T (=R1), EWT(I1) has become R2 .le. 0.'
    !   CALL XERRWD (MSG, 50, 202, 0, 1, I, 0, 2, CB%CM_TN, EWTI)
      ISTATE = -6
      GO TO 580
! C Too much accuracy requested for machine precision. -------------------
 520  MSG = 'DLSODA-  At T (=R1), too much accuracy requested  '
    !   CALL XERRWD (MSG, 50, 203, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      for precision of machine..  See TOLSF (=R2) '
    !   CALL XERRWD (MSG, 50, 203, 0, 0, 0, 0, 2, CB%CM_TN, TOLSF)
      RWORK(14) = TOLSF
      ISTATE = -2
      GO TO 580
! C CB%CM_KFLAG = -1.  Error test failed repeatedly or with ABS(CB%CM_H) = CB%CM_HMIN. -----
 530  MSG = 'DLSODA-  At T(=R1) and step size CB%CM_H(=R2), the error'
    !   CALL XERRWD (MSG, 50, 204, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      test failed repeatedly or with ABS(CB%CM_H) = CB%CM_HMIN'
    !   CALL XERRWD (MSG, 50, 204, 0, 0, 0, 0, 2, CB%CM_TN, CB%CM_H)
      ISTATE = -4
      GO TO 560
! C CB%CM_KFLAG = -2.  Convergence failed repeatedly or with ABS(CB%CM_H) = CB%CM_HMIN. ----
 540  MSG = 'DLSODA-  At T (=R1) and step size CB%CM_H (=R2), the    '
    !   CALL XERRWD (MSG, 50, 205, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      corrector convergence failed repeatedly     '
    !   CALL XERRWD (MSG, 50, 205, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG = '      or with ABS(CB%CM_H) = CB%CM_HMIN   '
    !   CALL XERRWD (MSG, 30, 205, 0, 0, 0, 0, 2, CB%CM_TN, CB%CM_H)
      ISTATE = -5
      GO TO 560
! C RWORK length too small to proceed. -----------------------------------
 550  MSG = 'DLSODA-  At current T(=R1), RWORK length too small'
    !   CALL XERRWD (MSG, 50, 206, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      to proceed.  The integration was otherwise successful.'
    !   CALL XERRWD (MSG, 60, 206, 0, 0, 0, 0, 1, CB%CM_TN, 0.0D0)
      ISTATE = -7
      GO TO 580
! C IWORK length too small to proceed. -----------------------------------
 555  MSG = 'DLSODA-  At current T(=R1), IWORK length too small'
    !   CALL XERRWD (MSG, 50, 207, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      to proceed.  The integration was otherwise successful.'
    !   CALL XERRWD (MSG, 60, 207, 0, 0, 0, 0, 1, CB%CM_TN, 0.0D0)
      ISTATE = -7
      GO TO 580
! C Compute IMXER if relevant. -------------------------------------------
 560  BIG = 0.0D0
      IMXER = 1
      DO 570 I = 1,CB%CM_N
        SIZE = ABS(RWORK(I+CB%CM_LACOR-1)*RWORK(I+CB%CM_LEWT-1))
        IF (BIG .GE. SIZE) GO TO 570
        BIG = SIZE
        IMXER = I
 570    CONTINUE
      IWORK(16) = IMXER
! C Set Y vector, T, and optional outputs. -------------------------------
 580  DO 590 I = 1,CB%CM_N
 590    Y(I) = RWORK(I+CB%CM_LYH-1)
      T = CB%CM_TN
      RWORK(11) = CB%CM_HU
      RWORK(12) = CB%CM_H
      RWORK(13) = CB%CM_TN
      RWORK(15) = CB%CM_TSW
      IWORK(11) = CB%CM_NST
      IWORK(12) = CB%CM_NFE
      IWORK(13) = CB%CM_NJE
      IWORK(14) = CB%CM_NQU
      IWORK(15) = CB%CM_NQ
      IWORK(19) = CB%CM_MUSED
      IWORK(20) = CB%CM_METH
      RETURN
! C-----------------------------------------------------------------------
! C Block I.
! C The following block handles all error returns due to illegal input
! C (ISTATE = -3), as detected before calling the core integrator.
! C First the error message routine is called.  If the illegal input
! C is a negative ISTATE, the run is aborted (apparent infinite loop).
! C-----------------------------------------------------------------------
 601  MSG = 'DLSODA-  ISTATE (=I1) illegal.'
    !   CALL XERRWD (MSG, 30, 1, 0, 1, ISTATE, 0, 0, 0.0D0, 0.0D0)
      IF (ISTATE .LT. 0) GO TO 800
      GO TO 700
 602  MSG = 'DLSODA-  ITASK (=I1) illegal. '
    !   CALL XERRWD (MSG, 30, 2, 0, 1, ITASK, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 603  MSG = 'DLSODA-  ISTATE .gt. 1 but DLSODA not initialized.'
    !   CALL XERRWD (MSG, 50, 3, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 604  MSG = 'DLSODA-  NEQ (=I1) .lt. 1     '
    !   CALL XERRWD (MSG, 30, 4, 0, 1, NEQ(1), 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 605  MSG = 'DLSODA-  ISTATE = 3 and NEQ increased (I1 to I2). '
    !   CALL XERRWD (MSG, 50, 5, 0, 2, CB%CM_N, NEQ(1), 0, 0.0D0, 0.0D0)
      GO TO 700
 606  MSG = 'DLSODA-  ITOL (=I1) illegal.  '
    !   CALL XERRWD (MSG, 30, 6, 0, 1, ITOL, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 607  MSG = 'DLSODA-  IOPT (=I1) illegal.  '
    !   CALL XERRWD (MSG, 30, 7, 0, 1, IOPT, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 608  MSG = 'DLSODA-  JT (=I1) illegal.    '
    !   CALL XERRWD (MSG, 30, 8, 0, 1, JT, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 609  MSG = 'DLSODA-  ML (=I1) illegal: .lt.0 or .ge.NEQ (=I2) '
    !   CALL XERRWD (MSG, 50, 9, 0, 2, ML, NEQ(1), 0, 0.0D0, 0.0D0)
      GO TO 700
 610  MSG = 'DLSODA-  MU (=I1) illegal: .lt.0 or .ge.NEQ (=I2) '
    !   CALL XERRWD (MSG, 50, 10, 0, 2, MU, NEQ(1), 0, 0.0D0, 0.0D0)
      GO TO 700
 611  MSG = 'DLSODA-  CB%CM_IXPR (=I1) illegal.  '
    !   CALL XERRWD (MSG, 30, 11, 0, 1, CB%CM_IXPR, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 612  MSG = 'DLSODA-  CB%CM_MXSTEP (=I1) .lt. 0  '
    !   CALL XERRWD (MSG, 30, 12, 0, 1, CB%CM_MXSTEP, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 613  MSG = 'DLSODA-  CB%CM_MXHNIL (=I1) .lt. 0  '
    !   CALL XERRWD (MSG, 30, 13, 0, 1, CB%CM_MXHNIL, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 614  MSG = 'DLSODA-  TOUT (=R1) behind T (=R2)      '
    !   CALL XERRWD (MSG, 40, 14, 0, 0, 0, 0, 2, TOUT, T)
      MSG = '      Integration direction is given by H0 (=R1)  '
    !   CALL XERRWD (MSG, 50, 14, 0, 0, 0, 0, 1, H0, 0.0D0)
      GO TO 700
 615  MSG = 'DLSODA-  HMAX (=R1) .lt. 0.0  '
    !   CALL XERRWD (MSG, 30, 15, 0, 0, 0, 0, 1, HMAX, 0.0D0)
      GO TO 700
 616  MSG = 'DLSODA-  CB%CM_HMIN (=R1) .lt. 0.0  '
    !   CALL XERRWD (MSG, 30, 16, 0, 0, 0, 0, 1, CB%CM_HMIN, 0.0D0)
      GO TO 700
 617  MSG='DLSODA-  RWORK length needed, LENRW (=I1), exceeds LRW (=I2)'
    !   CALL XERRWD (MSG, 60, 17, 0, 2, LENRW, LRW, 0, 0.0D0, 0.0D0)
      GO TO 700
 618  MSG='DLSODA-  IWORK length needed, LENIW (=I1), exceeds LIW (=I2)'
    !   CALL XERRWD (MSG, 60, 18, 0, 2, LENIW, LIW, 0, 0.0D0, 0.0D0)
      GO TO 700
 619  MSG = 'DLSODA-  RTOL(I1) is R1 .lt. 0.0        '
    !   CALL XERRWD (MSG, 40, 19, 0, 1, I, 0, 1, RTOLI, 0.0D0)
      GO TO 700
 620  MSG = 'DLSODA-  ATOL(I1) is R1 .lt. 0.0        '
    !   CALL XERRWD (MSG, 40, 20, 0, 1, I, 0, 1, ATOLI, 0.0D0)
      GO TO 700
 621  EWTI = RWORK(CB%CM_LEWT+I-1)
      MSG = 'DLSODA-  EWT(I1) is R1 .le. 0.0         '
    !   CALL XERRWD (MSG, 40, 21, 0, 1, I, 0, 1, EWTI, 0.0D0)
      GO TO 700
 622  MSG='DLSODA-  TOUT(=R1) too close to T(=R2) to start integration.'
    !   CALL XERRWD (MSG, 60, 22, 0, 0, 0, 0, 2, TOUT, T)
      GO TO 700
 623  MSG='DLSODA-  ITASK = I1 and TOUT (=R1) behind TCUR - CB%CM_HU (= R2)  '
    !   CALL XERRWD (MSG, 60, 23, 0, 1, ITASK, 0, 2, TOUT, TP)
      GO TO 700
 624  MSG='DLSODA-  ITASK = 4 or 5 and TCRIT (=R1) behind TCUR (=R2)   '
    !   CALL XERRWD (MSG, 60, 24, 0, 0, 0, 0, 2, TCRIT, CB%CM_TN)
      GO TO 700
 625  MSG='DLSODA-  ITASK = 4 or 5 and TCRIT (=R1) behind TOUT (=R2)   '
    !   CALL XERRWD (MSG, 60, 25, 0, 0, 0, 0, 2, TCRIT, TOUT)
      GO TO 700
 626  MSG = 'DLSODA-  At start of problem, too much accuracy   '
    !   CALL XERRWD (MSG, 50, 26, 0, 0, 0, 0, 0, 0.0D0, 0.0D0)
      MSG='      requested for precision of machine..  See TOLSF (=R1) '
    !   CALL XERRWD (MSG, 60, 26, 0, 0, 0, 0, 1, TOLSF, 0.0D0)
      RWORK(14) = TOLSF
      GO TO 700
 627  MSG = 'DLSODA-  Trouble in DINTDY.  ITASK = I1, TOUT = R1'
    !   CALL XERRWD (MSG, 50, 27, 0, 1, ITASK, 0, 1, TOUT, 0.0D0)
      GO TO 700
 628  MSG = 'DLSODA-  CB%CM_MXORDN (=I1) .lt. 0  '
    !   CALL XERRWD (MSG, 30, 28, 0, 1, CB%CM_MXORDN, 0, 0, 0.0D0, 0.0D0)
      GO TO 700
 629  MSG = 'DLSODA-  CB%CM_MXORDS (=I1) .lt. 0  '
    !   CALL XERRWD (MSG, 30, 29, 0, 1, CB%CM_MXORDS, 0, 0, 0.0D0, 0.0D0)
! C
 700  ISTATE = -3
      RETURN
! C
 800  MSG = 'DLSODA-  Run aborted.. apparent infinite loop.    '
    !   CALL XERRWD (MSG, 50, 303, 2, 0, 0, 0, 0, 0.0D0, 0.0D0)
      RETURN
! C----------------------- End of Subroutine DLSODA ----------------------
      END
! *DECK DINTDY
      ATTRIBUTES(DEVICE) SUBROUTINE DINTDY (T, K, YH, NOT_NYH, DKY, IFLAG, CB)
! C***BEGIN PROLOGUE  DINTDY
! C***SUBSIDIARY
! C***PURPOSE  Interpolate solution derivatives.
! C***TYPE      DOUBLE PRECISION (SINTDY-S, DINTDY-D)
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C
! C  DINTDY computes interpolated values of the K-th derivative of the
! C  dependent variable vector y, and stores it in DKY.  This routine
! C  is called within the package with K = 0 and T = TOUT, but may
! C  also be called by the user for any K up to the current order.
! C  (See detailed instructions in the usage documentation.)
! C
! C  The computed values in DKY are gotten by interpolation using the
! C  Nordsieck history array YH.  This array corresponds uniquely to a
! C  vector-valued polynomial of degree NQCUR or less, and DKY is set
! C  to the K-th derivative of this polynomial at T.
! C  The formula for DKY is:
! C               q
! C   DKY(i)  =  sum  c(j,K) * (T - tn)**(j-K) * h**(-j) * YH(i,j+1)
! C              j=K
! C  where  c(j,K) = j*(j-1)*...*(j-K+1), q = NQCUR, tn = TCUR, h = HCUR.
! C  The quantities  nq = NQCUR, l = nq+1, CB%CM_N = NEQ, tn, and h are
! C  communicated by COMMON.  The above sum is done in reverse order.
! C  IFLAG is returned negative if either K or T is out of bounds.
! C
! C***SEE ALSO  DLSODE
! C***ROUTINES CALLED  XERRWD
! C***COMMON BLOCKS    DLS001
! C***REVISION HISTORY  (YYMMDD)
! C   791129  DATE WRITTEN
! C   890501  Modified prologue to SLATEC/LDOC format.  (FNF)
! C   890503  Minor cosmetic changes.  (FNF)
! C   930809  Renamed to allow single/double precision versions. (ACH)
! C   010418  Reduced size of Common block /DLS001/. (ACH)
! C   031105  Restored 'own' variables to Common block /DLS001/, to
! C           enable interrupt/restart feature. (ACH)
! C   050427  Corrected roundoff decrement in TP. (ACH)
! C***END PROLOGUE  DINTDY
! C**End
      INTEGER K, NOT_NYH, IFLAG
      DOUBLE PRECISION T, YH, DKY
      DIMENSION YH(NOT_NYH,*), DKY(*)
      TYPE(commonBlock) CB
    !   INTEGER IOWND, IOWNS, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   DOUBLE PRECISION ROWNS, &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND
    !   COMMON /DLS001/ ROWNS(209), &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND, &
    !      IOWND(6), IOWNS(6), &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
      INTEGER I, IC, J, JB, JB2, JJ, JJ1, JP1
      DOUBLE PRECISION C, R, S, TP
      CHARACTER*80 MSG
!       !!!$OMP THREADPRIVATE(/DLS001/)
! C
! C***FIRST EXECUTABLE STATEMENT  DINTDY
      IFLAG = 0
      IF (K .LT. 0 .OR. K .GT. CB%CM_NQ) GO TO 80
      TP = CB%CM_TN - CB%CM_HU -  100.0D0*CB%CM_UROUND*SIGN(ABS(CB%CM_TN) + ABS(CB%CM_HU), CB%CM_HU)
      IF ((T-TP)*(T-CB%CM_TN) .GT. 0.0D0) GO TO 90
! C
      S = (T - CB%CM_TN)/CB%CM_H
      IC = 1
      IF (K .EQ. 0) GO TO 15
      JJ1 = CB%CM_L - K
      DO 10 JJ = JJ1,CB%CM_NQ
 10     IC = IC*JJ
 15   C = IC
      DO 20 I = 1,CB%CM_N
 20     DKY(I) = C*YH(I,CB%CM_L)
      IF (K .EQ. CB%CM_NQ) GO TO 55
      JB2 = CB%CM_NQ - K
      DO 50 JB = 1,JB2
        J = CB%CM_NQ - JB
        JP1 = J + 1
        IC = 1
        IF (K .EQ. 0) GO TO 35
        JJ1 = JP1 - K
        DO 30 JJ = JJ1,J
 30       IC = IC*JJ
 35     C = IC
        DO 40 I = 1,CB%CM_N
 40       DKY(I) = C*YH(I,JP1) + S*DKY(I)
 50     CONTINUE
      IF (K .EQ. 0) RETURN
 55   R = CB%CM_H**(-K)
      DO 60 I = 1,CB%CM_N
 60     DKY(I) = R*DKY(I)
      RETURN
! C
 80   MSG = 'DINTDY-  K (=I1) illegal      '
    !   CALL XERRWD (MSG, 30, 51, 0, 1, K, 0, 0, 0.0D0, 0.0D0)
      IFLAG = -1
      RETURN
 90   MSG = 'DINTDY-  T (=R1) illegal      '
    !   CALL XERRWD (MSG, 30, 52, 0, 0, 0, 0, 1, T, 0.0D0)
      MSG='      T not in interval TCUR - CB%CM_HU (= R1) to TCUR (=R2)      '
    !   CALL XERRWD (MSG, 60, 52, 0, 0, 0, 0, 2, TP, CB%CM_TN)
      IFLAG = -2
      RETURN
! C----------------------- END OF SUBROUTINE DINTDY ----------------------
      END
! *DECK DSTODA
      ATTRIBUTES(DEVICE) SUBROUTINE DSTODA (NEQ, Y, YH, NOT_NYH, YH1, EWT, SAVF, ACOR, &
         WM, IWM, F, JAC, PJAC, SLVS, CB)
      !!!EXTERNAL F, JAC, PJAC, SLVS
      INTEGER NEQ, NOT_NYH, IWM
      DOUBLE PRECISION Y, YH, YH1, EWT, SAVF, ACOR, WM
      DIMENSION NEQ(*), Y(*), YH(NOT_NYH,*), YH1(*), EWT(*), SAVF(*), &
         ACOR(*), WM(*), IWM(*)
      TYPE(commonBlock) CB
      interface
        attributes(device) subroutine F (NEQ, TT, NIN, DN)
          implicit none
          integer NEQ(*)
          double precision TT, NIN(*), DN(*)
        end subroutine F
        attributes(device) subroutine JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
          implicit none
          integer NEQ(*), NROWPD, ML, MU
          double precision T, Y(*), PD(NROWPD,*)
        end subroutine JAC
      end interface
    !   INTEGER IOWND, CB%CM_IALTH, CB%CM_IPUP, CB%CM_LMAX, MEO, CB%CM_NQNYH, CB%CM_NSLP, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   INTEGER IOWND2, CB%CM_ICOUNT, CB%CM_IRFLAG, CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
    !   DOUBLE PRECISION CB%CM_CONIT, CB%CM_CRATE, CB%CM_EL, CB%CM_ELCO, CB%CM_HOLD, CB%CM_RMAX, CB%CM_TESCO, &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND
    !   DOUBLE PRECISION ROWND2, CB%CM_CM1, CB%CM_CM2, CB%CM_PDEST, CB%CM_PDLAST, CB%CM_RATIO, &
    !      CB%CM_PDNORM
    !   COMMON /DLS001/ CB%CM_CONIT, CB%CM_CRATE, CB%CM_EL(13), CB%CM_ELCO(13,12), &
    !      CB%CM_HOLD, CB%CM_RMAX, CB%CM_TESCO(3,12), &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND, &
    !      IOWND(6), CB%CM_IALTH, CB%CM_IPUP, CB%CM_LMAX, MEO, CB%CM_NQNYH, CB%CM_NSLP, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   COMMON /DLSA01/ ROWND2, CB%CM_CM1(12), CB%CM_CM2(5), CB%CM_PDEST, CB%CM_PDLAST, CB%CM_RATIO, &
    !      CB%CM_PDNORM, &
    !      IOWND2(3), CB%CM_ICOUNT, CB%CM_IRFLAG, CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
      INTEGER I, I1, IREDO, IRET, J, JB, M, NCF, NEWQ
      INTEGER LM1, LM1P1, LM2, LM2P1, NQM1, NQM2
      DOUBLE PRECISION DCON, DDN, DEL, DELP, DSM, DUP, EXDN, EXSM, EXUP, &
         R, RH, RHDN, RHSM, RHUP, TOLD, DMNORM
      DOUBLE PRECISION ALPHA, DM1, DM2, EXM1, EXM2, &
         PDH, PNORM, RATE, RH1, RH1IT, RH2, RM 
    !   CB%CM_SM1(12)
    !   SAVE CB%CM_SM1
    !   DATA CB%CM_SM1/0.5D0, 0.575D0, 0.55D0, 0.45D0, 0.35D0, 0.25D0, &
    !      0.20D0, 0.15D0, 0.10D0, 0.075D0, 0.050D0, 0.025D0/
      !!!$OMP THREADPRIVATE(/DLS001/,/DLSA01/)
! C-----------------------------------------------------------------------
! C DSTODA performs one step of the integration of an initial value
! C problem for a system of ordinary differential equations.
! C Note: DSTODA is independent of the value of the iteration method
! C indicator CB%CM_MITER, when this is .ne. 0, and hence is independent
! C of the type of chord method used, or the Jacobian structure.
! C Communication with DSTODA is done with the following variables:
! C
! C Y      = an array of length .ge. CB%CM_N used as the Y argument in
! C          all calls to F and JAC.
! C NEQ    = integer array containing problem size in NEQ(1), and
! C          passed as the NEQ argument in all calls to F and JAC.
! C YH     = an CB%CM_NYH by CB%CM_LMAX array containing the dependent variables
! C          and their approximate scaled derivatives, where
! C          CB%CM_LMAX = CB%CM_MAXORD + 1.  YH(i,j+1) contains the approximate
! C          j-th derivative of y(i), scaled by CB%CM_H**j/factorial(j)
! C          (j = 0,1,...,CB%CM_NQ).  On entry for the first step, the first
! C          two columns of YH must be set from the initial values.
! C CB%CM_NYH    = a constant integer .ge. CB%CM_N, the first dimension of YH.
! C YH1    = a one-dimensional array occupying the same space as YH.
! C EWT    = an array of length CB%CM_N containing multiplicative weights
! C          for local error measurements.  Local errors in y(i) are
! C          compared to 1.0/EWT(i) in various error tests.
! C SAVF   = an array of working storage, of length CB%CM_N.
! C ACOR   = a work array of length CB%CM_N, used for the accumulated
! C          corrections.  On a successful return, ACOR(i) contains
! C          the estimated one-step local error in y(i).
! C WM,IWM = real and integer work arrays associated with matrix
! C          operations in chord iteration (CB%CM_MITER .ne. 0).
! C PJAC   = name of routine to evaluate and preprocess Jacobian matrix
! C          and P = I - CB%CM_H*CB%CM_EL0*Jac, if a chord method is being used.
! C          It also returns an estimate of norm(Jac) in CB%CM_PDNORM.
! C SLVS   = name of routine to solve linear system in chord iteration.
! C CB%CM_CCMAX  = maximum relative change in CB%CM_H*CB%CM_EL0 before PJAC is called.
! C CB%CM_H      = the step size to be attempted on the next step.
! C          CB%CM_H is altered by the error control algorithm during the
! C          problem.  CB%CM_H can be either positive or negative, but its
! C          sign must remain constant throughout the problem.
! C CB%CM_HMIN   = the minimum absolute value of the step size CB%CM_H to be used.
! C CB%CM_HMXI   = inverse of the maximum absolute value of CB%CM_H to be used.
! C          CB%CM_HMXI = 0.0 is allowed and corresponds to an infinite HMAX.
! C          CB%CM_HMIN and CB%CM_HMXI may be changed at any time, but will not
! C          take effect until the next change of CB%CM_H is considered.
! C CB%CM_TN     = the independent variable. CB%CM_TN is updated on each step taken.
! C CB%CM_JSTART = an integer used for input only, with the following
! C          values and meanings:
! C               0  perform the first step.
! C           .gt.0  take a new step continuing from the last.
! C              -1  take the next step with a new value of CB%CM_H,
! C                    CB%CM_N, CB%CM_METH, CB%CM_MITER, and/or matrix parameters.
! C              -2  take the next step with a new value of CB%CM_H,
! C                    but with other inputs unchanged.
! C          On return, CB%CM_JSTART is set to 1 to facilitate continuation.
! C CB%CM_KFLAG  = a completion code with the following meanings:
! C               0  the step was succesful.
! C              -1  the requested error could not be achieved.
! C              -2  corrector convergence could not be achieved.
! C              -3  fatal error in PJAC or SLVS.
! C          A return with CB%CM_KFLAG = -1 or -2 means either
! C          ABS(CB%CM_H) = CB%CM_HMIN or 10 consecutive failures occurred.
! C          On a return with CB%CM_KFLAG negative, the values of CB%CM_TN and
! C          the YH array are as of the beginning of the last
! C          step, and CB%CM_H is the last step size attempted.
! C CB%CM_MAXORD = the maximum order of integration method to be allowed.
! C CB%CM_MAXCOR = the maximum number of corrector iterations allowed.
! C CB%CM_MSBP   = maximum number of steps between PJAC calls (CB%CM_MITER .gt. 0).
! C CB%CM_MXNCF  = maximum number of convergence failures allowed.
! C CB%CM_METH   = current method.
! C          CB%CM_METH = 1 means Adams method (nonstiff)
! C          CB%CM_METH = 2 means BDF method (stiff)
! C          CB%CM_METH may be reset by DSTODA.
! C CB%CM_MITER  = corrector iteration method.
! C          CB%CM_MITER = 0 means functional iteration.
! C          CB%CM_MITER = JT .gt. 0 means a chord iteration corresponding
! C          to Jacobian type JT.  (The DLSODA/DLSODAR argument JT is
! C          communicated here as CB%CM_JTYP, but is not used in DSTODA
! C          except to load CB%CM_MITER following a method switch.)
! C          CB%CM_MITER may be reset by DSTODA.
! C CB%CM_N      = the number of first-order differential equations.
! C-----------------------------------------------------------------------
      CB%CM_SM1 = (/0.5D0, 0.575D0, 0.55D0, 0.45D0, 0.35D0, 0.25D0, &
                   0.20D0, 0.15D0, 0.10D0, 0.075D0, 0.050D0, 0.025D0/)
      CB%CM_KFLAG = 0
      TOLD = CB%CM_TN
      NCF = 0
      CB%CM_IERPJ = 0
      CB%CM_IERSL = 0
      CB%CM_JCUR = 0
      CB%CM_ICF = 0
      DELP = 0.0D0
      IF (CB%CM_JSTART .GT. 0) GO TO 200
      IF (CB%CM_JSTART .EQ. -1) GO TO 100
      IF (CB%CM_JSTART .EQ. -2) GO TO 160
! C-----------------------------------------------------------------------
! C On the first call, the order is set to 1, and other variables are
! C initialized.  CB%CM_RMAX is the maximum ratio by which CB%CM_H can be increased
! C in a single step.  It is initially 1.E4 to compensate for the small
! C initial CB%CM_H, but then is normally equal to 10.  If a failure
! C occurs (in corrector convergence or error test), CB%CM_RMAX is set at 2
! C for the next increase.
! C DCFODE is called to get the needed coefficients for both methods.
! C-----------------------------------------------------------------------
      CB%CM_LMAX = CB%CM_MAXORD + 1
      CB%CM_NQ = 1
      CB%CM_L = 2
      CB%CM_IALTH = 2
      CB%CM_RMAX = 10000.0D0
      CB%CM_RC = 0.0D0
      CB%CM_EL0 = 1.0D0
      CB%CM_CRATE = 0.7D0
      CB%CM_HOLD = CB%CM_H
      CB%CM_NSLP = 0
      CB%CM_IPUP = CB%CM_MITER
      IRET = 3
! C Initialize switching parameters.  CB%CM_METH = 1 is assumed initially. -----
      CB%CM_ICOUNT = 20
      CB%CM_IRFLAG = 0
      CB%CM_PDEST = 0.0D0
      CB%CM_PDLAST = 0.0D0
      CB%CM_RATIO = 5.0D0
      CALL DCFODE (2, CB%CM_ELCO, CB%CM_TESCO, CB)
      DO 10 I = 1,5
 10     CB%CM_CM2(I) = CB%CM_TESCO(2,I)*CB%CM_ELCO(I+1,I)
      CALL DCFODE (1, CB%CM_ELCO, CB%CM_TESCO, CB)
      DO 20 I = 1,12
 20     CB%CM_CM1(I) = CB%CM_TESCO(2,I)*CB%CM_ELCO(I+1,I)
      GO TO 150
! C-----------------------------------------------------------------------
! C The following block handles preliminaries needed when CB%CM_JSTART = -1.
! C CB%CM_IPUP is set to CB%CM_MITER to force a matrix update.
! C If an order increase is about to be considered (CB%CM_IALTH = 1),
! C CB%CM_IALTH is reset to 2 to postpone consideration one more step.
! C If the caller has changed CB%CM_METH, DCFODE is called to reset
! C the coefficients of the method.
! C If CB%CM_H is to be changed, YH must be rescaled.
! C If CB%CM_H or CB%CM_METH is being changed, CB%CM_IALTH is reset to CB%CM_L = CB%CM_NQ + 1
! C to prevent further changes in CB%CM_H for that many steps.
! C-----------------------------------------------------------------------
 100  CB%CM_IPUP = CB%CM_MITER
      CB%CM_LMAX = CB%CM_MAXORD + 1
      IF (CB%CM_IALTH .EQ. 1) CB%CM_IALTH = 2
      IF (CB%CM_METH .EQ. CB%CM_MUSED) GO TO 160
      CALL DCFODE (CB%CM_METH, CB%CM_ELCO, CB%CM_TESCO, CB)
      CB%CM_IALTH = CB%CM_L
      IRET = 1
! C-----------------------------------------------------------------------
! C The el vector and related constants are reset
! C whenever the order CB%CM_NQ is changed, or at the start of the problem.
! C-----------------------------------------------------------------------
 150  DO 155 I = 1,CB%CM_L
 155    CB%CM_EL(I) = CB%CM_ELCO(I,CB%CM_NQ)
      CB%CM_NQNYH = CB%CM_NQ*NOT_NYH
      CB%CM_RC = CB%CM_RC*CB%CM_EL(1)/CB%CM_EL0
      CB%CM_EL0 = CB%CM_EL(1)
      CB%CM_CONIT = 0.5D0/(CB%CM_NQ+2)
      GO TO (160, 170, 200), IRET
! C-----------------------------------------------------------------------
! C If CB%CM_H is being changed, the CB%CM_H ratio RH is checked against
! C CB%CM_RMAX, CB%CM_HMIN, and CB%CM_HMXI, and the YH array rescaled.  CB%CM_IALTH is set to
! C CB%CM_L = CB%CM_NQ + 1 to prevent a change of CB%CM_H for that many steps, unless
! C forced by a convergence or error test failure.
! C-----------------------------------------------------------------------
 160  IF (CB%CM_H .EQ. CB%CM_HOLD) GO TO 200
      RH = CB%CM_H/CB%CM_HOLD
      CB%CM_H = CB%CM_HOLD
      IREDO = 3
      GO TO 175
 170  RH = MAX(RH,CB%CM_HMIN/ABS(CB%CM_H))
 175  RH = MIN(RH,CB%CM_RMAX)
      RH = RH/MAX(1.0D0,ABS(CB%CM_H)*CB%CM_HMXI*RH)
! C-----------------------------------------------------------------------
! C If CB%CM_METH = 1, also restrict the new step size by the stability region.
! C If this reduces CB%CM_H, set CB%CM_IRFLAG to 1 so that if there are roundoff
! C problems later, we can assume that is the cause of the trouble.
! C-----------------------------------------------------------------------
      IF (CB%CM_METH .EQ. 2) GO TO 178
      CB%CM_IRFLAG = 0
      PDH = MAX(ABS(CB%CM_H)*CB%CM_PDLAST,0.000001D0)
      IF (RH*PDH*1.00001D0 .LT. CB%CM_SM1(CB%CM_NQ)) GO TO 178
      RH = CB%CM_SM1(CB%CM_NQ)/PDH
      CB%CM_IRFLAG = 1
 178  CONTINUE
      R = 1.0D0
      DO 180 J = 2,CB%CM_L
        R = R*RH
        DO 180 I = 1,CB%CM_N
 180      YH(I,J) = YH(I,J)*R
      CB%CM_H = CB%CM_H*RH
      CB%CM_RC = CB%CM_RC*RH
      CB%CM_IALTH = CB%CM_L
      IF (IREDO .EQ. 0) GO TO 690
! C-----------------------------------------------------------------------
! C This section computes the predicted values by effectively
! C multiplying the YH array by the Pascal triangle matrix.
! C CB%CM_RC is the ratio of new to old values of the coefficient  CB%CM_H*CB%CM_EL(1).
! C When CB%CM_RC differs from 1 by more than CB%CM_CCMAX, CB%CM_IPUP is set to CB%CM_MITER
! C to force PJAC to be called, if a Jacobian is involved.
! C In any case, PJAC is called at least every CB%CM_MSBP steps.
! C-----------------------------------------------------------------------
 200  IF (ABS(CB%CM_RC-1.0D0) .GT. CB%CM_CCMAX) CB%CM_IPUP = CB%CM_MITER
      IF (CB%CM_NST .GE. CB%CM_NSLP+CB%CM_MSBP) CB%CM_IPUP = CB%CM_MITER
      CB%CM_TN = CB%CM_TN + CB%CM_H
      I1 = CB%CM_NQNYH + 1
      DO 215 JB = 1,CB%CM_NQ
        I1 = I1 - NOT_NYH
! CDIR$ IVDEP
        DO 210 I = I1,CB%CM_NQNYH
 210      YH1(I) = YH1(I) + YH1(I+NOT_NYH)
 215    CONTINUE
      PNORM = DMNORM (CB%CM_N, YH1, EWT, CB)
! C-----------------------------------------------------------------------
! C Up to CB%CM_MAXCOR corrector iterations are taken.  A convergence test is
! C made on the RMS-norm of each correction, weighted by the error
! C weight vector EWT.  The sum of the corrections is accumulated in the
! C vector ACOR(i).  The YH array is not altered in the corrector loop.
! C-----------------------------------------------------------------------
 220  M = 0
      RATE = 0.0D0
      DEL = 0.0D0
      DO 230 I = 1,CB%CM_N
 230    Y(I) = YH(I,1)
      CALL F (NEQ, CB%CM_TN, Y, SAVF)
      CB%CM_NFE = CB%CM_NFE + 1
      IF (CB%CM_IPUP .LE. 0) GO TO 250
! C-----------------------------------------------------------------------
! C If indicated, the matrix P = I - CB%CM_H*CB%CM_EL(1)*J is reevaluated and
! C preprocessed before starting the corrector iteration.  CB%CM_IPUP is set
! C to 0 as an indicator that this has been done.
! C-----------------------------------------------------------------------
    !   CALL PJAC (NEQ, Y, YH, NOT_NYH, EWT, ACOR, SAVF, WM, IWM, F, JAC)
      CALL DPRJA (NEQ, Y, YH, NOT_NYH, EWT, ACOR, SAVF, WM, IWM, F, JAC, CB)
      CB%CM_IPUP = 0
      CB%CM_RC = 1.0D0
      CB%CM_NSLP = CB%CM_NST
      CB%CM_CRATE = 0.7D0
      IF (CB%CM_IERPJ .NE. 0) GO TO 430
 250  DO 260 I = 1,CB%CM_N
 260    ACOR(I) = 0.0D0
 270  IF (CB%CM_MITER .NE. 0) GO TO 350
! C-----------------------------------------------------------------------
! C In the case of functional iteration, update Y directly from
! C the result of the last function evaluation.
! C-----------------------------------------------------------------------
      DO 290 I = 1,CB%CM_N
        SAVF(I) = CB%CM_H*SAVF(I) - YH(I,2)
 290    Y(I) = SAVF(I) - ACOR(I)
      DEL = DMNORM (CB%CM_N, Y, EWT, CB)
      DO 300 I = 1,CB%CM_N
        Y(I) = YH(I,1) + CB%CM_EL(1)*SAVF(I)
 300    ACOR(I) = SAVF(I)
      GO TO 400
! C-----------------------------------------------------------------------
! C In the case of the chord method, compute the corrector error,
! C and solve the linear system with that as right-hand side and
! C P as coefficient matrix.
! C-----------------------------------------------------------------------
 350  DO 360 I = 1,CB%CM_N
 360    Y(I) = CB%CM_H*SAVF(I) - (YH(I,2) + ACOR(I))
    !   CALL SLVS (WM, IWM, Y, SAVF)
      CALL DSOLSY (WM, IWM, Y, SAVF, CB)
      IF (CB%CM_IERSL .LT. 0) GO TO 430
      IF (CB%CM_IERSL .GT. 0) GO TO 410
      DEL = DMNORM (CB%CM_N, Y, EWT, CB)
      DO 380 I = 1,CB%CM_N
        ACOR(I) = ACOR(I) + Y(I)
 380    Y(I) = YH(I,1) + CB%CM_EL(1)*ACOR(I)
! C-----------------------------------------------------------------------
! C Test for convergence.  If M .gt. 0, an estimate of the convergence
! C rate constant is stored in CB%CM_CRATE, and this is used in the test.
! C
! C We first check for a change of iterates that is the size of
! C roundoff error.  If this occurs, the iteration has converged, and a
! C new rate estimate is not formed.
! C In all other cases, force at least two iterations to estimate a
! C local Lipschitz constant estimate for Adams methods.
! C On convergence, form CB%CM_PDES = local maximum Lipschitz constant
! C estimate.  CB%CM_PDLAST is the most recent nonzero estimate.
! C-----------------------------------------------------------------------
 400  CONTINUE
      IF (DEL .LE. 100.0D0*PNORM*CB%CM_UROUND) GO TO 450
      IF (M .EQ. 0 .AND. CB%CM_METH .EQ. 1) GO TO 405
      IF (M .EQ. 0) GO TO 402
      RM = 1024.0D0
      IF (DEL .LE. 1024.0D0*DELP) RM = DEL/DELP
      RATE = MAX(RATE,RM)
      CB%CM_CRATE = MAX(0.2D0*CB%CM_CRATE,RM)
 402  DCON = DEL*MIN(1.0D0,1.5D0*CB%CM_CRATE)/(CB%CM_TESCO(2,CB%CM_NQ)*CB%CM_CONIT)
      IF (DCON .GT. 1.0D0) GO TO 405
      CB%CM_PDEST = MAX(CB%CM_PDEST,RATE/ABS(CB%CM_H*CB%CM_EL(1)))
      IF (CB%CM_PDEST .NE. 0.0D0) CB%CM_PDLAST = CB%CM_PDEST
      GO TO 450
 405  CONTINUE
      M = M + 1
      IF (M .EQ. CB%CM_MAXCOR) GO TO 410
      IF (M .GE. 2 .AND. DEL .GT. 2.0D0*DELP) GO TO 410
      DELP = DEL
      CALL F (NEQ, CB%CM_TN, Y, SAVF)
      CB%CM_NFE = CB%CM_NFE + 1
      GO TO 270
! C-----------------------------------------------------------------------
! C The corrector iteration failed to converge.
! C If CB%CM_MITER .ne. 0 and the Jacobian is out of date, PJAC is called for
! C the next try.  Otherwise the YH array is retracted to its values
! C before prediction, and CB%CM_H is reduced, if possible.  If CB%CM_H cannot be
! C reduced or CB%CM_MXNCF failures have occurred, exit with CB%CM_KFLAG = -2.
! C-----------------------------------------------------------------------
 410  IF (CB%CM_MITER .EQ. 0 .OR. CB%CM_JCUR .EQ. 1) GO TO 430
      CB%CM_ICF = 1
      CB%CM_IPUP = CB%CM_MITER
      GO TO 220
 430  CB%CM_ICF = 2
      NCF = NCF + 1
      CB%CM_RMAX = 2.0D0
      CB%CM_TN = TOLD
      I1 = CB%CM_NQNYH + 1
      DO 445 JB = 1,CB%CM_NQ
        I1 = I1 - NOT_NYH
! CDIR$ IVDEP
        DO 440 I = I1,CB%CM_NQNYH
 440      YH1(I) = YH1(I) - YH1(NOT_NYH)
 445    CONTINUE
      IF (CB%CM_IERPJ .LT. 0 .OR. CB%CM_IERSL .LT. 0) GO TO 680
      IF (ABS(CB%CM_H) .LE. CB%CM_HMIN*1.00001D0) GO TO 670
      IF (NCF .EQ. CB%CM_MXNCF) GO TO 670
      RH = 0.25D0
      CB%CM_IPUP = CB%CM_MITER
      IREDO = 1
      GO TO 170
! C-----------------------------------------------------------------------
! C The corrector has converged.  CB%CM_JCUR is set to 0
! C to signal that the Jacobian involved may need updating later.
! C The local error test is made and control passes to statement 500
! C if it fails.
! C-----------------------------------------------------------------------
 450  CB%CM_JCUR = 0
      IF (M .EQ. 0) DSM = DEL/CB%CM_TESCO(2,CB%CM_NQ)
      IF (M .GT. 0) DSM = DMNORM (CB%CM_N, ACOR, EWT, CB)/CB%CM_TESCO(2,CB%CM_NQ)
      IF (DSM .GT. 1.0D0) GO TO 500
! C-----------------------------------------------------------------------
! C After a successful step, update the YH array.
! C Decrease CB%CM_ICOUNT by 1, and if it is -1, consider switching methods.
! C If a method switch is made, reset various parameters,
! C rescale the YH array, and exit.  If there is no switch,
! C consider changing CB%CM_H if CB%CM_IALTH = 1.  Otherwise decrease CB%CM_IALTH by 1.
! C If CB%CM_IALTH is then 1 and CB%CM_NQ .lt. CB%CM_MAXORD, then ACOR is saved for
! C use in a possible order increase on the next step.
! C If a change in CB%CM_H is considered, an increase or decrease in order
! C by one is considered also.  A change in CB%CM_H is made only if it is by a
! C factor of at least 1.1.  If not, CB%CM_IALTH is set to 3 to prevent
! C testing for that many steps.
! C-----------------------------------------------------------------------
      CB%CM_KFLAG = 0
      IREDO = 0
      CB%CM_NST = CB%CM_NST + 1
      CB%CM_HU = CB%CM_H
      CB%CM_NQU = CB%CM_NQ
      CB%CM_MUSED = CB%CM_METH
      DO 460 J = 1,CB%CM_L
        DO 460 I = 1,CB%CM_N
 460      YH(I,J) = YH(I,J) + CB%CM_EL(J)*ACOR(I)
      CB%CM_ICOUNT = CB%CM_ICOUNT - 1
      IF (CB%CM_ICOUNT .GE. 0) GO TO 488
      IF (CB%CM_METH .EQ. 2) GO TO 480
! C-----------------------------------------------------------------------
! C We are currently using an Adams method.  Consider switching to BDF.
! C If the current order is greater than 5, assume the problem is
! C not stiff, and skip this section.
! C If the Lipschitz constant and error estimate are not polluted
! C by roundoff, go to 470 and perform the usual test.
! C Otherwise, switch to the BDF methods if the last step was
! C restricted to insure stability (irflag = 1), and stay with Adams
! C method if not.  When switching to BDF with polluted error estimates,
! C in the absence of other information, double the step size.
! C
! C When the estimates are OK, we make the usual test by computing
! C the step size we could have (ideally) used on this step,
! C with the current (Adams) method, and also that for the BDF.
! C If CB%CM_NQ .gt. CB%CM_MXORDS, we consider changing to order CB%CM_MXORDS on switching.
! C Compare the two step sizes to decide whether to switch.
! C The step size advantage must be at least CB%CM_RATIO = 5 to switch.
! C-----------------------------------------------------------------------
      IF (CB%CM_NQ .GT. 5) GO TO 488
      IF (DSM .GT. 100.0D0*PNORM*CB%CM_UROUND .AND. CB%CM_PDEST .NE. 0.0D0) &
         GO TO 470
      IF (CB%CM_IRFLAG .EQ. 0) GO TO 488
      RH2 = 2.0D0
      NQM2 = MIN(CB%CM_NQ,CB%CM_MXORDS)
      GO TO 478
 470  CONTINUE
      EXSM = 1.0D0/CB%CM_L
      RH1 = 1.0D0/(1.2D0*DSM**EXSM + 0.0000012D0)
      RH1IT = 2.0D0*RH1
      PDH = CB%CM_PDLAST*ABS(CB%CM_H)
      IF (PDH*RH1 .GT. 0.00001D0) RH1IT = CB%CM_SM1(CB%CM_NQ)/PDH
      RH1 = MIN(RH1,RH1IT)
      IF (CB%CM_NQ .LE. CB%CM_MXORDS) GO TO 474
         NQM2 = CB%CM_MXORDS
         LM2 = CB%CM_MXORDS + 1
         EXM2 = 1.0D0/LM2
         LM2P1 = LM2 + 1
         DM2 = DMNORM (CB%CM_N, YH(1,LM2P1), EWT, CB)/CB%CM_CM2(CB%CM_MXORDS)
         RH2 = 1.0D0/(1.2D0*DM2**EXM2 + 0.0000012D0)
         GO TO 476
 474  DM2 = DSM*(CB%CM_CM1(CB%CM_NQ)/CB%CM_CM2(CB%CM_NQ))
      RH2 = 1.0D0/(1.2D0*DM2**EXSM + 0.0000012D0)
      NQM2 = CB%CM_NQ
 476  CONTINUE
      IF (RH2 .LT. CB%CM_RATIO*RH1) GO TO 488
! C THE SWITCH TEST PASSED.  RESET RELEVANT QUANTITIES FOR BDF. ----------
 478  RH = RH2
      CB%CM_ICOUNT = 20
      CB%CM_METH = 2
      CB%CM_MITER = CB%CM_JTYP
      CB%CM_PDLAST = 0.0D0
      CB%CM_NQ = NQM2
      CB%CM_L = CB%CM_NQ + 1
      GO TO 170
! C-----------------------------------------------------------------------
! C We are currently using a BDF method.  Consider switching to Adams.
! C Compute the step size we could have (ideally) used on this step,
! C with the current (BDF) method, and also that for the Adams.
! C If CB%CM_NQ .gt. CB%CM_MXORDN, we consider changing to order CB%CM_MXORDN on switching.
! C Compare the two step sizes to decide whether to switch.
! C The step size advantage must be at least 5/CB%CM_RATIO = 1 to switch.
! C If the step size for Adams would be so small as to cause
! C roundoff pollution, we stay with BDF.
! C-----------------------------------------------------------------------
 480  CONTINUE
      EXSM = 1.0D0/CB%CM_L
      IF (CB%CM_MXORDN .GE. CB%CM_NQ) GO TO 484
         NQM1 = CB%CM_MXORDN
         LM1 = CB%CM_MXORDN + 1
         EXM1 = 1.0D0/LM1
         LM1P1 = LM1 + 1
         DM1 = DMNORM (CB%CM_N, YH(1,LM1P1), EWT, CB)/CB%CM_CM1(CB%CM_MXORDN)
         RH1 = 1.0D0/(1.2D0*DM1**EXM1 + 0.0000012D0)
         GO TO 486
 484  DM1 = DSM*(CB%CM_CM2(CB%CM_NQ)/CB%CM_CM1(CB%CM_NQ))
      RH1 = 1.0D0/(1.2D0*DM1**EXSM + 0.0000012D0)
      NQM1 = CB%CM_NQ
      EXM1 = EXSM
 486  RH1IT = 2.0D0*RH1
      PDH = CB%CM_PDNORM*ABS(CB%CM_H)
      IF (PDH*RH1 .GT. 0.00001D0) RH1IT = CB%CM_SM1(NQM1)/PDH
      RH1 = MIN(RH1,RH1IT)
      RH2 = 1.0D0/(1.2D0*DSM**EXSM + 0.0000012D0)
      IF (RH1*CB%CM_RATIO .LT. 5.0D0*RH2) GO TO 488
      ALPHA = MAX(0.001D0,RH1)
      DM1 = (ALPHA**EXM1)*DM1
      IF (DM1 .LE. 1000.0D0*CB%CM_UROUND*PNORM) GO TO 488
! C The switch test passed.  Reset relevant quantities for Adams. --------
      RH = RH1
      CB%CM_ICOUNT = 20
      CB%CM_METH = 1
      CB%CM_MITER = 0
      CB%CM_PDLAST = 0.0D0
      CB%CM_NQ = NQM1
      CB%CM_L = CB%CM_NQ + 1
      GO TO 170
! C
! C No method switch is being made.  Do the usual step/order selection. --
 488  CONTINUE
      CB%CM_IALTH = CB%CM_IALTH - 1
      IF (CB%CM_IALTH .EQ. 0) GO TO 520
      IF (CB%CM_IALTH .GT. 1) GO TO 700
      IF (CB%CM_L .EQ. CB%CM_LMAX) GO TO 700
      DO 490 I = 1,CB%CM_N
 490    YH(I,CB%CM_LMAX) = ACOR(I)
      GO TO 700
! C-----------------------------------------------------------------------
! C The error test failed.  CB%CM_KFLAG keeps track of multiple failures.
! C Restore CB%CM_TN and the YH array to their previous values, and prepare
! C to try the step again.  Compute the optimum step size for this or
! C one lower order.  After 2 or more failures, CB%CM_H is forced to decrease
! C by a factor of 0.2 or less.
! C-----------------------------------------------------------------------
 500  CB%CM_KFLAG = CB%CM_KFLAG - 1
      CB%CM_TN = TOLD
      I1 = CB%CM_NQNYH + 1
      DO 515 JB = 1,CB%CM_NQ
        I1 = I1 - NOT_NYH
! CDIR$ IVDEP
        DO 510 I = I1,CB%CM_NQNYH
 510      YH1(I) = YH1(I) - YH1(I+NOT_NYH)
 515    CONTINUE
      CB%CM_RMAX = 2.0D0
      IF (ABS(CB%CM_H) .LE. CB%CM_HMIN*1.00001D0) GO TO 660
      IF (CB%CM_KFLAG .LE. -3) GO TO 640
      IREDO = 2
      RHUP = 0.0D0
      GO TO 540
! C-----------------------------------------------------------------------
! C Regardless of the success or failure of the step, factors
! C RHDN, RHSM, and RHUP are computed, by which CB%CM_H could be multiplied
! C at order CB%CM_NQ - 1, order CB%CM_NQ, or order CB%CM_NQ + 1, respectively.
! C In the case of failure, RHUP = 0.0 to avoid an order increase.
! C The largest of these is determined and the new order chosen
! C accordingly.  If the order is to be increased, we compute one
! C additional scaled derivative.
! C-----------------------------------------------------------------------
 520  RHUP = 0.0D0
      IF (CB%CM_L .EQ. CB%CM_LMAX) GO TO 540
      DO 530 I = 1,CB%CM_N
 530    SAVF(I) = ACOR(I) - YH(I,CB%CM_LMAX)
      DUP = DMNORM (CB%CM_N, SAVF, EWT, CB)/CB%CM_TESCO(3,CB%CM_NQ)
      EXUP = 1.0D0/(CB%CM_L+1)
      RHUP = 1.0D0/(1.4D0*DUP**EXUP + 0.0000014D0)
 540  EXSM = 1.0D0/CB%CM_L
      RHSM = 1.0D0/(1.2D0*DSM**EXSM + 0.0000012D0)
      RHDN = 0.0D0
      IF (CB%CM_NQ .EQ. 1) GO TO 550
      DDN = DMNORM (CB%CM_N, YH(1,CB%CM_L), EWT, CB)/CB%CM_TESCO(1,CB%CM_NQ)
      EXDN = 1.0D0/CB%CM_NQ
      RHDN = 1.0D0/(1.3D0*DDN**EXDN + 0.0000013D0)
! C If CB%CM_METH = 1, limit RH according to the stability region also. --------
 550  IF (CB%CM_METH .EQ. 2) GO TO 560
      PDH = MAX(ABS(CB%CM_H)*CB%CM_PDLAST,0.000001D0)
      IF (CB%CM_L .LT. CB%CM_LMAX) RHUP = MIN(RHUP,CB%CM_SM1(CB%CM_L)/PDH)
      RHSM = MIN(RHSM,CB%CM_SM1(CB%CM_NQ)/PDH)
      IF (CB%CM_NQ .GT. 1) RHDN = MIN(RHDN,CB%CM_SM1(CB%CM_NQ-1)/PDH)
      CB%CM_PDEST = 0.0D0
 560  IF (RHSM .GE. RHUP) GO TO 570
      IF (RHUP .GT. RHDN) GO TO 590
      GO TO 580
 570  IF (RHSM .LT. RHDN) GO TO 580
      NEWQ = CB%CM_NQ
      RH = RHSM
      GO TO 620
 580  NEWQ = CB%CM_NQ - 1
      RH = RHDN
      IF (CB%CM_KFLAG .LT. 0 .AND. RH .GT. 1.0D0) RH = 1.0D0
      GO TO 620
 590  NEWQ = CB%CM_L
      RH = RHUP
      IF (RH .LT. 1.1D0) GO TO 610
      R = CB%CM_EL(CB%CM_L)/CB%CM_L
      DO 600 I = 1,CB%CM_N
 600    YH(I,NEWQ+1) = ACOR(I)*R
      GO TO 630
 610  CB%CM_IALTH = 3
      GO TO 700
! C If CB%CM_METH = 1 and CB%CM_H is restricted by stability, bypass 10 percent test.
 620  IF (CB%CM_METH .EQ. 2) GO TO 622
      IF (RH*PDH*1.00001D0 .GE. CB%CM_SM1(NEWQ)) GO TO 625
 622  IF (CB%CM_KFLAG .EQ. 0 .AND. RH .LT. 1.1D0) GO TO 610
 625  IF (CB%CM_KFLAG .LE. -2) RH = MIN(RH,0.2D0)
! C-----------------------------------------------------------------------
! C If there is a change of order, reset CB%CM_NQ, CB%CM_L, and the coefficients.
! C In any case CB%CM_H is reset according to RH and the YH array is rescaled.
! C Then exit from 690 if the step was OK, or redo the step otherwise.
! C-----------------------------------------------------------------------
      IF (NEWQ .EQ. CB%CM_NQ) GO TO 170
 630  CB%CM_NQ = NEWQ
      CB%CM_L = CB%CM_NQ + 1
      IRET = 2
      GO TO 150
! C-----------------------------------------------------------------------
! C Control reaches this section if 3 or more failures have occured.
! C If 10 failures have occurred, exit with CB%CM_KFLAG = -1.
! C It is assumed that the derivatives that have accumulated in the
! C YH array have errors of the wrong order.  Hence the first
! C derivative is recomputed, and the order is set to 1.  Then
! C CB%CM_H is reduced by a factor of 10, and the step is retried,
! C until it succeeds or CB%CM_H reaches CB%CM_HMIN.
! C-----------------------------------------------------------------------
 640  IF (CB%CM_KFLAG .EQ. -10) GO TO 660
      RH = 0.1D0
      RH = MAX(CB%CM_HMIN/ABS(CB%CM_H),RH)
      CB%CM_H = CB%CM_H*RH
      DO 645 I = 1,CB%CM_N
 645    Y(I) = YH(I,1)
      CALL F (NEQ, CB%CM_TN, Y, SAVF)
      CB%CM_NFE = CB%CM_NFE + 1
      DO 650 I = 1,CB%CM_N
 650    YH(I,2) = CB%CM_H*SAVF(I)
      CB%CM_IPUP = CB%CM_MITER
      CB%CM_IALTH = 5
      IF (CB%CM_NQ .EQ. 1) GO TO 200
      CB%CM_NQ = 1
      CB%CM_L = 2
      IRET = 3
      GO TO 150
! C-----------------------------------------------------------------------
! C All returns are made through this section.  CB%CM_H is saved in CB%CM_HOLD
! C to allow the caller to change CB%CM_H on the next step.
! C-----------------------------------------------------------------------
 660  CB%CM_KFLAG = -1
      GO TO 720
 670  CB%CM_KFLAG = -2
      GO TO 720
 680  CB%CM_KFLAG = -3
      GO TO 720
 690  CB%CM_RMAX = 10.0D0
 700  R = 1.0D0/CB%CM_TESCO(2,CB%CM_NQU)
      DO 710 I = 1,CB%CM_N
 710    ACOR(I) = ACOR(I)*R
 720  CB%CM_HOLD = CB%CM_H
      CB%CM_JSTART = 1
      RETURN
! C----------------------- End of Subroutine DSTODA ----------------------
      END
! *DECK DCFODE
      ATTRIBUTES(DEVICE) SUBROUTINE DCFODE (DCF_METH, DCF_ELCO, DCF_TESCO, CB)
! C***BEGIN PROLOGUE  DCFODE
! C***SUBSIDIARY
! C***PURPOSE  Set ODE integrator coefficients.
! C***TYPE      DOUBLE PRECISION (SCFODE-S, DCFODE-D)
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C
! C  DCFODE is called by the integrator routine to set coefficients
! C  needed there.  The coefficients for the current method, as
! C  given by the value of CB%CM_METH, are set for all orders and saved.
! C  The maximum order assumed here is 12 if CB%CM_METH = 1 and 5 if CB%CM_METH = 2.
! C  (A smaller value of the maximum order is also allowed.)
! C  DCFODE is called once at the beginning of the problem,
! C  and is not called again unless and until CB%CM_METH is changed.
! C
! C  The CB%CM_ELCO array contains the basic method coefficients.
! C  The coefficients el(i), 1 .le. i .le. nq+1, for the method of
! C  order nq are stored in CB%CM_ELCO(i,nq).  They are given by a genetrating
! C  polynomial, i.e.,
! C      l(x) = el(1) + el(2)*x + ... + el(nq+1)*x**nq.
! C  For the implicit Adams methods, l(x) is given by
! C      dl/dx = (x+1)*(x+2)*...*(x+nq-1)/factorial(nq-1),    l(-1) = 0.
! C  For the BDF methods, l(x) is given by
! C      l(x) = (x+1)*(x+2)* ... *(x+nq)/K,
! C  where         K = factorial(nq)*(1 + 1/2 + ... + 1/nq).
! C
! C  The CB%CM_TESCO array contains test constants used for the
! C  local error test and the selection of step size and/or order.
! C  At order nq, CB%CM_TESCO(k,nq) is used for the selection of step
! C  size at order nq - 1 if k = 1, at order nq if k = 2, and at order
! C  nq + 1 if k = 3.
! C
! C***SEE ALSO  DLSODE
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791129  DATE WRITTEN
! C   890501  Modified prologue to SLATEC/LDOC format.  (FNF)
! C   890503  Minor cosmetic changes.  (FNF)
! C   930809  Renamed to allow single/double precision versions. (ACH)
! C***END PROLOGUE  DCFODE
! C**End
      INTEGER DCF_METH
      INTEGER I, IB, DCF_NQ, NQM1, NQP1
      DOUBLE PRECISION DCF_ELCO, DCF_TESCO
      TYPE(commonBlock) CB
      DOUBLE PRECISION AGAMQ, FNQ, FNQM1, PC, PINT, RAGQ, &
         RQFAC, RQ1FAC, TSIGN, XPIN
      DIMENSION DCF_ELCO(13,12), DCF_TESCO(3,12)
      DIMENSION PC(12)
! C
! C***FIRST EXECUTABLE STATEMENT  DCFODE
      GO TO (100, 200), DCF_METH
! C
 100  DCF_ELCO(1,1) = 1.0D0
      DCF_ELCO(2,1) = 1.0D0
      DCF_TESCO(1,1) = 0.0D0
      DCF_TESCO(2,1) = 2.0D0
      DCF_TESCO(1,2) = 1.0D0
      DCF_TESCO(3,12) = 0.0D0
      PC(1) = 1.0D0
      RQFAC = 1.0D0
      DO 140 DCF_NQ = 2,12
! C-----------------------------------------------------------------------
! C The PC array will contain the coefficients of the polynomial
! C     p(x) = (x+1)*(x+2)*...*(x+nq-1).
! C Initially, p(x) = 1.
! C-----------------------------------------------------------------------
        RQ1FAC = RQFAC
        RQFAC = RQFAC/DCF_NQ
        NQM1 = DCF_NQ - 1
        FNQM1 = NQM1
        NQP1 = DCF_NQ + 1
! C Form coefficients of p(x)*(x+nq-1). ----------------------------------
        PC(DCF_NQ) = 0.0D0
        DO 110 IB = 1,NQM1
          I = NQP1 - IB
 110      PC(I) = PC(I-1) + FNQM1*PC(I)
        PC(1) = FNQM1*PC(1)
! C Compute integral, -1 to 0, of p(x) and x*p(x). -----------------------
        PINT = PC(1)
        XPIN = PC(1)/2.0D0
        TSIGN = 1.0D0
        DO 120 I = 2,DCF_NQ
          TSIGN = -TSIGN
          PINT = PINT + TSIGN*PC(I)/I
 120      XPIN = XPIN + TSIGN*PC(I)/(I+1)
! C Store coefficients in DCF_ELCO and DCF_TESCO. --------------------------------
        DCF_ELCO(1,DCF_NQ) = PINT*RQ1FAC
        DCF_ELCO(2,DCF_NQ) = 1.0D0
        DO 130 I = 2,DCF_NQ
 130      DCF_ELCO(I+1,DCF_NQ) = RQ1FAC*PC(I)/I
        AGAMQ = RQFAC*XPIN
        RAGQ = 1.0D0/AGAMQ
        DCF_TESCO(2,DCF_NQ) = RAGQ
        IF (DCF_NQ .LT. 12) DCF_TESCO(1,NQP1) = RAGQ*RQFAC/NQP1
        DCF_TESCO(3,NQM1) = RAGQ
 140    CONTINUE
      RETURN
! C
 200  PC(1) = 1.0D0
      RQ1FAC = 1.0D0
      DO 230 DCF_NQ = 1,5
! C-----------------------------------------------------------------------
! C The PC array will contain the coefficients of the polynomial
! C     p(x) = (x+1)*(x+2)*...*(x+nq).
! C Initially, p(x) = 1.
! C-----------------------------------------------------------------------
        FNQ = DCF_NQ
        NQP1 = DCF_NQ + 1
! C Form coefficients of p(x)*(x+nq). ------------------------------------
        PC(NQP1) = 0.0D0
        DO 210 IB = 1,DCF_NQ
          I = DCF_NQ + 2 - IB
 210      PC(I) = PC(I-1) + FNQ*PC(I)
        PC(1) = FNQ*PC(1)
! C Store coefficients in DCF_ELCO and DCF_TESCO. --------------------------------
        DO 220 I = 1,NQP1
 220      DCF_ELCO(I,DCF_NQ) = PC(I)/PC(2)
        DCF_ELCO(2,DCF_NQ) = 1.0D0
        DCF_TESCO(1,DCF_NQ) = RQ1FAC
        DCF_TESCO(2,DCF_NQ) = NQP1/DCF_ELCO(1,DCF_NQ)
        DCF_TESCO(3,DCF_NQ) = (DCF_NQ+2)/DCF_ELCO(1,DCF_NQ)
        RQ1FAC = RQ1FAC/FNQ
 230    CONTINUE
      RETURN
! C----------------------- END OF SUBROUTINE DCFODE ----------------------
      END
! *DECK DPRJA
      ATTRIBUTES(DEVICE) SUBROUTINE DPRJA (NEQ, Y, YH, NOT_NYH, EWT, FTEM, SAVF, WM, IWM, &
         F, JAC, CB)
      !!!EXTERNAL F, JAC
      INTEGER NEQ, NOT_NYH, IWM
      DOUBLE PRECISION Y, YH, EWT, FTEM, SAVF, WM
      DIMENSION NEQ(*), Y(*), YH(NOT_NYH,*), EWT(*), FTEM(*), SAVF(*), &
         WM(*), IWM(*)
      TYPE(commonBlock) CB
      interface
        attributes(device) subroutine F (NEQ, TT, NIN, DN)
          implicit none
          integer NEQ(*)
          double precision TT, NIN(*), DN(*)
        end subroutine F
        attributes(device) subroutine JAC (NEQ, T, Y, ML, MU, PD, NROWPD)
          implicit none
          integer NEQ(*), NROWPD, ML, MU
          double precision T, Y(*), PD(NROWPD,*)
        end subroutine JAC
      end interface
    !   INTEGER IOWND, IOWNS, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   INTEGER IOWND2, IOWNS2, CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
    !   DOUBLE PRECISION ROWNS, &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND
    !   DOUBLE PRECISION ROWND2, ROWNS2, CB%CM_PDNORM
    !   COMMON /DLS001/ ROWNS(209), &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND, &
    !      IOWND(6), IOWNS(6), &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   COMMON /DLSA01/ ROWND2, ROWNS2(20), CB%CM_PDNORM, &
    !      IOWND2(3), IOWNS2(2), CB%CM_JTYP, CB%CM_MUSED, CB%CM_MXORDN, CB%CM_MXORDS
      INTEGER I, I1, I2, IER, II, J, J1, JJ, LENP, &
         MBA, MBAND, MEB1, MEBAND, ML, ML3, MU, NP1
      DOUBLE PRECISION CON, FAC, HL0, R, R0, SRUR, YI, YJ, YJJ, &
         DMNORM, DFNORM, DBNORM
      !!!$OMP THREADPRIVATE(/DLS001/,/DLSA01/)
! C-----------------------------------------------------------------------
! C DPRJA is called by DSTODA to compute and process the matrix
! C P = I - CB%CM_H*CB%CM_EL(1)*J , where J is an approximation to the Jacobian.
! C Here J is computed by the user-supplied routine JAC if
! C CB%CM_MITER = 1 or 4 or by finite differencing if CB%CM_MITER = 2 or 5.
! C J, scaled by -CB%CM_H*CB%CM_EL(1), is stored in WM.  Then the norm of J (the
! C matrix norm consistent with the weighted max-norm on vectors given
! C by DMNORM) is computed, and J is overwritten by P.  P is then
! C subjected to LU decomposition in preparation for later solution
! C of linear systems with P as coefficient matrix.  This is done
! C by DGEFA if CB%CM_MITER = 1 or 2, and by DGBFA if CB%CM_MITER = 4 or 5.
! C
! C In addition to variables described previously, communication
! C with DPRJA uses the following:
! C Y     = array containing predicted values on entry.
! C FTEM  = work array of length CB%CM_N (ACOR in DSTODA).
! C SAVF  = array containing f evaluated at predicted y.
! C WM    = real work space for matrices.  On output it contains the
! C         LU decomposition of P.
! C         Storage of matrix elements starts at WM(3).
! C         WM also contains the following matrix-related data:
! C         WM(1) = SQRT(CB%CM_UROUND), used in numerical Jacobian increments.
! C IWM   = integer work space containing pivot information, starting at
! C         IWM(21).   IWM also contains the band parameters
! C         ML = IWM(1) and MU = IWM(2) if CB%CM_MITER is 4 or 5.
! C CB%CM_EL0   = CB%CM_EL(1) (input).
! C CB%CM_PDNORM= norm of Jacobian matrix. (Output).
! C CB%CM_IERPJ = output error flag,  = 0 if no trouble, .gt. 0 if
! C         P matrix found to be singular.
! C CB%CM_JCUR  = output flag = 1 to indicate that the Jacobian matrix
! C         (or approximation) is now current.
! C This routine also uses the Common variables CB%CM_EL0, CB%CM_H, CB%CM_TN, CB%CM_UROUND,
! C CB%CM_MITER, CB%CM_N, CB%CM_NFE, and CB%CM_NJE.
! C-----------------------------------------------------------------------
      CB%CM_NJE = CB%CM_NJE + 1
      CB%CM_IERPJ = 0
      CB%CM_JCUR = 1
      HL0 = CB%CM_H*CB%CM_EL0
      GO TO (100, 200, 300, 400, 500), CB%CM_MITER
! C If CB%CM_MITER = 1, call JAC and multiply by scalar. -----------------------
 100  LENP = CB%CM_N*CB%CM_N
      DO 110 I = 1,LENP
 110    WM(I+2) = 0.0D0
      CALL JAC (NEQ, CB%CM_TN, Y, 0, 0, WM(3), CB%CM_N)
      CON = -HL0
      DO 120 I = 1,LENP
 120    WM(I+2) = WM(I+2)*CON
      GO TO 240
! C If CB%CM_MITER = 2, make N calls to F to approximate J. --------------------
 200  FAC = DMNORM (CB%CM_N, SAVF, EWT, CB)
      R0 = 1000.0D0*ABS(CB%CM_H)*CB%CM_UROUND*CB%CM_N*FAC
      IF (R0 .EQ. 0.0D0) R0 = 1.0D0
      SRUR = WM(1)
      J1 = 2
      DO 230 J = 1,CB%CM_N
        YJ = Y(J)
        R = MAX(SRUR*ABS(YJ),R0/EWT(J))
        Y(J) = Y(J) + R
        FAC = -HL0/R
        CALL F (NEQ, CB%CM_TN, Y, FTEM)
        DO 220 I = 1,CB%CM_N
 220      WM(I+J1) = (FTEM(I) - SAVF(I))*FAC
        Y(J) = YJ
        J1 = J1 + CB%CM_N
 230    CONTINUE
      CB%CM_NFE = CB%CM_NFE + CB%CM_N
 240  CONTINUE
! C Compute norm of Jacobian. --------------------------------------------
      CB%CM_PDNORM = DFNORM (CB%CM_N, WM(3), EWT, CB)/ABS(HL0)
! C Add identity matrix. -------------------------------------------------
      J = 3
      NP1 = CB%CM_N + 1
      DO 250 I = 1,CB%CM_N
        WM(J) = WM(J) + 1.0D0
 250    J = J + NP1
! C Do LU decomposition on P. --------------------------------------------
      CALL DGEFA (WM(3), CB%CM_N, CB%CM_N, IWM(21), IER, CB)
      IF (IER .NE. 0) CB%CM_IERPJ = 1
      RETURN
! C Dummy block only, since CB%CM_MITER is never 3 in this routine. ------------
 300  RETURN
! C If CB%CM_MITER = 4, call JAC and multiply by scalar. -----------------------
 400  ML = IWM(1)
      MU = IWM(2)
      ML3 = ML + 3
      MBAND = ML + MU + 1
      MEBAND = MBAND + ML
      LENP = MEBAND*CB%CM_N
      DO 410 I = 1,LENP
 410    WM(I+2) = 0.0D0
      CALL JAC (NEQ, CB%CM_TN, Y, ML, MU, WM(ML3), MEBAND)
      CON = -HL0
      DO 420 I = 1,LENP
 420    WM(I+2) = WM(I+2)*CON
      GO TO 570
! C If CB%CM_MITER = 5, make MBAND calls to F to approximate J. ----------------
 500  ML = IWM(1)
      MU = IWM(2)
      MBAND = ML + MU + 1
      MBA = MIN(MBAND,CB%CM_N)
      MEBAND = MBAND + ML
      MEB1 = MEBAND - 1
      SRUR = WM(1)
      FAC = DMNORM (CB%CM_N, SAVF, EWT, CB)
      R0 = 1000.0D0*ABS(CB%CM_H)*CB%CM_UROUND*CB%CM_N*FAC
      IF (R0 .EQ. 0.0D0) R0 = 1.0D0
      DO 560 J = 1,MBA
        DO 530 I = J,CB%CM_N,MBAND
          YI = Y(I)
          R = MAX(SRUR*ABS(YI),R0/EWT(I))
 530      Y(I) = Y(I) + R
        CALL F (NEQ, CB%CM_TN, Y, FTEM)
        DO 550 JJ = J,CB%CM_N,MBAND
          Y(JJ) = YH(JJ,1)
          YJJ = Y(JJ)
          R = MAX(SRUR*ABS(YJJ),R0/EWT(JJ))
          FAC = -HL0/R
          I1 = MAX(JJ-MU,1)
          I2 = MIN(JJ+ML,CB%CM_N)
          II = JJ*MEB1 - ML + 2
          DO 540 I = I1,I2
 540        WM(II+I) = (FTEM(I) - SAVF(I))*FAC
 550      CONTINUE
 560    CONTINUE
      CB%CM_NFE = CB%CM_NFE + MBA
 570  CONTINUE
! C Compute norm of Jacobian. --------------------------------------------
      CB%CM_PDNORM = DBNORM (CB%CM_N, WM(ML+3), MEBAND, ML, MU, EWT, CB)/ABS(HL0)
! C Add identity matrix. -------------------------------------------------
      II = MBAND + 2
      DO 580 I = 1,CB%CM_N
        WM(II) = WM(II) + 1.0D0
 580    II = II + MEBAND
! C Do LU decomposition of P. --------------------------------------------
      CALL DGBFA (WM(3), MEBAND, CB%CM_N, ML, MU, IWM(21), IER, CB)
      IF (IER .NE. 0) CB%CM_IERPJ = 1
      RETURN
! C----------------------- End of Subroutine DPRJA -----------------------
      END
! *DECK DSOLSY
      ATTRIBUTES(DEVICE) SUBROUTINE DSOLSY (WM, IWM, X, TEM, CB)
! C***BEGIN PROLOGUE  DSOLSY
! C***SUBSIDIARY
! C***PURPOSE  ODEPACK linear system solver.
! C***TYPE      DOUBLE PRECISION (SSOLSY-S, DSOLSY-D)
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C
! C  This routine manages the solution of the linear system arising from
! C  a chord iteration.  It is called if CB%CM_MITER .ne. 0.
! C  If CB%CM_MITER is 1 or 2, it calls DGESL to accomplish this.
! C  If CB%CM_MITER = 3 it updates the coefficient h*CB%CM_EL0 in the diagonal
! C  matrix, and then computes the solution.
! C  If CB%CM_MITER is 4 or 5, it calls DGBSL.
! C  Communication with DSOLSY uses the following variables:
! C  WM    = real work space containing the inverse diagonal matrix if
! C          CB%CM_MITER = 3 and the LU decomposition of the matrix otherwise.
! C          Storage of matrix elements starts at WM(3).
! C          WM also contains the following matrix-related data:
! C          WM(1) = SQRT(CB%CM_UROUND) (not used here),
! C          WM(2) = HL0, the previous value of h*CB%CM_EL0, used if CB%CM_MITER = 3.
! C  IWM   = integer work space containing pivot information, starting at
! C          IWM(21), if CB%CM_MITER is 1, 2, 4, or 5.  IWM also contains band
! C          parameters ML = IWM(1) and MU = IWM(2) if CB%CM_MITER is 4 or 5.
! C  X     = the right-hand side vector on input, and the solution vector
! C          on output, of length CB%CM_N.
! C  TEM   = vector of work space of length CB%CM_N, not used in this version.
! C  CB%CM_IERSL = output flag (in COMMON).  CB%CM_IERSL = 0 if no trouble occurred.
! C          CB%CM_IERSL = 1 if a singular matrix arose with CB%CM_MITER = 3.
! C  This routine also uses the COMMON variables CB%CM_EL0, CB%CM_H, CB%CM_MITER, and CB%CM_N.
! C
! C***SEE ALSO  DLSODE
! C***ROUTINES CALLED  DGBSL, DGESL
! C***COMMON BLOCKS    DLS001
! C***REVISION HISTORY  (YYMMDD)
! C   791129  DATE WRITTEN
! C   890501  Modified prologue to SLATEC/LDOC format.  (FNF)
! C   890503  Minor cosmetic changes.  (FNF)
! C   930809  Renamed to allow single/double precision versions. (ACH)
! C   010418  Reduced size of Common block /DLS001/. (ACH)
! C   031105  Restored 'own' variables to Common block /DLS001/, to
! C           enable interrupt/restart feature. (ACH)
! C***END PROLOGUE  DSOLSY
! C**End
      INTEGER IWM
      DOUBLE PRECISION WM, X, TEM
      DIMENSION WM(*), IWM(*), X(*), TEM(*)
      TYPE(commonBlock) CB
    !   INTEGER IOWND, IOWNS, &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
    !   DOUBLE PRECISION ROWNS, &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND
    !   COMMON /DLS001/ ROWNS(209), &
    !      CB%CM_CCMAX, CB%CM_EL0, CB%CM_H, CB%CM_HMIN, CB%CM_HMXI, CB%CM_HU, CB%CM_RC, CB%CM_TN, CB%CM_UROUND, &
    !      IOWND(6), IOWNS(6), &
    !      CB%CM_ICF, CB%CM_IERPJ, CB%CM_IERSL, CB%CM_JCUR, CB%CM_JSTART, CB%CM_KFLAG, CB%CM_L, &
    !      CB%CM_LYH, CB%CM_LEWT, CB%CM_LACOR, CB%CM_LSAVF, CB%CM_LWM, CB%CM_LIWM, CB%CM_METH, CB%CM_MITER, &
    !      CB%CM_MAXORD, CB%CM_MAXCOR, CB%CM_MSBP, CB%CM_MXNCF, CB%CM_N, CB%CM_NQ, CB%CM_NST, CB%CM_NFE, CB%CM_NJE, CB%CM_NQU
      INTEGER I, MEBAND, ML, MU
      DOUBLE PRECISION DI, HL0, PHL0, R
      !!!$OMP THREADPRIVATE(/DLS001/)
! C
! C***FIRST EXECUTABLE STATEMENT  DSOLSY
      CB%CM_IERSL = 0
      GO TO (100, 100, 300, 400, 400), CB%CM_MITER
 100  CALL DGESL (WM(3), CB%CM_N, CB%CM_N, IWM(21), X, 0, CB)
      RETURN
! C
 300  PHL0 = WM(2)
      HL0 = CB%CM_H*CB%CM_EL0
      WM(2) = HL0
      IF (HL0 .EQ. PHL0) GO TO 330
      R = HL0/PHL0
      DO 320 I = 1,CB%CM_N
        DI = 1.0D0 - R*(1.0D0 - 1.0D0/WM(I+2))
        IF (ABS(DI) .EQ. 0.0D0) GO TO 390
 320    WM(I+2) = 1.0D0/DI
 330  DO 340 I = 1,CB%CM_N
 340    X(I) = WM(I+2)*X(I)
      RETURN
 390  CB%CM_IERSL = 1
      RETURN
! C
 400  ML = IWM(1)
      MU = IWM(2)
      MEBAND = 2*ML + MU + 1
      CALL DGBSL (WM(3), MEBAND, CB%CM_N, ML, MU, IWM(21), X, 0, CB)
      RETURN
! C----------------------- END OF SUBROUTINE DSOLSY ----------------------
      END
! *DECK DEWSET
      ATTRIBUTES(DEVICE) SUBROUTINE DEWSET (DEW_N, ITOL, RTOL, ATOL, YCUR, EWT, CB)
! C***BEGIN PROLOGUE  DEWSET
! C***SUBSIDIARY
! C***PURPOSE  Set error weight vector.
! C***TYPE      DOUBLE PRECISION (SEWSET-S, DEWSET-D)
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C
! C  This subroutine sets the error weight vector EWT according to
! C      EWT(i) = RTOL(i)*ABS(YCUR(i)) + ATOL(i),  i = 1,...,CB%CM_N,
! C  with the subscript on RTOL and/or ATOL possibly replaced by 1 above,
! C  depending on the value of ITOL.
! C
! C***SEE ALSO  DLSODE
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791129  DATE WRITTEN
! C   890501  Modified prologue to SLATEC/LDOC format.  (FNF)
! C   890503  Minor cosmetic changes.  (FNF)
! C   930809  Renamed to allow single/double precision versions. (ACH)
! C***END PROLOGUE  DEWSET
! C**End
      INTEGER DEW_N, ITOL
      INTEGER I
      DOUBLE PRECISION RTOL, ATOL, YCUR, EWT
      DIMENSION RTOL(*), ATOL(*), YCUR(DEW_N), EWT(DEW_N)
      TYPE(commonBlock) CB
! C
! C***FIRST EXECUTABLE STATEMENT  DEWSET
      GO TO (10, 20, 30, 40), ITOL
 10   CONTINUE
      DO 15 I = 1,DEW_N
 15     EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(1)
      RETURN
 20   CONTINUE
      DO 25 I = 1,DEW_N
 25     EWT(I) = RTOL(1)*ABS(YCUR(I)) + ATOL(I)
      RETURN
 30   CONTINUE
      DO 35 I = 1,DEW_N
 35     EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(1)
      RETURN
 40   CONTINUE
      DO 45 I = 1,DEW_N
 45     EWT(I) = RTOL(I)*ABS(YCUR(I)) + ATOL(I)
      RETURN
! C----------------------- END OF SUBROUTINE DEWSET ----------------------
      END
! *DECK DMNORM
      ATTRIBUTES(DEVICE) DOUBLE PRECISION FUNCTION DMNORM (DM_N, V, W, CB)
! C-----------------------------------------------------------------------
! C This function routine computes the weighted max-norm
! C of the vector of length CB%CM_N contained in the array V, with weights
! C contained in the array w of length CB%CM_N:
! C   DMNORM = MAX(i=1,...,CB%CM_N) ABS(V(i))*W(i)
! C-----------------------------------------------------------------------
      INTEGER DM_N,   I
      DOUBLE PRECISION V, W, VM
      DIMENSION V(DM_N), W(DM_N)
      TYPE(commonBlock) CB
      VM = 0.0D0
      DO 10 I = 1,DM_N
 10     VM = MAX(VM,ABS(V(I))*W(I))
      DMNORM = VM
      RETURN
! C----------------------- End of Function DMNORM ------------------------
      END
! *DECK DFNORM
      ATTRIBUTES(DEVICE) DOUBLE PRECISION FUNCTION DFNORM (DF_N, A, W, CB)
! C-----------------------------------------------------------------------
! C This function computes the norm of a full CB%CM_N by CB%CM_N matrix,
! C stored in the array A, that is consistent with the weighted max-norm
! C on vectors, with weights stored in the array W:
! C   DFNORM = MAX(i=1,...,CB%CM_N) ( W(i) * Sum(j=1,...,CB%CM_N) ABS(a(i,j))/W(j) )
! C-----------------------------------------------------------------------
      INTEGER DF_N, I, J
      DOUBLE PRECISION A, W, AN, SUM
      DIMENSION A(DF_N,DF_N), W(DF_N)
      TYPE(commonBlock) CB
      AN = 0.0D0
      DO 20 I = 1,DF_N
        SUM = 0.0D0
        DO 10 J = 1,DF_N
 10       SUM = SUM + ABS(A(I,J))/W(J)
        AN = MAX(AN,SUM*W(I))
 20     CONTINUE
      DFNORM = AN
      RETURN
! C----------------------- End of Function DFNORM ------------------------
      END
! *DECK DBNORM
      ATTRIBUTES(DEVICE) DOUBLE PRECISION FUNCTION DBNORM (DB_N, A, NRA, ML, MU, W, CB)
! C-----------------------------------------------------------------------
! C This function computes the norm of a banded CB%CM_N by CB%CM_N matrix,
! C stored in the array A, that is consistent with the weighted max-norm
! C on vectors, with weights stored in the array W.
! C ML and MU are the lower and upper half-bandwidths of the matrix.
! C NRA is the first dimension of the A array, NRA .ge. ML+MU+1.
! C In terms of the matrix elements a(i,j), the norm is given by:
! C   DBNORM = MAX(i=1,...,CB%CM_N) ( W(i) * Sum(j=1,...,CB%CM_N) ABS(a(i,j))/W(j) )
! C-----------------------------------------------------------------------
      INTEGER DB_N, NRA, ML, MU
      INTEGER I, I1, JLO, JHI, J
      DOUBLE PRECISION A, W
      DOUBLE PRECISION AN, SUM
      DIMENSION A(NRA,DB_N), W(DB_N)
      TYPE(commonBlock) CB
      AN = 0.0D0
      DO 20 I = 1,DB_N
        SUM = 0.0D0
        I1 = I + MU + 1
        JLO = MAX(I-ML,1)
        JHI = MIN(I+MU,DB_N)
        DO 10 J = JLO,JHI
 10       SUM = SUM + ABS(A(I1-J,J))/W(J)
        AN = MAX(AN,SUM*W(I))
 20     CONTINUE
      DBNORM = AN
      RETURN
! C----------------------- End of Function DBNORM ------------------------
      END
! *DECK DSRCMA
      ATTRIBUTES(DEVICE) SUBROUTINE DSRCMA (RSAV, ISAV, JOB, CB)
! C-----------------------------------------------------------------------
! C This routine saves or restores (depending on JOB) the contents of
! C the Common blocks DLS001, DLSA01, which are used
! C internally by one or more ODEPACK solvers.
! C
! C RSAV = real array of length 240 or more.
! C ISAV = integer array of length 46 or more.
! C JOB  = flag indicating to save or restore the Common blocks:
! C        JOB  = 1 if Common is to be saved (written to RSAV/ISAV)
! C        JOB  = 2 if Common is to be restored (read from RSAV/ISAV)
! C        A call with JOB = 2 presumes a prior call with JOB = 1.
! C-----------------------------------------------------------------------
      INTEGER ISAV, JOB
    !   INTEGER CB%CM_ILS, CB%CM_ILSA
      INTEGER I, LENRLS, LENILS, LENRLA, LENILA
      DOUBLE PRECISION RSAV
    !   DOUBLE PRECISION CB%CM_RLS, CB%CM_RLSA
      DIMENSION RSAV(*), ISAV(*)
      TYPE(commonBlock) CB
      SAVE LENRLS, LENILS, LENRLA, LENILA
    !   COMMON /DLS001/ CB%CM_RLS(218), CB%CM_ILS(37)
    !   COMMON /DLSA01/ CB%CM_RLSA(22), CB%CM_ILSA(9)
      !DATA LENRLS/218/, LENILS/37/, LENRLA/22/, LENILA/9/
      !!!$OMP THREADPRIVATE(/DLS001/,/DLSA01/)
      LENRLS = 218
      LENILS = 37
      LENRLA = 22
      LENILA = 9
! C
      IF (JOB .EQ. 2) GO TO 100
      DO 10 I = 1,LENRLS
 10     RSAV(I) = CB%CM_RLS(I)
      DO 15 I = 1,LENRLA
 15     RSAV(LENRLS+I) = CB%CM_RLSA(I)
! C
      DO 20 I = 1,LENILS
 20     ISAV(I) = CB%CM_ILS(I)
      DO 25 I = 1,LENILA
 25     ISAV(LENILS+I) = CB%CM_ILSA(I)
! C
      RETURN
! C
 100  CONTINUE
      DO 110 I = 1,LENRLS
 110     CB%CM_RLS(I) = RSAV(I)
      DO 115 I = 1,LENRLA
 115     CB%CM_RLSA(I) = RSAV(LENRLS+I)
! C
      DO 120 I = 1,LENILS
 120     CB%CM_ILS(I) = ISAV(I)
      DO 125 I = 1,LENILA
 125     CB%CM_ILSA(I) = ISAV(LENILS+I)
! C
      RETURN
! C----------------------- End of Subroutine DSRCMA ----------------------
      END
! *DECK DGEFA
      ATTRIBUTES(DEVICE) SUBROUTINE DGEFA (A, LDA, DGE_N, IPVT, INFO, CB)
! C***BEGIN PROLOGUE  DGEFA
! C***PURPOSE  Factor a matrix using Gaussian elimination.
! C***CATEGORY  D2A1
! C***TYPE      DOUBLE PRECISION (SGEFA-S, DGEFA-D, CGEFA-C)
! C***KEYWORDS  GENERAL MATRIX, LINEAR ALGEBRA, LINPACK,
! C             MATRIX FACTORIZATION
! C***AUTHOR  Moler, C. B., (U. of New Mexico)
! C***DESCRIPTION
! C
! C     DGEFA factors a double precision matrix by Gaussian elimination.
! C
! C     DGEFA is usually called by DGECO, but it can be called
! C     directly with a saving in time if  RCOND  is not needed.
! C     (Time for DGECO) = (1 + 9/CB%CM_N)*(Time for DGEFA) .
! C
! C     On Entry
! C
! C        A       DOUBLE PRECISION(LDA, CB%CM_N)
! C                the matrix to be factored.
! C
! C        LDA     INTEGER
! C                the leading dimension of the array  A .
! C
! C        CB%CM_N       INTEGER
! C                the order of the matrix  A .
! C
! C     On Return
! C
! C        A       an upper triangular matrix and the multipliers
! C                which were used to obtain it.
! C                The factorization can be written  A = CB%CM_L*U  where
! C                CB%CM_L  is a product of permutation and unit lower
! C                triangular matrices and  U  is upper triangular.
! C
! C        IPVT    INTEGER(CB%CM_N)
! C                an integer vector of pivot indices.
! C
! C        INFO    INTEGER
! C                = 0  normal value.
! C                = K  if  U(K,K) .EQ. 0.0 .  This is not an error
! C                     condition for this subroutine, but it does
! C                     indicate that DGESL or DGEDI will divide by zero
! C                     if called.  Use  RCOND  in DGECO for a reliable
! C                     indication of singularity.
! C
! C***REFERENCES  J. J. Dongarra, J. R. Bunch, C. B. Moler, and G. W.
! C                 Stewart, LINPACK Users' Guide, SIAM, 1979.
! C***ROUTINES CALLED  DAXPY, DSCAL, IDAMAX
! C***REVISION HISTORY  (YYMMDD)
! C   780814  DATE WRITTEN
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900326  Removed duplicate information from DESCRIPTION section.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DGEFA
      INTEGER LDA,DGE_N,IPVT(*),INFO
      DOUBLE PRECISION A(LDA,*)
      TYPE(commonBlock) CB
! C
      DOUBLE PRECISION T
      INTEGER IDAMAX,J,K,KP1,DGE_L,NM1
! C
! C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
! C
! C***FIRST EXECUTABLE STATEMENT  DGEFA
      INFO = 0
      NM1 = DGE_N - 1
      IF (NM1 .LT. 1) GO TO 70
      DO 60 K = 1, NM1
         KP1 = K + 1
! C
! C        FIND L = PIVOT INDEX
! C
         DGE_L = IDAMAX(DGE_N-K+1,A(K,K),1, CB) + K - 1
         IPVT(K) = DGE_L
! C
! C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
! C
         IF (A(DGE_L,K) .EQ. 0.0D0) GO TO 40
! C
! C           INTERCHANGE IF NECESSARY
! C
            IF (DGE_L .EQ. K) GO TO 10
               T = A(DGE_L,K)
               A(DGE_L,K) = A(K,K)
               A(K,K) = T
   10       CONTINUE
! C
! C           COMPUTE MULTIPLIERS
! C
            T = -1.0D0/A(K,K)
            CALL DSCAL(DGE_N-K,T,A(K+1,K),1,CB)
! C
! C           ROW ELIMINATION WITH COLUMN INDEXING
! C
            DO 30 J = KP1, DGE_N
               T = A(DGE_L,J)
               IF (DGE_L .EQ. K) GO TO 20
                  A(DGE_L,J) = A(K,J)
                  A(K,J) = T
   20          CONTINUE
               CALL DAXPY(DGE_N-K,T,A(K+1,K),1,A(K+1,J),1,CB)
   30       CONTINUE
         GO TO 50
   40    CONTINUE
            INFO = K
   50    CONTINUE
   60 CONTINUE
   70 CONTINUE
      IPVT(DGE_N) = DGE_N
      IF (A(DGE_N,DGE_N) .EQ. 0.0D0) INFO = DGE_N
      RETURN
! C----------------------- End of Subroutine DGEFA ----------------------
      END
! *DECK DGESL
      ATTRIBUTES(DEVICE) SUBROUTINE DGESL (A, LDA, DGES_N, IPVT, B, JOB, CB)
! C***BEGIN PROLOGUE  DGESL
! C***PURPOSE  Solve the real system A*X=B or TRANS(A)*X=B using the
! C            factors computed by DGECO or DGEFA.
! C***CATEGORY  D2A1
! C***TYPE      DOUBLE PRECISION (SGESL-S, DGESL-D, CGESL-C)
! C***KEYWORDS  LINEAR ALGEBRA, LINPACK, MATRIX, SOLVE
! C***AUTHOR  Moler, C. B., (U. of New Mexico)
! C***DESCRIPTION
! C
! C     DGESL solves the double precision system
! C     A * X = B  or  TRANS(A) * X = B
! C     using the factors computed by DGECO or DGEFA.
! C
! C     On Entry
! C
! C        A       DOUBLE PRECISION(LDA, CB%CM_N)
! C                the output from DGECO or DGEFA.
! C
! C        LDA     INTEGER
! C                the leading dimension of the array  A .
! C
! C        CB%CM_N       INTEGER
! C                the order of the matrix  A .
! C
! C        IPVT    INTEGER(CB%CM_N)
! C                the pivot vector from DGECO or DGEFA.
! C
! C        B       DOUBLE PRECISION(CB%CM_N)
! C                the right hand side vector.
! C
! C        JOB     INTEGER
! C                = 0         to solve  A*X = B ,
! C                = nonzero   to solve  TRANS(A)*X = B  where
! C                            TRANS(A)  is the transpose.
! C
! C     On Return
! C
! C        B       the solution vector  X .
! C
! C     Error Condition
! C
! C        A division by zero will occur if the input factor contains a
! C        zero on the diagonal.  Technically this indicates singularity
! C        but it is often caused by improper arguments or improper
! C        setting of LDA .  It will not occur if the subroutines are
! C        called correctly and if DGECO has set RCOND .GT. 0.0
! C        or DGEFA has set INFO .EQ. 0 .
! C
! C     To compute  INVERSE(A) * C  where  C  is a matrix
! C     with  P  columns
! C           CALL DGECO(A,LDA,CB%CM_N,IPVT,RCOND,Z)
! C           IF (RCOND is too small) GO TO ...
! C           DO 10 J = 1, P
! C              CALL DGESL(A,LDA,CB%CM_N,IPVT,C(1,J),0)
! C        10 CONTINUE
! C
! C***REFERENCES  J. J. Dongarra, J. R. Bunch, C. B. Moler, and G. W.
! C                 Stewart, LINPACK Users' Guide, SIAM, 1979.
! C***ROUTINES CALLED  DAXPY, DDOT
! C***REVISION HISTORY  (YYMMDD)
! C   780814  DATE WRITTEN
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900326  Removed duplicate information from DESCRIPTION section.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DGESL
      INTEGER LDA,DGES_N,IPVT(*),JOB
      DOUBLE PRECISION A(LDA,*),B(*)
      TYPE(commonBlock) CB
! C
      DOUBLE PRECISION DDOT,T
      INTEGER K,KB,DGES_L,NM1
! C***FIRST EXECUTABLE STATEMENT  DGESL
      NM1 = DGES_N - 1
      IF (JOB .NE. 0) GO TO 50
! C
! C        JOB = 0 , SOLVE  A * X = B
! C        FIRST SOLVE  CB%CM_L*Y = B
! C
         IF (NM1 .LT. 1) GO TO 30
         DO 20 K = 1, NM1
            DGES_L = IPVT(K)
            T = B(DGES_L)
            IF (DGES_L .EQ. K) GO TO 10
               B(DGES_L) = B(K)
               B(K) = T
   10       CONTINUE
            CALL DAXPY(DGES_N-K,T,A(K+1,K),1,B(K+1),1,CB)
   20    CONTINUE
   30    CONTINUE
! C
! C        NOW SOLVE  U*X = Y
! C
         DO 40 KB = 1, DGES_N
            K = DGES_N + 1 - KB
            B(K) = B(K)/A(K,K)
            T = -B(K)
            CALL DAXPY(K-1,T,A(1,K),1,B(1),1,CB)
   40    CONTINUE
      GO TO 100
   50 CONTINUE
! C
! C        JOB = NONZERO, SOLVE  TRANS(A) * X = B
! C        FIRST SOLVE  TRANS(U)*Y = B
! C
         DO 60 K = 1, DGES_N
            T = DDOT(K-1,A(1,K),1,B(1),1,CB)
            B(K) = (B(K) - T)/A(K,K)
   60    CONTINUE
! C
! C        NOW SOLVE TRANS(L)*X = Y
! C
         IF (NM1 .LT. 1) GO TO 90
         DO 80 KB = 1, NM1
            K = DGES_N - KB
            B(K) = B(K) + DDOT(DGES_N-K,A(K+1,K),1,B(K+1),1,CB)
            DGES_L = IPVT(K)
            IF (DGES_L .EQ. K) GO TO 70
               T = B(DGES_L)
               B(DGES_L) = B(K)
               B(K) = T
   70       CONTINUE
   80    CONTINUE
   90    CONTINUE
  100 CONTINUE
      RETURN
! C----------------------- End of Subroutine DGESL ----------------------
      END
! *DECK DGBFA
      ATTRIBUTES(DEVICE) SUBROUTINE DGBFA (ABD, LDA, DGB_N, ML, MU, IPVT, INFO, CB)
! C***BEGIN PROLOGUE  DGBFA
! C***PURPOSE  Factor a band matrix using Gaussian elimination.
! C***CATEGORY  D2A2
! C***TYPE      DOUBLE PRECISION (SGBFA-S, DGBFA-D, CGBFA-C)
! C***KEYWORDS  BANDED, LINEAR ALGEBRA, LINPACK, MATRIX FACTORIZATION
! C***AUTHOR  Moler, C. B., (U. of New Mexico)
! C***DESCRIPTION
! C
! C     DGBFA factors a double precision band matrix by elimination.
! C
! C     DGBFA is usually called by DGBCO, but it can be called
! C     directly with a saving in time if  RCOND  is not needed.
! C
! C     On Entry
! C
! C        ABD     DOUBLE PRECISION(LDA, CB%CM_N)
! C                contains the matrix in band storage.  The columns
! C                of the matrix are stored in the columns of  ABD  and
! C                the diagonals of the matrix are stored in rows
! C                ML+1 through 2*ML+MU+1 of  ABD .
! C                See the comments below for details.
! C
! C        LDA     INTEGER
! C                the leading dimension of the array  ABD .
! C                LDA must be .GE. 2*ML + MU + 1 .
! C
! C        CB%CM_N       INTEGER
! C                the order of the original matrix.
! C
! C        ML      INTEGER
! C                number of diagonals below the main diagonal.
! C                0 .LE. ML .LT.  CB%CM_N .
! C
! C        MU      INTEGER
! C                number of diagonals above the main diagonal.
! C                0 .LE. MU .LT.  CB%CM_N .
! C                More efficient if  ML .LE. MU .
! C     On Return
! C
! C        ABD     an upper triangular matrix in band storage and
! C                the multipliers which were used to obtain it.
! C                The factorization can be written  A = CB%CM_L*U  where
! C                CB%CM_L  is a product of permutation and unit lower
! C                triangular matrices and  U  is upper triangular.
! C
! C        IPVT    INTEGER(CB%CM_N)
! C                an integer vector of pivot indices.
! C
! C        INFO    INTEGER
! C                = 0  normal value.
! C                = K  if  U(K,K) .EQ. 0.0 .  This is not an error
! C                     condition for this subroutine, but it does
! C                     indicate that DGBSL will divide by zero if
! C                     called.  Use  RCOND  in DGBCO for a reliable
! C                     indication of singularity.
! C
! C     Band Storage
! C
! C           If  A  is a band matrix, the following program segment
! C           will set up the input.
! C
! C                   ML = (band width below the diagonal)
! C                   MU = (band width above the diagonal)
! C                   M = ML + MU + 1
! C                   DO 20 J = 1, CB%CM_N
! C                      I1 = MAX(1, J-MU)
! C                      I2 = MIN(CB%CM_N, J+ML)
! C                      DO 10 I = I1, I2
! C                         K = I - J + M
! C                         ABD(K,J) = A(I,J)
! C                10    CONTINUE
! C                20 CONTINUE
! C
! C           This uses rows  ML+1  through  2*ML+MU+1  of  ABD .
! C           In addition, the first  ML  rows in  ABD  are used for
! C           elements generated during the triangularization.
! C           The total number of rows needed in  ABD  is  2*ML+MU+1 .
! C           The  ML+MU by ML+MU  upper left triangle and the
! C           ML by ML  lower right triangle are not referenced.
! C
! C***REFERENCES  J. J. Dongarra, J. R. Bunch, C. B. Moler, and G. W.
! C                 Stewart, LINPACK Users' Guide, SIAM, 1979.
! C***ROUTINES CALLED  DAXPY, DSCAL, IDAMAX
! C***REVISION HISTORY  (YYMMDD)
! C   780814  DATE WRITTEN
! C   890531  Changed all specific intrinsics to generic.  (WRB)
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900326  Removed duplicate information from DESCRIPTION section.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DGBFA
      INTEGER LDA,DGB_N,ML,MU,IPVT(*),INFO
      DOUBLE PRECISION ABD(LDA,*)
      TYPE(commonBlock) CB
! C
      DOUBLE PRECISION T
      INTEGER I,IDAMAX,I0,J,JU,JZ,J0,J1,K,KP1,DGB_L,LM,M,MM,NM1
! C
! C***FIRST EXECUTABLE STATEMENT  DGBFA
      M = ML + MU + 1
      INFO = 0
! C
! C     ZERO INITIAL FILL-IN COLUMNS
! C
      J0 = MU + 2
      J1 = MIN(DGB_N,M) - 1
      IF (J1 .LT. J0) GO TO 30
      DO 20 JZ = J0, J1
         I0 = M + 1 - JZ
         DO 10 I = I0, ML
            ABD(I,JZ) = 0.0D0
   10    CONTINUE
   20 CONTINUE
   30 CONTINUE
      JZ = J1
      JU = 0
! C
! C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
! C
      NM1 = DGB_N - 1
      IF (NM1 .LT. 1) GO TO 130
      DO 120 K = 1, NM1
         KP1 = K + 1
! C
! C        ZERO NEXT FILL-IN COLUMN
! C
         JZ = JZ + 1
         IF (JZ .GT. DGB_N) GO TO 50
         IF (ML .LT. 1) GO TO 50
            DO 40 I = 1, ML
               ABD(I,JZ) = 0.0D0
   40       CONTINUE
   50    CONTINUE
! C
! C        FIND L = PIVOT INDEX
! C
         LM = MIN(ML,DGB_N-K)
         DGB_L = IDAMAX(LM+1,ABD(M,K),1, CB) + M - 1
         IPVT(K) = DGB_L + K - M
! C
! C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
! C
         IF (ABD(DGB_L,K) .EQ. 0.0D0) GO TO 100
! C
! C           INTERCHANGE IF NECESSARY
! C
            IF (DGB_L .EQ. M) GO TO 60
               T = ABD(DGB_L,K)
               ABD(DGB_L,K) = ABD(M,K)
               ABD(M,K) = T
   60       CONTINUE
! C
! C           COMPUTE MULTIPLIERS
! C
            T = -1.0D0/ABD(M,K)
            CALL DSCAL(LM,T,ABD(M+1,K),1,CB)
! C
! C           ROW ELIMINATION WITH COLUMN INDEXING
! C
            JU = MIN(MAX(JU,MU+IPVT(K)),DGB_N)
            MM = M
            IF (JU .LT. KP1) GO TO 90
            DO 80 J = KP1, JU
               DGB_L = DGB_L - 1
               MM = MM - 1
               T = ABD(DGB_L,J)
               IF (DGB_L .EQ. MM) GO TO 70
                  ABD(DGB_L,J) = ABD(MM,J)
                  ABD(MM,J) = T
   70          CONTINUE
               CALL DAXPY(LM,T,ABD(M+1,K),1,ABD(MM+1,J),1,CB)
   80       CONTINUE
   90       CONTINUE
         GO TO 110
  100    CONTINUE
            INFO = K
  110    CONTINUE
  120 CONTINUE
  130 CONTINUE
      IPVT(DGB_N) = DGB_N
      IF (ABD(M,DGB_N) .EQ. 0.0D0) INFO = DGB_N
      RETURN
! C----------------------- End of Subroutine DGBFA ----------------------
      END
! *DECK DGBSL
      ATTRIBUTES(DEVICE) SUBROUTINE DGBSL (ABD, LDA, DGBS_N, ML, MU, IPVT, B, JOB, CB)
! C***BEGIN PROLOGUE  DGBSL
! C***PURPOSE  Solve the real band system A*X=B or TRANS(A)*X=B using
! C            the factors computed by DGBCO or DGBFA.
! C***CATEGORY  D2A2
! C***TYPE      DOUBLE PRECISION (SGBSL-S, DGBSL-D, CGBSL-C)
! C***KEYWORDS  BANDED, LINEAR ALGEBRA, LINPACK, MATRIX, SOLVE
! C***AUTHOR  Moler, C. B., (U. of New Mexico)
! C***DESCRIPTION
! C
! C     DGBSL solves the double precision band system
! C     A * X = B  or  TRANS(A) * X = B
! C     using the factors computed by DGBCO or DGBFA.
! C
! C     On Entry
! C
! C        ABD     DOUBLE PRECISION(LDA, CB%CM_N)
! C                the output from DGBCO or DGBFA.
! C
! C        LDA     INTEGER
! C                the leading dimension of the array  ABD .
! C
! C        CB%CM_N       INTEGER
! C                the order of the original matrix.
! C
! C        ML      INTEGER
! C                number of diagonals below the main diagonal.
! C
! C        MU      INTEGER
! C                number of diagonals above the main diagonal.
! C
! C        IPVT    INTEGER(CB%CM_N)
! C                the pivot vector from DGBCO or DGBFA.
! C
! C        B       DOUBLE PRECISION(CB%CM_N)
! C                the right hand side vector.
! C
! C        JOB     INTEGER
! C                = 0         to solve  A*X = B ,
! C                = nonzero   to solve  TRANS(A)*X = B , where
! C                            TRANS(A)  is the transpose.
! C
! C     On Return
! C
! C        B       the solution vector  X .
! C
! C     Error Condition
! C
! C        A division by zero will occur if the input factor contains a
! C        zero on the diagonal.  Technically this indicates singularity
! C        but it is often caused by improper arguments or improper
! C        setting of LDA .  It will not occur if the subroutines are
! C        called correctly and if DGBCO has set RCOND .GT. 0.0
! C        or DGBFA has set INFO .EQ. 0 .
! C
! C     To compute  INVERSE(A) * C  where  C  is a matrix
! C     with  P  columns
! C           CALL DGBCO(ABD,LDA,CB%CM_N,ML,MU,IPVT,RCOND,Z)
! C           IF (RCOND is too small) GO TO ...
! C           DO 10 J = 1, P
! C              CALL DGBSL(ABD,LDA,CB%CM_N,ML,MU,IPVT,C(1,J),0)
! C        10 CONTINUE
! C
! C***REFERENCES  J. J. Dongarra, J. R. Bunch, C. B. Moler, and G. W.
! C                 Stewart, LINPACK Users' Guide, SIAM, 1979.
! C***ROUTINES CALLED  DAXPY, DDOT
! C***REVISION HISTORY  (YYMMDD)
! C   780814  DATE WRITTEN
! C   890531  Changed all specific intrinsics to generic.  (WRB)
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900326  Removed duplicate information from DESCRIPTION section.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DGBSL
      INTEGER LDA,DGBS_N,ML,MU,IPVT(*),JOB
      DOUBLE PRECISION ABD(LDA,*),B(*)
      TYPE(commonBlock) CB
! C
      DOUBLE PRECISION DDOT,T
      INTEGER K,KB,DGBS_L,LA,LB,LM,M,NM1
! C***FIRST EXECUTABLE STATEMENT  DGBSL
      M = MU + ML + 1
      NM1 = DGBS_N - 1
      IF (JOB .NE. 0) GO TO 50
! C
! C        JOB = 0 , SOLVE  A * X = B
! C        FIRST SOLVE CB%CM_L*Y = B
! C
         IF (ML .EQ. 0) GO TO 30
         IF (NM1 .LT. 1) GO TO 30
            DO 20 K = 1, NM1
               LM = MIN(ML,DGBS_N-K)
               DGBS_L = IPVT(K)
               T = B(DGBS_L)
               IF (DGBS_L .EQ. K) GO TO 10
                  B(DGBS_L) = B(K)
                  B(K) = T
   10          CONTINUE
               CALL DAXPY(LM,T,ABD(M+1,K),1,B(K+1),1,CB)
   20       CONTINUE
   30    CONTINUE
! C
! C        NOW SOLVE  U*X = Y
! C
         DO 40 KB = 1, DGBS_N
            K = DGBS_N + 1 - KB
            B(K) = B(K)/ABD(M,K)
            LM = MIN(K,M) - 1
            LA = M - LM
            LB = K - LM
            T = -B(K)
            CALL DAXPY(LM,T,ABD(LA,K),1,B(LB),1,CB)
   40    CONTINUE
      GO TO 100
   50 CONTINUE
! C
! C        JOB = NONZERO, SOLVE  TRANS(A) * X = B
! C        FIRST SOLVE  TRANS(U)*Y = B
! C
         DO 60 K = 1, DGBS_N
            LM = MIN(K,M) - 1
            LA = M - LM
            LB = K - LM
            T = DDOT(LM,ABD(LA,K),1,B(LB),1,CB)
            B(K) = (B(K) - T)/ABD(M,K)
   60    CONTINUE
! C
! C        NOW SOLVE TRANS(L)*X = Y
! C
         IF (ML .EQ. 0) GO TO 90
         IF (NM1 .LT. 1) GO TO 90
            DO 80 KB = 1, NM1
               K = DGBS_N - KB
               LM = MIN(ML,DGBS_N-K)
               B(K) = B(K) + DDOT(LM,ABD(M+1,K),1,B(K+1),1,CB)
               DGBS_L = IPVT(K)
               IF (DGBS_L .EQ. K) GO TO 70
                  T = B(DGBS_L)
                  B(DGBS_L) = B(K)
                  B(K) = T
   70          CONTINUE
   80       CONTINUE
   90    CONTINUE
  100 CONTINUE
      RETURN
! C----------------------- End of Subroutine DGBSL ----------------------
      END
! C***WHAT IS REQUIRED FOR DOUBLE PRECISION FUNCTION?
!DECK DUMACH
      ATTRIBUTES(DEVICE) DOUBLE PRECISION FUNCTION DUMACH (CB)
! C***BEGIN PROLOGUE  DUMACH
! C***PURPOSE  Compute the unit roundoff of the machine.
! C***CATEGORY  R1
! C***TYPE      DOUBLE PRECISION (RUMACH-S, DUMACH-D)
! C***KEYWORDS  MACHINE CONSTANTS
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C *Usage:
! C        DOUBLE PRECISION  A, DUMACH
! C        A = DUMACH()
! C
! C *Function Return Values:
! C     A : the unit roundoff of the machine.
! C
! C *Description:
! C     The unit roundoff is defined as the smallest positive machine
! C     number u such that  1.0 + u .ne. 1.0.  This is computed by DUMACH
! C     in a machine-independent manner.
! C
! C***REFERENCES  (NONE)
! C***ROUTINES CALLED  DUMSUM
! C***REVISION HISTORY  (YYYYMMDD)
! C   19930216  DATE WRITTEN
! C   19930818  Added SLATEC-format prologue.  (FNF)
! C   20030707  Added DUMSUM to force normal storage of COMP.  (ACH)
! C***END PROLOGUE  DUMACH
! C
      DOUBLE PRECISION U, COMP
      TYPE(commonBlock) CB
! C***FIRST EXECUTABLE STATEMENT  DUMACH
      U = 1.0D0
 10   U = U*0.5D0
      CALL DUMSUM(1.0D0, U, COMP, CB)
      IF (COMP .NE. 1.0D0) GO TO 10
      DUMACH = U*2.0D0
      RETURN
! C----------------------- End of Function DUMACH ------------------------
      END
! *DECK IXSAV
      ATTRIBUTES(DEVICE) INTEGER FUNCTION IXSAV (IPAR, IVALUE, ISET, CB)
! C***BEGIN PROLOGUE  IXSAV
! C***SUBSIDIARY
! C***PURPOSE  Save and recall error message control parameters.
! C***CATEGORY  R3C
! C***TYPE      ALL (IXSAV-A)
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C
! C  IXSAV saves and recalls one of two error message parameters:
! C    LUNIT, the logical unit number to which messages are printed, and
! C    MESFLG, the message print flag.
! C  This is a modification of the SLATEC library routine J4SAVE.
! C
! C  Saved local variables..
! C   LUNIT  = Logical unit number for messages.  The default is obtained
! C            by a call to IUMACH (may be machine-dependent).
! C   MESFLG = Print control flag..
! C            1 means print all messages (the default).
! C            0 means no printing.
! C
! C  On input..
! C    IPAR   = Parameter indicator (1 for LUNIT, 2 for MESFLG).
! C    IVALUE = The value to be set for the parameter, if ISET = .TRUE.
! C    ISET   = Logical flag to indicate whether to read or write.
! C             If ISET = .TRUE., the parameter will be given
! C             the value IVALUE.  If ISET = .FALSE., the parameter
! C             will be unchanged, and IVALUE is a dummy argument.
! C
! C  On return..
! C    IXSAV = The (old) value of the parameter.
! C
! C***SEE ALSO  XERRWD, XERRWV
! C***ROUTINES CALLED  IUMACH
! C***REVISION HISTORY  (YYMMDD)
! C   921118  DATE WRITTEN
! C   930329  Modified prologue to SLATEC format. (FNF)
! C   930915  Added IUMACH call to get default output unit.  (ACH)
! C   930922  Minor cosmetic changes. (FNF)
! C   010425  Type declaration for IUMACH added. (ACH)
! C***END PROLOGUE  IXSAV
! C
! C Subroutines called by IXSAV.. None
! C Function routine called by IXSAV.. IUMACH
! C-----------------------------------------------------------------------
! C**End
      LOGICAL ISET
      INTEGER IPAR, IVALUE
      TYPE(commonBlock) CB
! C-----------------------------------------------------------------------
      INTEGER LUNIT, MESFLG
! C-----------------------------------------------------------------------
! C The following Fortran-77 declaration is to cause the values of the
! C listed (local) variables to be saved between calls to this routine.
! C-----------------------------------------------------------------------
      !SAVE LUNIT, MESFLG
      !DATA LUNIT/-1/, MESFLG/1/
      !!!$OMP THREADPRIVATE(LUNIT,MESFLG)
! C
! C***FIRST EXECUTABLE STATEMENT  IXSAV
      LUNIT = -1
      MESFLG = 1
      IF (IPAR .EQ. 1) THEN
        IF (LUNIT .EQ. -1) LUNIT = 6
        IXSAV = LUNIT
        IF (ISET) LUNIT = IVALUE
        ENDIF
! C
      IF (IPAR .EQ. 2) THEN
        IXSAV = MESFLG
        IF (ISET) MESFLG = IVALUE
        ENDIF
! C
      RETURN
! C----------------------- End of Function IXSAV -------------------------
      END
! *DECK IDAMAX
      ATTRIBUTES(DEVICE) INTEGER FUNCTION IDAMAX (IDA_N, DX, INCX, CB)
! C***BEGIN PROLOGUE  IDAMAX
! C***PURPOSE  Find the smallest index of that component of a vector
! C            having the maximum magnitude.
! C***CATEGORY  D1A2
! C***TYPE      DOUBLE PRECISION (ISAMAX-S, IDAMAX-D, ICAMAX-C)
! C***KEYWORDS  BLAS, LINEAR ALGEBRA, MAXIMUM COMPONENT, VECTOR
! C***AUTHOR  Lawson, C. CB%CM_L., (JPL)
! C           Hanson, R. J., (SNLA)
! C           Kincaid, D. R., (U. of Texas)
! C           Krogh, F. T., (JPL)
! C***DESCRIPTION
! C
! C                B CB%CM_L A S  Subprogram
! C    Description of Parameters
! C
! C     --Input--
! C        CB%CM_N  number of elements in input vector(s)
! C       DX  double precision vector with CB%CM_N elements
! C     INCX  storage spacing between elements of DX
! C
! C     --Output--
! C   IDAMAX  smallest index (zero if CB%CM_N .LE. 0)
! C
! C     Find smallest index of maximum magnitude of double precision DX.
! C     IDAMAX = first I, I = 1 to CB%CM_N, to maximize ABS(DX(IX+(I-1)*INCX)),
! C     where IX = 1 if INCX .GE. 0, else IX = 1+(1-CB%CM_N)*INCX.
! C
! C***REFERENCES  C. CB%CM_L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
! C                 Krogh, Basic linear algebra subprograms for Fortran
! C                 usage, Algorithm No. 539, Transactions on Mathematical
! C                 Software 5, 3 (September 1979), pp. 308-323.
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791001  DATE WRITTEN
! C   890531  Changed all specific intrinsics to generic.  (WRB)
! C   890531  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900821  Modified to correct problem with a negative increment.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  IDAMAX
      DOUBLE PRECISION DX(*), DMAX, XMAG
      INTEGER I, INCX, IX, IDA_N
      TYPE(commonBlock) CB
! C***FIRST EXECUTABLE STATEMENT  IDAMAX
      IDAMAX = 0
      IF (IDA_N .LE. 0) RETURN
      IDAMAX = 1
      IF (IDA_N .EQ. 1) RETURN
! C
      IF (INCX .EQ. 1) GOTO 20
! C
! C     Code for increments not equal to 1.
! C
      IX = 1
      IF (INCX .LT. 0) IX = (-IDA_N+1)*INCX + 1
      DMAX = ABS(DX(IX))
      IX = IX + INCX
      DO 10 I = 2,IDA_N
        XMAG = ABS(DX(IX))
        IF (XMAG .GT. DMAX) THEN
          IDAMAX = I
          DMAX = XMAG
        ENDIF
        IX = IX + INCX
   10 CONTINUE
      RETURN
! C
! C     Code for increments equal to 1.
! C
   20 DMAX = ABS(DX(1))
      DO 30 I = 2,IDA_N
        XMAG = ABS(DX(I))
        IF (XMAG .GT. DMAX) THEN
          IDAMAX = I
          DMAX = XMAG
        ENDIF
   30 CONTINUE
      RETURN
      END
! *DECK DAXPY
      ATTRIBUTES(DEVICE) SUBROUTINE DAXPY (DAX_N, DA, DX, INCX, DY, INCY, CB)
! C***BEGIN PROLOGUE  DAXPY
! C***PURPOSE  Compute a constant times a vector plus a vector.
! C***CATEGORY  D1A7
! C***TYPE      DOUBLE PRECISION (SAXPY-S, DAXPY-D, CAXPY-C)
! C***KEYWORDS  BLAS, LINEAR ALGEBRA, TRIAD, VECTOR
! C***AUTHOR  Lawson, C. CB%CM_L., (JPL)
! C           Hanson, R. J., (SNLA)
! C           Kincaid, D. R., (U. of Texas)
! C           Krogh, F. T., (JPL)
! C***DESCRIPTION
! C
! C                B CB%CM_L A S  Subprogram
! C    Description of Parameters
! C
! C     --Input--
! C        CB%CM_N  number of elements in input vector(s)
! C       DA  double precision scalar multiplier
! C       DX  double precision vector with CB%CM_N elements
! C     INCX  storage spacing between elements of DX
! C       DY  double precision vector with CB%CM_N elements
! C     INCY  storage spacing between elements of DY
! C
! C     --Output--
! C       DY  double precision result (unchanged if CB%CM_N .LE. 0)
! C
! C     Overwrite double precision DY with double precision DA*DX + DY.
! C     For I = 0 to CB%CM_N-1, replace  DY(LY+I*INCY) with DA*DX(LX+I*INCX) +
! C       DY(LY+I*INCY),
! C     where LX = 1 if INCX .GE. 0, else LX = 1+(1-CB%CM_N)*INCX, and LY is
! C     defined in a similar way using INCY.
! C
! C***REFERENCES  C. CB%CM_L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
! C                 Krogh, Basic linear algebra subprograms for Fortran
! C                 usage, Algorithm No. 539, Transactions on Mathematical
! C                 Software 5, 3 (September 1979), pp. 308-323.
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791001  DATE WRITTEN
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   920310  Corrected definition of LX in DESCRIPTION.  (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DAXPY
      INTEGER DAX_N, INCX, INCY
      DOUBLE PRECISION DX(*), DY(*), DA
      TYPE(commonBlock) CB
! C***FIRST EXECUTABLE STATEMENT  DAXPY
      IF (DAX_N.LE.0 .OR. DA.EQ.0.0D0) RETURN
      IF (INCX .EQ. INCY) IF (INCX-1) 5,20,60
! C
! C     Code for unequal or nonpositive increments.
! C
    5 IX = 1
      IY = 1
      IF (INCX .LT. 0) IX = (-DAX_N+1)*INCX + 1
      IF (INCY .LT. 0) IY = (-DAX_N+1)*INCY + 1
      DO 10 I = 1,DAX_N
        DY(IY) = DY(IY) + DA*DX(IX)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
! C
! C     Code for both increments equal to 1.
! C
! C     Clean-up loop so remaining vector length is a multiple of 4.
! C
   20 M = MOD(DAX_N,4)
      IF (M .EQ. 0) GO TO 40
      DO 30 I = 1,M
        DY(I) = DY(I) + DA*DX(I)
   30 CONTINUE
      IF (DAX_N .LT. 4) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,DAX_N,4
        DY(I) = DY(I) + DA*DX(I)
        DY(I+1) = DY(I+1) + DA*DX(I+1)
        DY(I+2) = DY(I+2) + DA*DX(I+2)
        DY(I+3) = DY(I+3) + DA*DX(I+3)
   50 CONTINUE
      RETURN
! C
! C     Code for equal, positive, non-unit increments.
! C
   60 NS = DAX_N*INCX
      DO 70 I = 1,NS,INCX
        DY(I) = DA*DX(I) + DY(I)
   70 CONTINUE
      RETURN
! C----------------------- End of Subroutine DAXPY ----------------------
      END
! *DECK DUMSUM
      ATTRIBUTES(DEVICE) SUBROUTINE DUMSUM(A,B,C,CB)
! C     Routine to force normal storing of A + B, for DUMACH.
      DOUBLE PRECISION A, B, C
      TYPE(commonBlock) CB
      C = A + B
      RETURN
! C----------------------- End of Subroutine DUMSUM ----------------------
      END
! *DECK DSCAL
      ATTRIBUTES(DEVICE) SUBROUTINE DSCAL (DSC_N, DA, DX, INCX, CB)
! C***BEGIN PROLOGUE  DSCAL
! C***PURPOSE  Multiply a vector by a constant.
! C***CATEGORY  D1A6
! C***TYPE      DOUBLE PRECISION (SSCAL-S, DSCAL-D, CSCAL-C)
! C***KEYWORDS  BLAS, LINEAR ALGEBRA, SCALE, VECTOR
! C***AUTHOR  Lawson, C. CB%CM_L., (JPL)
! C           Hanson, R. J., (SNLA)
! C           Kincaid, D. R., (U. of Texas)
! C           Krogh, F. T., (JPL)
! C***DESCRIPTION
! C
! C                B CB%CM_L A S  Subprogram
! C    Description of Parameters
! C
! C     --Input--
! C        CB%CM_N  number of elements in input vector(s)
! C       DA  double precision scale factor
! C       DX  double precision vector with CB%CM_N elements
! C     INCX  storage spacing between elements of DX
! C
! C     --Output--
! C       DX  double precision result (unchanged if CB%CM_N.LE.0)
! C
! C     Replace double precision DX by double precision DA*DX.
! C     For I = 0 to CB%CM_N-1, replace DX(IX+I*INCX) with  DA * DX(IX+I*INCX),
! C     where IX = 1 if INCX .GE. 0, else IX = 1+(1-CB%CM_N)*INCX.
! C
! C***REFERENCES  C. CB%CM_L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
! C                 Krogh, Basic linear algebra subprograms for Fortran
! C                 usage, Algorithm No. 539, Transactions on Mathematical
! C                 Software 5, 3 (September 1979), pp. 308-323.
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791001  DATE WRITTEN
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   900821  Modified to correct problem with a negative increment.
! C           (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DSCAL
      DOUBLE PRECISION DA, DX(*)
      INTEGER I, INCX, IX, M, MP1, DSC_N
      TYPE(commonBlock) CB
! C***FIRST EXECUTABLE STATEMENT  DSCAL
      IF (DSC_N .LE. 0) RETURN
      IF (INCX .EQ. 1) GOTO 20
! C
! C     Code for increment not equal to 1.
! C
      IX = 1
      IF (INCX .LT. 0) IX = (-DSC_N+1)*INCX + 1
      DO 10 I = 1,DSC_N
        DX(IX) = DA*DX(IX)
        IX = IX + INCX
   10 CONTINUE
      RETURN
! C
! C     Code for increment equal to 1.
! C
! C     Clean-up loop so remaining vector length is a multiple of 5.
! C
   20 M = MOD(DSC_N,5)
      IF (M .EQ. 0) GOTO 40
      DO 30 I = 1,M
        DX(I) = DA*DX(I)
   30 CONTINUE
      IF (DSC_N .LT. 5) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,DSC_N,5
        DX(I) = DA*DX(I)
        DX(I+1) = DA*DX(I+1)
        DX(I+2) = DA*DX(I+2)
        DX(I+3) = DA*DX(I+3)
        DX(I+4) = DA*DX(I+4)
   50 CONTINUE
      RETURN
! C----------------------- End of Subroutine DSCAL ----------------------
      END
! *DECK DDOT
      ATTRIBUTES(DEVICE) DOUBLE PRECISION FUNCTION DDOT (DDOT_N, DX, INCX, DY, INCY, CB)
! C***BEGIN PROLOGUE  DDOT
! C***PURPOSE  Compute the inner product of two vectors.
! C***CATEGORY  D1A4
! C***TYPE      DOUBLE PRECISION (SDOT-S, DDOT-D, CDOTU-C)
! C***KEYWORDS  BLAS, INNER PRODUCT, LINEAR ALGEBRA, VECTOR
! C***AUTHOR  Lawson, C. CB%CM_L., (JPL)
! C           Hanson, R. J., (SNLA)
! C           Kincaid, D. R., (U. of Texas)
! C           Krogh, F. T., (JPL)
! C***DESCRIPTION
! C
! C                B CB%CM_L A S  Subprogram
! C    Description of Parameters
! C
! C     --Input--
! C        CB%CM_N  number of elements in input vector(s)
! C       DX  double precision vector with CB%CM_N elements
! C     INCX  storage spacing between elements of DX
! C       DY  double precision vector with CB%CM_N elements
! C     INCY  storage spacing between elements of DY
! C
! C     --Output--
! C     DDOT  double precision dot product (zero if CB%CM_N .LE. 0)
! C
! C     Returns the dot product of double precision DX and DY.
! C     DDOT = sum for I = 0 to CB%CM_N-1 of  DX(LX+I*INCX) * DY(LY+I*INCY),
! C     where LX = 1 if INCX .GE. 0, else LX = 1+(1-CB%CM_N)*INCX, and LY is
! C     defined in a similar way using INCY.
! C
! C***REFERENCES  C. CB%CM_L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
! C                 Krogh, Basic linear algebra subprograms for Fortran
! C                 usage, Algorithm No. 539, Transactions on Mathematical
! C                 Software 5, 3 (September 1979), pp. 308-323.
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   791001  DATE WRITTEN
! C   890831  Modified array declarations.  (WRB)
! C   890831  REVISION DATE from Version 3.2
! C   891214  Prologue converted to Version 4.0 format.  (BAB)
! C   920310  Corrected definition of LX in DESCRIPTION.  (WRB)
! C   920501  Reformatted the REFERENCES section.  (WRB)
! C***END PROLOGUE  DDOT
      INTEGER DDOT_N, INCX, INCY
      DOUBLE PRECISION DX(*), DY(*)
      TYPE(commonBlock) CB
! C***FIRST EXECUTABLE STATEMENT  DDOT
      DDOT = 0.0D0
      IF (DDOT_N .LE. 0) RETURN
      IF (INCX .EQ. INCY) IF (INCX-1) 5,20,60
! C
! C     Code for unequal or nonpositive increments.
! C
    5 IX = 1
      IY = 1
      IF (INCX .LT. 0) IX = (-DDOT_N+1)*INCX + 1
      IF (INCY .LT. 0) IY = (-DDOT_N+1)*INCY + 1
      DO 10 I = 1,DDOT_N
        DDOT = DDOT + DX(IX)*DY(IY)
        IX = IX + INCX
        IY = IY + INCY
   10 CONTINUE
      RETURN
! C
! C     Code for both increments equal to 1.
! C
! C     Clean-up loop so remaining vector length is a multiple of 5.
! C
   20 M = MOD(DDOT_N,5)
      IF (M .EQ. 0) GO TO 40
      DO 30 I = 1,M
         DDOT = DDOT + DX(I)*DY(I)
   30 CONTINUE
      IF (DDOT_N .LT. 5) RETURN
   40 MP1 = M + 1
      DO 50 I = MP1,DDOT_N,5
      DDOT = DDOT + DX(I)*DY(I) + DX(I+1)*DY(I+1) + DX(I+2)*DY(I+2) + &
                    DX(I+3)*DY(I+3) + DX(I+4)*DY(I+4)
   50 CONTINUE
      RETURN
! C
! C     Code for equal, positive, non-unit increments.
! C
   60 NS = DDOT_N*INCX
      DO 70 I = 1,NS,INCX
        DDOT = DDOT + DX(I)*DY(I)
   70 CONTINUE
      RETURN
! C----------------------- End of Subroutine DDOT ----------------------
      END
! ! *DECK XERRWD
!       ATTRIBUTES(DEVICE) SUBROUTINE XERRWD (MSG, NMES, NERR, LEVEL, NI, I1, I2, NR, R1, R2, CB)
! ! C***BEGIN PROLOGUE  XERRWD
! ! C***SUBSIDIARY
! ! C***PURPOSE  Write error message with values.
! ! C***CATEGORY  R3C
! ! C***TYPE      DOUBLE PRECISION (XERRWV-S, XERRWD-D)
! ! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! ! C***DESCRIPTION
! ! C
! ! C  Subroutines XERRWD, XSETF, XSETUN, and the function routine IXSAV,
! ! C  as given here, constitute a simplified version of the SLATEC error
! ! C  handling package.
! ! C
! ! C  All arguments are input arguments.
! ! C
! ! C  MSG    = The message (character array).
! ! C  NMES   = The length of MSG (number of characters).
! ! C  NERR   = The error number (not used).
! ! C  LEVEL  = The error level..
! ! C           0 or 1 means recoverable (control returns to caller).
! ! C           2 means fatal (run is aborted--see note below).
! ! C  NI     = Number of integers (0, 1, or 2) to be printed with message.
! ! C  I1,I2  = Integers to be printed, depending on NI.
! ! C  NR     = Number of reals (0, 1, or 2) to be printed with message.
! ! C  R1,R2  = Reals to be printed, depending on NR.
! ! C
! ! C  Note..  this routine is machine-dependent and specialized for use
! ! C  in limited context, in the following ways..
! ! C  1. The argument MSG is assumed to be of type CHARACTER, and
! ! C     the message is printed with a format of (1X,A).
! ! C  2. The message is assumed to take only one line.
! ! C     Multi-line messages are generated by repeated calls.
! ! C  3. If LEVEL = 2, control passes to the statement   STOP
! ! C     to abort the run.  This statement may be machine-dependent.
! ! C  4. R1 and R2 are assumed to be in double precision and are printed
! ! C     in D21.13 format.
! ! C
! ! C***ROUTINES CALLED  IXSAV
! ! C***REVISION HISTORY  (YYMMDD)
! ! C   920831  DATE WRITTEN
! ! C   921118  Replaced MFLGSV/LUNSAV by IXSAV. (ACH)
! ! C   930329  Modified prologue to SLATEC format. (FNF)
! ! C   930407  Changed MSG from CHARACTER*1 array to variable. (FNF)
! ! C   930922  Minor cosmetic change. (FNF)
! ! C***END PROLOGUE  XERRWD
! ! C
! ! C*Internal Notes:
! ! C
! ! C For a different default logical unit number, IXSAV (or a subsidiary
! ! C routine that it calls) will need to be modified.
! ! C For a different run-abort command, change the statement following
! ! C statement 100 at the end.
! ! C-----------------------------------------------------------------------
! ! C Subroutines called by XERRWD.. None
! ! C Function routine called by XERRWD.. IXSAV
! ! C-----------------------------------------------------------------------
! ! C**End
! ! C
! ! C  Declare arguments.
! ! C
!       DOUBLE PRECISION R1, R2
!       INTEGER NMES, NERR, LEVEL, NI, I1, I2, NR
!       CHARACTER*(*) MSG
!       TYPE(commonBlock) CB
! ! C
! ! C  Declare local variables.
! ! C
!       INTEGER LUNIT, IXSAV, MESFLG
! ! C
! ! C  Get logical unit number and message print flag.
! ! C
! ! C***FIRST EXECUTABLE STATEMENT  XERRWD
!       LUNIT = IXSAV (1, 0, .FALSE., CB)
!       MESFLG = IXSAV (2, 0, .FALSE., CB)
!       IF (MESFLG .EQ. 0) GO TO 100
! ! C
! ! C  Write the message.
! ! C
!       WRITE (LUNIT,10)  MSG
!  10   FORMAT(1X,A)
!       IF (NI .EQ. 1) WRITE (LUNIT, 20) I1
!  20   FORMAT(6X,'In above message,  I1 =',I10)
!       IF (NI .EQ. 2) WRITE (LUNIT, 30) I1,I2
!  30   FORMAT(6X,'In above message,  I1 =',I10,3X,'I2 =',I10)
!       IF (NR .EQ. 1) WRITE (LUNIT, 40) R1
!  40   FORMAT(6X,'In above message,  R1 =',D21.13)
!       IF (NR .EQ. 2) WRITE (LUNIT, 50) R1,R2
!  50   FORMAT(6X,'In above,  R1 =',D21.13,3X,'R2 =',D21.13)
! ! C
! ! C  Abort the run if LEVEL = 2.
! ! C
!  100  IF (LEVEL .NE. 2) RETURN
!       STOP
! ! C----------------------- End of Subroutine XERRWD ----------------------
!       END
! *DECK IUMACH
!      ATTRIBUTES(DEVICE) INTEGER FUNCTION IUMACH()
! C***BEGIN PROLOGUE  IUMACH
! C***PURPOSE  Provide standard output unit number.
! C***CATEGORY  R1
! C***TYPE      INTEGER (IUMACH-I)
! C***KEYWORDS  MACHINE CONSTANTS
! C***AUTHOR  Hindmarsh, Alan C., (LLNL)
! C***DESCRIPTION
! C *Usage:
! C        INTEGER  LOUT, IUMACH
! C        LOUT = IUMACH()
! C
! C *Function Return Values:
! C     LOUT : the standard logical unit for Fortran output.
! C
! C***REFERENCES  (NONE)
! C***ROUTINES CALLED  (NONE)
! C***REVISION HISTORY  (YYMMDD)
! C   930915  DATE WRITTEN
! C   930922  Made user-callable, and other cosmetic changes. (FNF)
! C***END PROLOGUE  IUMACH
! C
! C*Internal Notes:
! C  The built-in value of 6 is standard on a wide range of Fortran
! C  systems.  This may be machine-dependent.
! C**End
! C***FIRST EXECUTABLE STATEMENT  IUMACH
!      IUMACH = 6
! C
!      RETURN
! C----------------------- End of Function IUMACH ------------------------
!      END

end module dlsoda_gpu
